//=============================================================================
// U_MVCModel
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MVCModel;

interface                 

uses
  U_Component,
  U_MVCAbstract;

type
  // TRLMVCModel
  TRLMVCModel = class(TRLComponent, IRLMVCModel)
  private
    FController: TObject;
    FControllerIntf: IRLMVCController;
    function GetController: TObject;
    procedure SetController(const AValue: TObject);
    function GetControllerIntf: IRLMVCController;
  protected
    procedure ControllerChanged; virtual;
    procedure CheckController; virtual;
    property ControllerIntf: IRLMVCController read GetControllerIntf;
  public
    procedure Initialize; virtual;
    property Controller: TObject read GetController write SetController;
  end;

implementation

uses
  U_MVCController;

{ TRLMVCModel }

function TRLMVCModel.GetController: TObject;
begin
  Result := FController;
end;

procedure TRLMVCModel.SetController(const AValue: TObject);
begin
  if AValue <> FController then
  begin
    FController := AValue;
    FControllerIntf := nil;
    ControllerChanged;
  end;
end;

function TRLMVCModel.GetControllerIntf: IRLMVCController;
begin
  if not Assigned(FControllerIntf) then
  begin
    CheckController;
    Controller.GetInterface(IRLMVCController, FControllerIntf);
  end;

  Result := FControllerIntf;
end;

procedure TRLMVCModel.ControllerChanged;
begin
end;

procedure TRLMVCModel.CheckController;
begin
  if not Assigned(Controller) then
    raise ERLMVCControllerIsNull.Create('Controller is null.');
end;

procedure TRLMVCModel.Initialize;
begin
end;

end.
