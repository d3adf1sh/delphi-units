//=============================================================================
// U_NTUtils
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_NTUtils;

interface

uses
  Windows,
  SysUtils,
  U_BaseType;

type
  // TRLNTUtils
  TRLNTUtils = class(TRLBaseType)
  public
    class function IsValidFile(const AFileName: string): Boolean;
    class function GetPathOfFile(const AFileName: string): string;
    class function GetNameOfFile(const AFileName: string): string;
    class function GetExtensionOfFile(const AFileName: string): string;
    class function GetNameOfFileWithoutExtension(const AFileName: string): string;
    class function RunApplication(const AApplicationName: string; const AShowWindow: Integer): Boolean;
    class function RunCommand(const ACommand: string; const AWait: Boolean): Boolean;
    class function RunBatch(const ACommand: string; out AOutput: string): Boolean;
    class function ExitComputer(const AFlags: LongWord): Boolean;
    class function LogoffComputer: Boolean;
    class function RestartComputer: Boolean;
    class function ShutdownComputer: Boolean;
    class function GetLastError: string;
    class procedure ClearIECache;
  end;

implementation

uses
  WinInet;

{ TRLNTUtils }

class function TRLNTUtils.IsValidFile(const AFileName: string): Boolean;
begin
  Result := DirectoryExists(GetPathOfFile(AFileName)) and
    (GetNameOfFile(AFileName) <> '');
end;

class function TRLNTUtils.GetPathOfFile(const AFileName: string): string;
begin
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(AFileName));
end;

class function TRLNTUtils.GetNameOfFile(const AFileName: string): string;
begin
  Result := ExtractFileName(AFileName);
end;

class function TRLNTUtils.GetExtensionOfFile(const AFileName: string): string;
begin
  Result := ExtractFileExt(AFileName);
end;

class function TRLNTUtils.GetNameOfFileWithoutExtension(
  const AFileName: string): string;
var
  sExtension: string;
begin
  Result := GetNameOfFile(AFileName);
  sExtension := GetExtensionOfFile(Result);
  if sExtension <> '' then
    Delete(Result, Length(Result) - Length(sExtension) + 1,
      Length(sExtension));
end;

class function TRLNTUtils.RunApplication(const AApplicationName: string;
  const AShowWindow: Integer): Boolean;
var
  lProcess: TProcessInformation;
  lStartup: TStartupInfo;
begin
  FillChar(lStartup, SizeOf(TStartupInfo), 0);
  lStartup.cb := SizeOf(TStartupInfo);
  lStartup.dwFlags := STARTF_USESHOWWINDOW;
  lStartup.wShowWindow := AShowWindow;
  Result := CreateProcess(PChar(AApplicationName), nil, nil, nil, False, 0, nil,
    nil, lStartup, lProcess);
end;

class function TRLNTUtils.RunCommand(const ACommand: string;
  const AWait: Boolean): Boolean;
var
  lProcess: TProcessInformation;
  lStartup: TStartupInfo;
begin
  FillChar(lStartup, SizeOf(TStartupInfo), 0);
  lStartup.cb := SizeOf(TStartupInfo);
  lStartup.dwFlags := STARTF_USESHOWWINDOW;
  lStartup.wShowWindow := SW_HIDE;
  Result := CreateProcess(nil, PChar(ACommand), nil, nil, False, 0, nil, nil,
    lStartup, lProcess);
  if Result and AWait then
    WaitForSingleObject(lProcess.hProcess, INFINITE);
end;

class function TRLNTUtils.RunBatch(const ACommand: string;
  out AOutput: string): Boolean;
const
  nBufferSize = 255;
var
  lAttributes: TSecurityAttributes;
  lReadPipe: THandle;
  lWritePipe: THandle;
  lProcess: TProcessInformation;
  lStartup: TStartupInfo;
  lBuffer: array[0..nBufferSize] of Char;
  nBytes: Cardinal;
  bReaded: Boolean;
begin
  Result := False;
  AOutput := '';
  lAttributes.nLength := SizeOf(lAttributes);
  lAttributes.lpSecurityDescriptor := nil;
  lAttributes.bInheritHandle := True;
  if CreatePipe(lReadPipe, lWritePipe, @lAttributes, 0) then
    try
      FillChar(lStartup, SizeOf(TStartupInfo), 0);
      lStartup.cb := SizeOf(TStartupInfo);
      lStartup.hStdInput := GetStdHandle(STD_INPUT_HANDLE);
      lStartup.hStdOutput := lWritePipe;
      lStartup.hStdError := lWritePipe;
      lStartup.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      lStartup.wShowWindow := SW_HIDE;
      if CreateProcess(nil, PChar(ACommand), nil, nil, True, 0, nil, nil,
        lStartup, lProcess) then
      begin
        if lWritePipe <> 0 then
          CloseHandle(lWritePipe);
        repeat
          nBytes := 0;
          bReaded := ReadFile(lReadPipe, lBuffer, nBufferSize, nBytes, nil);
          if nBytes > 0 then
          begin
            lBuffer[nBytes] := #0;
            { TODO -oRafael : Reimplementar. }
            //OemToAnsi(lBuffer, lBuffer);
            AOutput := AOutput + lBuffer;
          end;
        until not bReaded or (nBytes = 0);

        WaitForSingleObject(lProcess.hProcess, INFINITE);
        Result := True;
      end;
    finally
      if lReadPipe <> 0 then
        CloseHandle(lReadPipe);
    end;
end;

class function TRLNTUtils.ExitComputer(const AFlags: LongWord): Boolean;
var
  nHandle: THandle;
  lNewState: TTokenPrivileges;
  lPreviousState: TTokenPrivileges;
  nReturnLength: DWORD;
begin
  if (Win32Platform = VER_PLATFORM_WIN32_NT) and
    OpenProcessToken(GetCurrentProcess, TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, nHandle) and
    LookupPrivilegeValue(nil, 'SeShutdownPrivilege', lNewState.Privileges[0].Luid) then
  begin
    lNewState.PrivilegeCount := 1;
    lNewState.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
    nReturnLength := 0;
    AdjustTokenPrivileges(nHandle, False, lNewState, SizeOf(lPreviousState),
      lPreviousState, nReturnLength);
  end;

  Result := ExitWindowsEx(AFlags, 0);
end;

class function TRLNTUtils.LogoffComputer: Boolean;
begin
  Result := ExitComputer(EWX_LOGOFF or EWX_FORCE);
end;

class function TRLNTUtils.RestartComputer: Boolean;
begin
  Result := ExitComputer(EWX_REBOOT or EWX_FORCE);
end;

class function TRLNTUtils.ShutdownComputer: Boolean;
begin
  Result := ExitComputer(EWX_POWEROFF or EWX_FORCE);
end;

class function TRLNTUtils.GetLastError: string;
begin
  Result := SysErrorMessage(Windows.GetLastError);
end;

class procedure TRLNTUtils.ClearIECache;
var
  ptrEntryInfo: PInternetCacheEntryInfo;
  nCacheDir: LongWord;
  nEntrySize: LongWord;
begin
  nEntrySize := 0;
  FindFirstUrlCacheEntry(nil, TInternetCacheEntryInfo(nil^), nEntrySize);
  GetMem(ptrEntryInfo, nEntrySize);
  try
    if nEntrySize > 0 then
      ptrEntryInfo^.dwStructSize := nEntrySize;
    nCacheDir := FindFirstUrlCacheEntry(nil, ptrEntryInfo^, nEntrySize);
    try
      if nCacheDir <> 0 then
        repeat
          DeleteUrlCacheEntry(ptrEntryInfo^.lpszSourceUrlName);
          FreeMem(ptrEntryInfo, nEntrySize);
          ptrEntryInfo := nil;
          nEntrySize := 0;
          FindNextUrlCacheEntry(nCacheDir, TInternetCacheEntryInfo(nil^),
            nEntrySize);
          GetMem(ptrEntryInfo, nEntrySize);
          if nEntrySize > 0 then
            ptrEntryInfo^.dwStructSize := nEntrySize;
        until not FindNextUrlCacheEntry(nCacheDir, ptrEntryInfo^, nEntrySize);
    finally
      FindCloseUrlCache(nCacheDir);
    end;
  finally
    if Assigned(ptrEntryInfo) then
      FreeMem(ptrEntryInfo, nEntrySize);
  end;
end;

end.
