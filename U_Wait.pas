//=============================================================================
// U_Wait
// AppKit - Application Development Kit
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Wait;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Collection;

type
  // Enumerated types
  TRLWaitKind = (wkProcessing, wkLoading, wkSaving, wkCalculating, wkPrinting);

  // References
  TRLWaitClass = class of TRLWait;

  // TRLWaitMessage
  TRLWaitMessage = class(TRLCollectionItem)
  private
    FText: string;
    FAllowCancel: Boolean;
  public
    property Text: string read FText write FText;
    property AllowCancel: Boolean read FAllowCancel write FAllowCancel;
  end;

  // TRLWaitMessages
  TRLWaitMessages = class(TRLCollection)
  private
    FKind: TRLWaitKind;
    function GetItem(const AIndex: Integer): TRLWaitMessage;
    function GetHasCancel: Boolean;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TRLWaitMessage;
    function Insert(const AIndex: Integer): TRLWaitMessage;
    property Items[const AIndex: Integer]: TRLWaitMessage read GetItem; default;
    property Kind: TRLWaitKind read FKind write FKind;
    property HasCancel: Boolean read GetHasCancel;
  end;

  // TRLWait
  TRLWait = class(TRLBaseType)
  private
    FShowing: Boolean;
    FMessages: TRLWaitMessages;
    FMessageIndex: Integer;
    function GetMessage: TRLWaitMessage;
  protected
    procedure ShowMessage; virtual; abstract;
    procedure HideMessage; virtual; abstract;
    function MessageCanceled: Boolean; virtual; abstract;
    property Messages: TRLWaitMessages read FMessages;
    property MessageIndex: Integer read FMessageIndex;
    property Message: TRLWaitMessage read GetMessage;
  public
    destructor Destroy; override;
    procedure Show(const AMessage: string); overload; virtual;
    procedure Show(const AMessage: string; const AKind: TRLWaitKind); overload; virtual;
    procedure Show(const AMessage: string; const AKind: TRLWaitKind; const AAllowCancel: Boolean); overload; virtual;
    procedure Show(const AMessages: TRLWaitMessages); overload; virtual;
    function Showing: Boolean; virtual;
    function ShowNext: Boolean; virtual;
    function Canceled: Boolean; virtual;
    procedure Reset; virtual;
    procedure Hide; virtual;
    class function GetInstanceType: TRLWaitClass;
    class procedure SetInstanceType(const AValue: TRLWaitClass);
    class function Instance: TRLWait;
    class procedure DeleteInstance;
  end;

var
  RLWaitKindIcon: array[TRLWaitKind] of string = ('RES_ICO_WK_PROCESSING',
    'RES_ICO_WK_LOADING', 'RES_ICO_WK_SAVING', 'RES_ICO_WK_CALCULATING',
    'RES_ICO_WK_PRINTING');

implementation

var
  RLInstanceType: TRLWaitClass;
  RLInstance: TRLWait = nil;

{ TRLWaitMessages }

function TRLWaitMessages.GetItem(const AIndex: Integer): TRLWaitMessage;
begin
  Result := TRLWaitMessage(inherited Items[AIndex]);
end;

function TRLWaitMessages.GetHasCancel: Boolean;
var
  I: Integer;
begin
  Result := False;
  I := 0;
  while (I <= Pred(Count)) and not Result do
    if Items[I].AllowCancel then
      Result := True
    else
      Inc(I);
end;

function TRLWaitMessages.GetItemClass: TCollectionItemClass;
begin
  Result := TRLWaitMessage;
end;

function TRLWaitMessages.Add: TRLWaitMessage;
begin
  Result := TRLWaitMessage(inherited Add);
end;

function TRLWaitMessages.Insert(const AIndex: Integer): TRLWaitMessage;
begin
  Result := TRLWaitMessage(inherited Insert(AIndex));
end;

{ TRLWait }

destructor TRLWait.Destroy;
begin
  Hide;
  inherited;
end;

function TRLWait.GetMessage: TRLWaitMessage;
begin
  Result := FMessages[FMessageIndex];
end;

class function TRLWait.GetInstanceType: TRLWaitClass;
begin
  Result := RLInstanceType;
end;

class procedure TRLWait.SetInstanceType(const AValue: TRLWaitClass);
begin
  if RLInstanceType <> AValue then
  begin
    if Assigned(RLInstance) then
      FreeAndNil(RLInstance);
    RLInstanceType := AValue;
  end;
end;

procedure TRLWait.Show(const AMessage: string);
begin
  Show(AMessage, wkProcessing);
end;

procedure TRLWait.Show(const AMessage: string; const AKind: TRLWaitKind);
begin
  Show(AMessage, AKind, False);
end;

procedure TRLWait.Show(const AMessage: string; const AKind: TRLWaitKind;
  const AAllowCancel: Boolean);
var
  lMessages: TRLWaitMessages;
  lMessage: TRLWaitMessage;
begin
  lMessages := TRLWaitMessages.Create;
  try
    lMessages.Kind := AKind;
    lMessage := lMessages.Add;
    lMessage.Text := AMessage;
    lMessage.AllowCancel := AAllowCancel;
    Show(lMessages);
  finally
    FreeAndNil(lMessages);
  end;
end;

procedure TRLWait.Show(const AMessages: TRLWaitMessages);
begin
  if Assigned(AMessages) and not FShowing then
  begin
    FShowing := True;
    FMessages := AMessages;
    FMessageIndex := -1;
    ShowNext;
  end;
end;

function TRLWait.Showing: Boolean;
begin
  Result := FShowing;
end;

function TRLWait.ShowNext: Boolean;
begin
  if FShowing then
  begin
    if FMessageIndex < Pred(FMessages.Count) then
      Inc(FMessageIndex)
    else
      FMessageIndex := -1;
    Result := FMessageIndex <> -1;
    if Result then
      ShowMessage;
  end
  else
    Result := False;
end;

function TRLWait.Canceled: Boolean;
begin
  Result := FShowing and MessageCanceled;
end;

procedure TRLWait.Reset;
begin
  if FShowing then
    FMessageIndex := -1;
end;

procedure TRLWait.Hide;
begin
  if FShowing then
  begin
    HideMessage;
    FShowing := False;
    FMessages := nil;
    FMessageIndex := -1;
  end;
end;

class function TRLWait.Instance: TRLWait;
begin
  if not Assigned(RLInstance) and Assigned(RLInstanceType) then
    RLInstance := TRLWait(RLInstanceType.Create);
  Result := RLInstance;
end;

class procedure TRLWait.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLWait.DeleteInstance;

end.
