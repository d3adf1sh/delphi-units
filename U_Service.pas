//=============================================================================
// U_Service
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Service;

interface

uses
  Windows,
  Classes,
  SvcMgr;

type
  // References
  TRLServiceClass = class of TRLService;

  // TRLService
  TRLService = class(TService)
  protected
    procedure ServiceExecute(Sender: TService); virtual;
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer); override;
    function GetServiceController: TServiceController; override;
  end;

var
  RLService: TRLService = nil;

implementation

uses
  U_NTUtils;

// Utils
procedure ServiceController(ACtrlCode: DWord); stdcall;
begin
  if Assigned(RLService) then
    RLService.Controller(ACtrlCode);
end;

{ TRLService }

constructor TRLService.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
  inherited;
  Name := TRLNTUtils.GetNameOfFileWithoutExtension(ParamStr(0));
  AllowPause := False;
  OnExecute := ServiceExecute;
end;

procedure TRLService.ServiceExecute(Sender: TService);
begin
  while not Terminated do
  begin
    ServiceThread.ProcessRequests(False);
    Sleep(1000);
  end;
end;

function TRLService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

end.
