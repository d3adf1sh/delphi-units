//=============================================================================
// U_BaseType
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_BaseType;

interface

uses
  Classes;

type
  // References
  TRLBaseTypeClass = class of TRLBaseType;

  // TRLBaseType
  TRLBaseType = class(TPersistent, IInterface)
  private
    FDestroyed: Boolean;
  protected
    property Destroyed: Boolean read FDestroyed;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

implementation

{ TRLBaseType }

constructor TRLBaseType.Create;
begin
  inherited Create;
end;

destructor TRLBaseType.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

function TRLBaseType.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TRLBaseType._AddRef: Integer;
begin
  Result := -1;
end;

function TRLBaseType._Release: Integer;
begin
  Result := -1;
end;

end.

