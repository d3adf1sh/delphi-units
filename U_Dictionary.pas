 //=============================================================================
// U_Dictionary
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Dictionary;

{$I RL.INC}

interface

uses
  SysUtils,
  Classes,
  XMLIntf,
  U_BaseType,
  U_StringList,
  U_KeyedCollection,
  U_Serializer;

type
  // TRLDictionaryItem
  TRLDictionaryItem = class(TRLKeyedCollectionItem)
  private
    FValue: string;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
  public
    procedure Assign(Source: TPersistent); override;
    property Key: string read GetKey write SetKey;
    property Value: string read FValue write FValue;
  end;

  // TRLDictionaryItems
  TRLDictionaryItems = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLDictionaryItem;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TRLDictionaryItem;
    function Find(const AKey: string): TRLDictionaryItem;
    property Items[const AIndex: Integer]: TRLDictionaryItem read GetItem; default;
  end;

  // TRLDictionary
  TRLDictionary = class(TRLBaseType, IRLSerializer)
  private
    FSeparator: Char;
    FKeyValueSeparator: Char;
    FItems: TRLDictionaryItems;
    function GetHashSize: Integer;
    procedure SetHashSize(const AValue: Integer);
    function GetCaseSensitive: Boolean;
    procedure SetCaseSensitive(const AValue: Boolean);
    function GetCount: Integer;
    function GetKey(const AIndex: Integer): string;
    function GetValue(const AKey: string): string;
    procedure SetValue(const AKey, AValue: string);
    function GetValueFromIndex(const AIndex: Integer): string;
    procedure SetValueFromIndex(const AIndex: Integer; const AValue: string);
  protected
    function GetItemIdentifier: string; virtual;
    function GetItemsIdentifier: string; virtual;
    function GetKeyIdentifier: string; virtual;
    function GetValueIdentifier: string; virtual;
    function ValueToXML(const AKey, AValue: string): string; virtual;
    function ValueFromXML(const AKey, AValue: string): string; virtual;
    property Items: TRLDictionaryItems read FItems;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Add(const AKey, AValue: string); virtual;
    function IndexOfKey(const AKey: string): Integer; virtual;
    function ContainsKey(const AKey: string): Boolean; virtual;
    function TryGetValue(const AKey: string; out AValue: string): Boolean; virtual;
    procedure Remove(const AKey: string); virtual;
    procedure Clear; virtual;
{$IFDEF D15UP}
    function ToString: string; reintroduce; virtual;
{$ELSE}
    function ToString: string; virtual;
{$ENDIF}
    procedure FromString(const AValue: string); virtual;
    procedure ToStrings(const AStrings: TStrings); virtual;
    procedure FromStrings(const AStrings: TStrings); virtual;
    procedure ToXML(const ANode: IXMLNode); virtual;
    procedure FromXML(const ANode: IXMLNode); virtual;
    property HashSize: Integer read GetHashSize write SetHashSize;
    property CaseSensitive: Boolean read GetCaseSensitive write SetCaseSensitive;
    property Separator: Char read FSeparator write FSeparator;
    property KeyValueSeparator: Char read FKeyValueSeparator write FKeyValueSeparator;
    property Count: Integer read GetCount;
    property Keys[const AIndex: Integer]: string read GetKey;
    property Values[const AKey: string]: string read GetValue write SetValue; default;
    property ValueFromIndex[const AIndex: Integer]: string read GetValueFromIndex write SetValueFromIndex;
  end;

implementation

uses
  StrUtils,
  Variants,
  U_System,
  U_RTTIContext;
  
{ TRLDictionaryItem }

procedure TRLDictionaryItem.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TRLDictionaryItem then
    Value := TRLDictionaryItem(Source).Value;
end;

function TRLDictionaryItem.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TRLDictionaryItem.SetKey(const AValue: string);
begin
  inherited;
end;

{ TRLDictionaryItems }

function TRLDictionaryItems.GetItem(const AIndex: Integer): TRLDictionaryItem;
begin
  Result := TRLDictionaryItem(inherited Items[AIndex]);
end;

function TRLDictionaryItems.GetItemClass: TCollectionItemClass;
begin
  Result := TRLDictionaryItem;
end;

function TRLDictionaryItems.Add: TRLDictionaryItem;
begin
  Result := TRLDictionaryItem(inherited Add);
end;

function TRLDictionaryItems.Find(const AKey: string): TRLDictionaryItem;
begin
  Result := TRLDictionaryItem(inherited Find(AKey));
end;

{ TRLDictionary }

constructor TRLDictionary.Create;
begin
  inherited;
  FItems := TRLDictionaryItems.Create;
  FItems.CaseSensitive := False;
  FSeparator := ';';
  FKeyValueSeparator := '=';
end;

destructor TRLDictionary.Destroy;
begin
  FreeAndNil(FItems);
  inherited;
end;

function TRLDictionary.GetHashSize: Integer;
begin
  Result := FItems.HashSize;
end;

procedure TRLDictionary.SetHashSize(const AValue: Integer);
begin
  FItems.HashSize := AValue;
end;

function TRLDictionary.GetCaseSensitive: Boolean;
begin
  Result := FItems.CaseSensitive;
end;

procedure TRLDictionary.SetCaseSensitive(const AValue: Boolean);
begin
  FItems.CaseSensitive := AValue;
end;

function TRLDictionary.GetCount: Integer;
begin
  Result := FItems.Count;
end;

function TRLDictionary.GetKey(const AIndex: Integer): string;
begin
  Result := FItems[AIndex].Key;
end;

function TRLDictionary.GetValue(const AKey: string): string;
begin
  TryGetValue(AKey, Result);
end;

procedure TRLDictionary.SetValue(const AKey, AValue: string);
begin
  Add(AKey, AValue);
end;

function TRLDictionary.GetValueFromIndex(const AIndex: Integer): string;
begin
  Result := FItems[AIndex].Value;
end;

procedure TRLDictionary.SetValueFromIndex(const AIndex: Integer;
  const AValue: string);
begin
  FItems[AIndex].Value := AValue;
end;

function TRLDictionary.GetItemIdentifier: string;
begin
  Result := 'Item';
end;

function TRLDictionary.GetItemsIdentifier: string;
begin
  Result := 'Items';
end;

function TRLDictionary.GetKeyIdentifier: string;
begin
  Result := 'Key';
end;

function TRLDictionary.GetValueIdentifier: string;
begin
  Result := 'Value';
end;

function TRLDictionary.ValueToXML(const AKey, AValue: string): string;
begin
  Result := AValue;
end;

function TRLDictionary.ValueFromXML(const AKey, AValue: string): string;
begin
  Result := AValue;
end;

procedure TRLDictionary.Assign(Source: TPersistent);
var
  lSource: TRLDictionary;
begin
  if Source is TRLDictionary then
  begin
    lSource := TRLDictionary(Source);
    HashSize := lSource.HashSize;
    CaseSensitive := lSource.CaseSensitive;
    Separator := lSource.Separator;
    KeyValueSeparator := lSource.KeyValueSeparator;
    Items.Assign(lSource.Items);
  end;
end;

procedure TRLDictionary.Add(const AKey, AValue: string);
var
  lItem: TRLDictionaryItem;
begin
  lItem := FItems.Find(AKey);
  if not Assigned(lItem) then
  begin
    lItem := FItems.Add;
    lItem.Key := AKey;
  end;

  lItem.Value := AValue;
end;

function TRLDictionary.IndexOfKey(const AKey: string): Integer;
var
  lItem: TRLDictionaryItem;
begin
  lItem := FItems.Find(AKey);
  if Assigned(lItem) then
    Result := lItem.Index
  else
    Result := -1;
end;

function TRLDictionary.ContainsKey(const AKey: string): Boolean;
begin
  Result := FItems.Contains(AKey);
end;

function TRLDictionary.TryGetValue(const AKey: string;
  out AValue: string): Boolean;
var
  lItem: TRLDictionaryItem;
begin
  lItem := FItems.Find(AKey);
  Result := Assigned(lItem);
  if Result then
    AValue := lItem.Value
  else
    AValue := '';
end;

procedure TRLDictionary.Remove(const AKey: string);
begin
  FItems.Remove(AKey);
end;

procedure TRLDictionary.Clear;
begin
  FItems.Clear;
end;

function TRLDictionary.ToString: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to Pred(Count) do
    Result := Result + Keys[I] + FKeyValueSeparator + ValueFromIndex[I] +
      IfThen(I < Pred(Count), FSeparator + ' ');
end;

procedure TRLDictionary.FromString(const AValue: string);
var
  I: Integer;
  sName: string;
  sValue: string;
  bName: Boolean;
  bValue: Boolean;
begin
  sName := '';
  sValue := '';
  bName := True;
  bValue := False;
  Clear;
  for I := LowCharIndex to HighCharIndex(AValue) do
  begin
    if not U_System.CharInSet(AValue[I], [FKeyValueSeparator, FSeparator]) then
    begin
      if bName then
        sName := sName + AValue[I];
      if bValue then
        sValue := sValue + AValue[I];
    end
    else
      if AValue[I] = FKeyValueSeparator then
      begin
        bName := False;
        bValue := True;
      end;

    if (AValue[I] = FSeparator) or (I = HighCharIndex(AValue)) then
    begin
      sName := Trim(sName);
      if sName <> '' then
        Values[sName] := sValue;
      sName := '';
      sValue := '';
      bName := True;
      bValue := False;
    end;
  end;
end;

procedure TRLDictionary.ToStrings(const AStrings: TStrings);
var
  I: Integer;
begin
  if Assigned(AStrings) then
  begin
    AStrings.BeginUpdate;
    try
      AStrings.Clear;
      for I := 0 to Pred(Count) do
        AStrings.Add(Keys[I] + AStrings.NameValueSeparator +
          ValueFromIndex[I]);
    finally
      AStrings.EndUpdate;
    end;
  end;
end;

procedure TRLDictionary.FromStrings(const AStrings: TStrings);
var
  I: Integer;
begin
  if Assigned(AStrings) then
  begin
    Clear;
    for I := 0 to Pred(AStrings.Count) do
      Values[AStrings.Names[I]] := AStrings.ValueFromIndex[I];
  end;
end;

procedure TRLDictionary.ToXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  lNode: IXMLNode;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesToXML(Self, ANode);
    lNodes := ANode.AddChild(GetItemsIdentifier);
    for I := 0 to Pred(Count) do
    begin
      lNode := lNodes.AddChild(GetItemIdentifier);
      lNode.Attributes[GetKeyIdentifier] := Keys[I];
      lNode.Attributes[GetValueIdentifier] := ValueToXML(Keys[I],
        ValueFromIndex[I]);
    end;
  end;
end;

procedure TRLDictionary.FromXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  sKey: string;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesFromXML(Self, ANode);
    lNodes := ANode.ChildNodes.FindNode(GetItemsIdentifier);
    if Assigned(lNodes) then
    begin
      Clear;
      for I := 0 to Pred(lNodes.ChildNodes.Count) do
      begin
        sKey := VarToStr(lNodes.ChildNodes[I].Attributes[GetKeyIdentifier]);
        Values[sKey] := ValueFromXML(sKey,
          VarToStr(lNodes.ChildNodes[I].Attributes[GetValueIdentifier]));
      end;
    end;
  end;
end;

end.
