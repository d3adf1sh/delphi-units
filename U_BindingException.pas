//=============================================================================
// U_BindingException
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_BindingException;

interface

uses
  U_FormException;

type
  // ERLBindingException
  ERLBindingException = class(ERLFormException)
  private
    FBindingName: string;
  public
    constructor CreateBindingException(const AMessage, ABindingName: string); overload; virtual;
    constructor CreateBindingException(const AMessage, ABindingName, ADetails: string); overload; virtual;
    property BindingName: string read FBindingName write FBindingName;
  end;

implementation

{ ERLBindingException }

constructor ERLBindingException.CreateBindingException(const AMessage,
  ABindingName: string);
begin
  CreateBindingException(AMessage, ABindingName, '');
end;

constructor ERLBindingException.CreateBindingException(const AMessage,
  ABindingName, ADetails: string);
begin
  inherited CreateFormException(AMessage, ADetails, fekWarning);
  FBindingName := ABindingName;
end;

end.
