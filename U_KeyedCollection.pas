//=============================================================================
// U_KeyedCollection
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_KeyedCollection;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_HashTable,
  U_StringList,
  U_Collection,
  U_Synchronizer;

type
  // Exceptions
  ERLItemKeyAlreadyExists = class(ERLException);
  ERLItemAlternativeKeyAlreadyExists = class(ERLException);

  // TRLKeyedCollectionItem
  TRLKeyedCollectionItem = class(TRLCollectionItem)
  private
    FKey: string;
    FAlternativeKey: string;
    FKeyUpdateCount: Integer;
    FAllowUpdateKey: Boolean;
    FAlternativeKeyUpdateCount: Integer;
    FAllowUpdateAlternativeKey: Boolean;
    function GetHash: TRLHashTable;
    function GetAlternativeHash: TRLHashTable;
  protected
    procedure BeginKeyUpdate; virtual;
    procedure EndKeyUpdate; virtual;
    function InKeyUpdate: Boolean; virtual;
    procedure UpdateKey; virtual;
    function MakeKey: string; virtual;
    function GetKey: string; virtual;
    procedure SetKey(const AValue: string); virtual;
    procedure KeyChanged; virtual;
    procedure BeginAlternativeKeyUpdate; virtual;
    procedure EndAlternativeKeyUpdate; virtual;
    function InAlternativeKeyUpdate: Boolean; virtual;
    procedure UpdateAlternativeKey; virtual;
    function MakeAlternativeKey: string; virtual;
    function GetAlternativeKey: string; virtual;
    procedure SetAlternativeKey(const AValue: string); virtual;
    procedure KeyAlreadyExists(const AValue: string); virtual;
    procedure AlternativeKeyAlreadyExists(const AValue: string); virtual;
    procedure AlternativeKeyChanged; virtual;
    property Hash: TRLHashTable read GetHash;
    property AlternativeHash: TRLHashTable read GetAlternativeHash;
    property KeyUpdateCount: Integer read FKeyUpdateCount;
    property AllowUpdateKey: Boolean read FAllowUpdateKey;
    property AlternativeKeyUpdateCount: Integer read FAlternativeKeyUpdateCount;
    property AllowUpdateAlternativeKey: BOolean read FAllowUpdateAlternativeKey;
  public
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure BeginUpdate; override;
    procedure EndUpdate; override;
  end;

  // TRLKeyedCollectionItemArray
  TRLKeyedCollectionItemArray = class(TRLCollectionItemArray)
  private
    function GetItem(const AIndex: Integer): TRLKeyedCollectionItem;
    procedure SetItem(const AIndex: Integer; const AValue: TRLKeyedCollectionItem);
  public
    property Items[const AIndex: Integer]: TRLKeyedCollectionItem read GetItem write SetItem; default;
  end;

  // TRLKeyedCollection
  TRLKeyedCollection = class(TRLCollection, IRLSynchronizer)
  private
    FHash: TRLHashTable;
    FAlternativeHash: TRLHashTable;
    function GetAlternativeHash: TRLHashTable;
    function GetItem(const AIndex: Integer): TRLKeyedCollectionItem;
    function GetHashSize: Integer;
    procedure SetHashSize(const AValue: Integer);
    function GetCaseSensitive: Boolean;
    procedure SetCaseSensitive(const AValue: Boolean);
  protected
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
    function CreateHash: TRLHashTable; virtual;
    function CreateAlternativeHash: TRLHashTable; virtual;
    procedure HashSizeChanged; virtual;
    procedure CaseSensitiveChanged; virtual;
    function FindHash(const AKey: string): TRLKeyedCollectionItem; virtual;
    function FindAlternativeHash(const AKey: string): TRLKeyedCollectionItem; virtual;
    property Hash: TRLHashTable read FHash;
    property AlternativeHash: TRLHashTable read GetAlternativeHash;
  public
    constructor CreateParented(const AParent: TObject); override;
    destructor Destroy; override;
    function Find(const AKey: string): TRLKeyedCollectionItem;
    function Contains(const AKey: string): Boolean; virtual;
    function Remove(const AKey: string): Boolean; virtual;
    procedure Synchronize(const ASource: TObject); virtual;
    function ToArray: TRLKeyedCollectionItemArray; overload;
    function ToArray(const AKeys: TRLStringList): TRLKeyedCollectionItemArray; overload;
    property Items[const AIndex: Integer]: TRLKeyedCollectionItem read GetItem;
    property HashSize: Integer read GetHashSize write SetHashSize;
    property CaseSensitive: Boolean read GetCaseSensitive write SetCaseSensitive;
  end;
                             
implementation

uses
  U_RTTIContext;

{ TRLKeyedCollectionItem }

destructor TRLKeyedCollectionItem.Destroy;
begin
  if (FKey <> '') and Assigned(Hash) then
    Hash.Remove(FKey);
  if (FAlternativeKey <> '') and Assigned(AlternativeHash) then
    AlternativeHash.Remove(FAlternativeKey);
  inherited;
end;

function TRLKeyedCollectionItem.GetHash: TRLHashTable;
begin
  if Collection is TRLKeyedCollection then
    Result := TRLKeyedCollection(Collection).Hash
  else
    Result := nil;
end;

function TRLKeyedCollectionItem.GetAlternativeHash: TRLHashTable;
begin
  if Collection is TRLKeyedCollection then
    Result := TRLKeyedCollection(Collection).AlternativeHash
  else
    Result := nil;
end;

procedure TRLKeyedCollectionItem.BeginKeyUpdate;
begin
  Inc(FKeyUpdateCount);
  if FKeyUpdateCount = 1 then
    FAllowUpdateKey := False;
end;

procedure TRLKeyedCollectionItem.EndKeyUpdate;
begin
  if FKeyUpdateCount > 0 then
  begin
    Dec(FKeyUpdateCount);
    if (FKeyUpdateCount = 0) and FAllowUpdateKey then
      UpdateKey;
  end;
end;

function TRLKeyedCollectionItem.InKeyUpdate: Boolean;
begin
  Result := FKeyUpdateCount > 0;
end;

procedure TRLKeyedCollectionItem.UpdateKey;
begin
  if Assigned(Collection) then
  begin
    if FKeyUpdateCount = 0 then
      SetKey(MakeKey)
    else
      FAllowUpdateKey := True;
  end;
end;

function TRLKeyedCollectionItem.MakeKey: string;
begin
  Result := '';
end;

function TRLKeyedCollectionItem.GetKey: string;
begin
  Result := FKey;
end;

procedure TRLKeyedCollectionItem.SetKey(const AValue: string);
var
  bCaseSensitive: Boolean;
begin
  bCaseSensitive := not Assigned(Hash) or Hash.CaseSensitive;
  if (bCaseSensitive and (AValue <> FKey)) or not SameText(AValue, FKey) then
  begin
    if Assigned(Hash) then
    begin
      if (AValue <> '') and not Hash.Add(AValue, Self) then
        KeyAlreadyExists(AValue);
      if FKey <> '' then
        Hash.Remove(FKey);
    end;

    FKey := AValue;
    KeyChanged;
  end;
end;

procedure TRLKeyedCollectionItem.KeyChanged;
begin
end;

procedure TRLKeyedCollectionItem.BeginAlternativeKeyUpdate;
begin
  Inc(FAlternativeKeyUpdateCount);
  if FAlternativeKeyUpdateCount = 1 then
    FAllowUpdateAlternativeKey := False;
end;

procedure TRLKeyedCollectionItem.EndAlternativeKeyUpdate;
begin
  if FAlternativeKeyUpdateCount > 0 then
  begin
    Dec(FAlternativeKeyUpdateCount);
    if (FAlternativeKeyUpdateCount = 0) and FAllowUpdateAlternativeKey then
      UpdateAlternativeKey;
  end;
end;

function TRLKeyedCollectionItem.InAlternativeKeyUpdate: Boolean;
begin
  Result := FAlternativeKeyUpdateCount > 0;
end;

procedure TRLKeyedCollectionItem.UpdateAlternativeKey;
begin
  if Assigned(Collection) then
  begin
    if FAlternativeKeyUpdateCount = 0 then
      SetAlternativeKey(MakeAlternativeKey)
    else
      FAllowUpdateAlternativeKey := True;
  end;
end;

function TRLKeyedCollectionItem.MakeAlternativeKey: string;
begin
  Result := '';
end;

function TRLKeyedCollectionItem.GetAlternativeKey: string;
begin
  Result := FAlternativeKey;
end;

procedure TRLKeyedCollectionItem.SetAlternativeKey(const AValue: string);
var
  bCaseSensitive: Boolean;
begin
  bCaseSensitive := not Assigned(AlternativeHash) or
    AlternativeHash.CaseSensitive;
  if (bCaseSensitive and (AValue <> FAlternativeKey)) or
    not SameText(AValue, FAlternativeKey) then
  begin
    if Assigned(AlternativeHash) then
    begin
      if (AValue <> '') and not AlternativeHash.Add(AValue, Self) then
        AlternativeKeyAlreadyExists(AValue);
      if FAlternativeKey <> '' then
        AlternativeHash.Remove(FAlternativeKey);
    end;

    FAlternativeKey := AValue;
    AlternativeKeyChanged;
  end;
end;

procedure TRLKeyedCollectionItem.KeyAlreadyExists(const AValue: string);
begin
  raise ERLItemKeyAlreadyExists.CreateFmt('Item key "%s" already exists.',
    [AValue]);
end;

procedure TRLKeyedCollectionItem.AlternativeKeyAlreadyExists(
  const AValue: string);
begin
  raise ERLItemAlternativeKeyAlreadyExists.CreateFmt(
    'Item alternative key "%s" already exists.', [AValue]);
end;

procedure TRLKeyedCollectionItem.AlternativeKeyChanged;
begin
end;

procedure TRLKeyedCollectionItem.Assign(Source: TPersistent);
var
  lItem: TRLKeyedCollectionItem;
begin
  if Source is TRLKeyedCollectionItem then
  begin
    lItem := TRLKeyedCollectionItem(Source);
    SetKey(lItem.GetKey);
    SetAlternativeKey(lItem.GetAlternativeKey);
  end;
end;

procedure TRLKeyedCollectionItem.BeginUpdate;
begin
  inherited;
  BeginKeyUpdate;
  BeginAlternativeKeyUpdate;
end;

procedure TRLKeyedCollectionItem.EndUpdate;
begin
  EndAlternativeKeyUpdate;
  EndKeyUpdate;
  inherited;
end;

{ TRLKeyedCollectionItemArray }

function TRLKeyedCollectionItemArray.GetItem(
  const AIndex: Integer): TRLKeyedCollectionItem;
begin
  Result := TRLKeyedCollectionItem(inherited Items[AIndex]);
end;

procedure TRLKeyedCollectionItemArray.SetItem(const AIndex: Integer;
  const AValue: TRLKeyedCollectionItem);
begin
  inherited Items[AIndex] := AValue;
end;

{ TRLKeyedCollection }

constructor TRLKeyedCollection.CreateParented(const AParent: TObject);
begin
  inherited;
  FHash := CreateHash;
end;

destructor TRLKeyedCollection.Destroy;
begin
  if Assigned(FAlternativeHash) then
    FreeAndNil(FAlternativeHash);
  FreeAndNil(FHash);
  inherited;
end;

function TRLKeyedCollection.GetAlternativeHash: TRLHashTable;
begin
  if not Assigned(FAlternativeHash) and not Destroyed then
    FAlternativeHash := CreateAlternativeHash;
  Result := FAlternativeHash;
end;

function TRLKeyedCollection.GetItem(
  const AIndex: Integer): TRLKeyedCollectionItem;
begin
  Result := TRLKeyedCollectionItem(inherited Items[AIndex]);
end;

function TRLKeyedCollection.GetHashSize: Integer;
begin
  Result := FHash.Size;
end;

procedure TRLKeyedCollection.SetHashSize(const AValue: Integer);
begin
  if AValue <> FHash.Size then
  begin
    Clear;
    FHash.Size := AValue;
    if Assigned(FAlternativeHash) then
      FAlternativeHash.Size := AValue;
    HashSizeChanged;
  end;
end;

function TRLKeyedCollection.GetCaseSensitive: Boolean;
begin
  Result := FHash.CaseSensitive;
end;

procedure TRLKeyedCollection.SetCaseSensitive(const AValue: Boolean);
begin
  if AValue <> FHash.CaseSensitive then
  begin
    Clear;
    FHash.CaseSensitive := AValue;
    CaseSensitiveChanged;
  end;
end;

function TRLKeyedCollection.GetItemClass: TCollectionItemClass;
begin
  Result := TRLKeyedCollectionItem;
end;

function TRLKeyedCollection.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLKeyedCollectionItemArray;
end;

function TRLKeyedCollection.CreateHash: TRLHashTable;
begin
  Result := TRLHashTable.Create;
end;

function TRLKeyedCollection.CreateAlternativeHash: TRLHashTable;
begin
  Result := TRLHashTable.Create;
  Result.Size := HashSize;
end;

procedure TRLKeyedCollection.HashSizeChanged;
begin
end;

procedure TRLKeyedCollection.CaseSensitiveChanged;
begin
end;

function TRLKeyedCollection.FindHash(
  const AKey: string): TRLKeyedCollectionItem;
var
  lItem: TObject;
begin
  if FHash.Find(AKey, lItem) then
    Result := TRLKeyedCollectionItem(lItem)
  else
    Result := nil;
end;

function TRLKeyedCollection.FindAlternativeHash(
  const AKey: string): TRLKeyedCollectionItem;
var
  lItem: TObject;
begin
  if Assigned(FAlternativeHash) and FAlternativeHash.Find(AKey, lItem) then
    Result := TRLKeyedCollectionItem(lItem)
  else
    Result := nil;
end;

function TRLKeyedCollection.Find(const AKey: string): TRLKeyedCollectionItem;
begin
  Result := FindHash(AKey);
end;

function TRLKeyedCollection.Contains(const AKey: string): Boolean;
begin
  Result := Assigned(Find(AKey));
end;

function TRLKeyedCollection.Remove(const AKey: string): Boolean;
var
  lItem: TRLKeyedCollectionItem;
begin
  lItem := Find(AKey);
  Result := Assigned(lItem);
  if Result then
    Delete(lItem.Index);
end;

procedure TRLKeyedCollection.Synchronize(const ASource: TObject);
var
  lSource: TRLKeyedCollection;
  lItem: TCollectionItem;
  I: Integer;
begin
  if ASource is TRLKeyedCollection then
  begin
    lSource := TRLKeyedCollection(ASource);
    for I := 0 to Pred(lSource.Count) do
    begin
      lItem := Find(lSource.Items[I].GetKey);
      if not Assigned(lItem) then
        lItem := Add;
      TRLRTTIContext.Instance.Synchronize(lItem, lSource.Items[I]);
    end;

    I := 0;
    while I < Count do
      if not lSource.Contains(Items[I].GetKey) then
        Delete(I)
      else
        Inc(I);
  end;
end;

function TRLKeyedCollection.ToArray: TRLKeyedCollectionItemArray;
begin
  Result := TRLKeyedCollectionItemArray(inherited ToArray);
end;

function TRLKeyedCollection.ToArray(
  const AKeys: TRLStringList): TRLKeyedCollectionItemArray;
var
  lItem: TRLKeyedCollectionItem;
  I: Integer;  
begin
  if Assigned(AKeys) then
  begin
    Result := TRLKeyedCollectionItemArray(GetItemArrayClass.Create);
    for I := 0 to Pred(AKeys.Count) do
    begin
      lItem := FindHash(AKeys[I]);
      if Assigned(lItem) then
        Result.Add(lItem);
    end;
  end
  else
    Result := nil;
end;

end.
