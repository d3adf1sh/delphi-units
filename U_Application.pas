//=============================================================================
// U_Application
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Application;

{$WARNINGS OFF W1002}

interface

uses
  SysUtils,
  SvcMgr,
  Forms,
  U_BaseType,
  U_Dictionary,
  U_Modules,
  U_Service;

const
  CAppParameter = 'App';
  CQuitParameter = 'Quit';

type
  // TRLApplication
  TRLApplication = class(TRLBaseType)
  private
    FFormClass: TFormClass;
    FServiceClass: TRLServiceClass;
    FFileLocation: string;
    FParameters: TRLDictionary;
    FModules: TRLModules;
    FModulesFileName: string;
    FAutoLoadModules: Boolean;
    function GetTitle: string;
    procedure SetTitle(const AValue: string);
    function GetMainForm: TForm;
    function GetFileName: string;
    function GetFileLocation: string;
    function GetParameters: TRLDictionary;
    function GetModules: TRLModules;
  protected
    function CreateParameters: TRLDictionary; virtual;
    function CreateModules: TRLModules; virtual;
    function CanInitialize: Boolean; virtual;
    procedure WriteConsole(const AValue: string); virtual;
    procedure ReadParameters; virtual;
    procedure ReadModules; virtual;
    procedure SaveModules; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    function Initialize: Boolean; virtual;
    procedure Prepare; virtual;                       
    function Run: Boolean; virtual;
    function IsApplication: Boolean; virtual;
    function IsService: Boolean; virtual;
    function GetParameter(const AName: string): string; virtual;
    function TryGetParameter(const AName: string; out AValue: string): Boolean; virtual;
    function ContainsParameter(const AName: string): Boolean; virtual;
    property FormClass: TFormClass read FFormClass write FFormClass;
    property ServiceClass: TRLServiceClass read FServiceClass write FServiceClass;
    property Title: string read GetTitle write SetTitle;
    property MainForm: TForm read GetMainForm;
    property FileName: string read GetFileName;
    property FileLocation: string read GetFileLocation;
    property Parameters: TRLDictionary read GetParameters;
    property Modules: TRLModules read GetModules;
    property ModulesFileName: string read FModulesFileName write FModulesFileName;
    property AutoLoadModules: Boolean read FAutoLoadModules write FAutoLoadModules;
    class procedure RunApplication(const AFormClass: TFormClass; const AServiceClass: TRLServiceClass);
    class function Instance: TRLApplication;
    class procedure DeleteInstance;
  end;

implementation

uses
  U_RTTIContext,
  U_NTUtils;

var
  RLInstance: TRLApplication = nil;

{ TRLApplication }

constructor TRLApplication.Create;
begin
  inherited;
  FModulesFileName := ChangeFileExt(FileName, '.Modules.xml');
  FAutoLoadModules := True;
end;

destructor TRLApplication.Destroy;
begin
  if Assigned(FParameters) then
    FreeAndNil(FParameters);
  if Assigned(FModules) then
    FreeAndNil(FModules);
  inherited;
end;

function TRLApplication.GetTitle: string;
begin
  Result := Forms.Application.Title;
end;

procedure TRLApplication.SetTitle(const AValue: string);
begin
  Forms.Application.Title := AValue;
end;

function TRLApplication.GetMainForm: TForm;
begin
  if IsApplication then
    Result := Forms.Application.MainForm
  else
    Result := nil;
end;

function TRLApplication.GetFileName: string;
begin
  Result := ParamStr(0);
end;

function TRLApplication.GetFileLocation: string;
begin
  if FFileLocation = '' then
    FFileLocation := TRLNTUtils.GetPathOfFile(FileName);
  Result := FFileLocation;
end;

function TRLApplication.GetParameters: TRLDictionary;
begin
  if not Assigned(FParameters) then
    FParameters := CreateParameters;
  Result := FParameters;
end;

function TRLApplication.GetModules: TRLModules;
begin
  if not Assigned(FModules) then
    FModules := CreateModules;
  Result := FModules;
end;

function TRLApplication.CreateParameters: TRLDictionary;
begin
  Result := TRLDictionary.Create;
  Result.HashSize := 16;
  Result.CaseSensitive := False;
end;

function TRLApplication.CreateModules: TRLModules;
begin
  Result := TRLModules.CreateParented(nil);
end;

function TRLApplication.CanInitialize: Boolean;
begin
  Result := True;
end;

procedure TRLApplication.WriteConsole(const AValue: string);
begin
  WriteLn(AValue);
end;

procedure TRLApplication.ReadParameters;
var
  I: Integer;
  sParam: string;
begin
  for I := 1 to ParamCount do
  begin
    sParam := ParamStr(I);
    if sParam[1] in SwitchChars then
    begin
      Delete(sParam, 1, 1);
      Parameters.Add(sParam, '');
    end
    else
    begin
      if Parameters.Count > 0 then
        Parameters.ValueFromIndex[Pred(Parameters.Count)] := sParam;
    end;
  end;
end;

procedure TRLApplication.ReadModules;
begin
  if FileExists(FModulesFileName) then
    TRLRTTIContext.Instance.FromFile(Modules, FModulesFileName, sfXML);
end;

procedure TRLApplication.SaveModules;
begin
  TRLRTTIContext.Instance.ToFile(Modules, FModulesFileName, sfXML);
end;

function TRLApplication.Initialize: Boolean;
begin
  ReadParameters;
  ReadModules;
  Result := CanInitialize;
  if Result then
  begin
    if Assigned(FServiceClass) and not ContainsParameter(CAppParameter) and
      (DebugHook = 0) then
      RLService := FServiceClass.CreateNew(SvcMgr.Application, 0);
    if IsApplication then
      Forms.Application.Initialize
    else
      if IsService then
        SvcMgr.Application.Initialize;
    if Assigned(FModules) and FAutoLoadModules then
      FModules.LoadAll(False);
  end;
end;

procedure TRLApplication.Prepare;
var
  ptrForm: ^TForm;
begin
  if IsApplication and not ContainsParameter(CQuitParameter) then
  begin
    ptrForm := @Forms.Application.MainForm;
    ptrForm^ := FFormClass.Create(Forms.Application);
  end;
end;

function TRLApplication.Run: Boolean;
begin
  Result := not ContainsParameter(CQuitParameter);
  if Result then
  begin
    if IsApplication then
      Forms.Application.Run
    else
      if IsService then
        SvcMgr.Application.Run;
  end;
end;

function TRLApplication.IsApplication: Boolean;
begin
  Result := not IsService and Assigned(FFormClass);
end;

function TRLApplication.IsService: Boolean;
begin
  Result := Assigned(RLService);
end;

function TRLApplication.GetParameter(const AName: string): string;
begin
  if Assigned(FParameters) then
    FParameters.TryGetValue(AName, Result)
  else
    Result := '';
end;

function TRLApplication.TryGetParameter(const AName: string;
  out AValue: string): Boolean;
begin
  if Assigned(FParameters) then
    Result := FParameters.TryGetValue(AName, AValue)
  else
  begin
    Result := False;
    AValue := '';
  end;
end;

function TRLApplication.ContainsParameter(const AName: string): Boolean;
begin
  Result := Assigned(FParameters) and FParameters.ContainsKey(AName);
end;

class procedure TRLApplication.RunApplication(const AFormClass: TFormClass;
  const AServiceClass: TRLServiceClass);
begin
  if not Assigned(RLInstance) then
  begin
    RLInstance := Self.Create;
    RLInstance.FormClass := AFormClass;
    RLInstance.ServiceClass := AServiceClass;
    if RLInstance.Initialize then
    begin
      RLInstance.Prepare;
      RLInstance.Run;
    end;
  end;
end;

class function TRLApplication.Instance: TRLApplication;
begin
  if not Assigned(RLInstance) then
    RLInstance := Self.Create;
  Result := RLInstance;
end;

class procedure TRLApplication.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLApplication.DeleteInstance;

end.
