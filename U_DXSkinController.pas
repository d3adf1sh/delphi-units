//=============================================================================
// U_DXSkinController
// AppKit - Application Development Kit
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DXSkinController;

interface

uses
  SysUtils,
  Classes,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  dxSkinCaramel,
  dxSkiniMaginary,
  dxSkinOffice2007Black,
  dxSkinOffice2007Blue,
  dxSkinOffice2007Green,
  dxSkinOffice2007Pink,
  dxSkinOffice2007Silver,
  dxSkinsForm,
  dxSkinsLookAndFeelPainter,
  U_BaseType;

type
  // TRLDXSkinController
  TRLDXSkinController = class(TRLBaseType)
  private
    FController: TdxSkinController;
    function GetSkinName: string;
    procedure SetSkinName(const AValue: string);
    function GetActive: Boolean;
    procedure SetActive(const AValue: Boolean);
  protected
    function CreateController: TdxSkinController; virtual;
    property Controller: TdxSkinController read FController;
  public
    constructor Create; override;
    destructor Destroy; override;
    function GetSkinNames: TStrings; virtual;
    procedure ReadSkinNames(const ASkinNames: TStrings); virtual;
    property SkinName: string read GetSkinName write SetSkinName;
    property Active: Boolean read GetActive write SetActive;
    class function Instance: TRLDXSkinController;
    class procedure DeleteInstance;
  end;

implementation

var
  FInstance: TRLDXSkinController = nil;
  
constructor TRLDXSkinController.Create;
begin
  inherited;
  FController := CreateController;
end;

destructor TRLDXSkinController.Destroy;
begin
  FreeAndNil(FController);
  inherited;
end;

function TRLDXSkinController.GetSkinName: string;
begin
  Result := FController.SkinName;
end;

procedure TRLDXSkinController.SetSkinName(const AValue: string);
begin
  FController.SkinName := AValue;
end;

function TRLDXSkinController.GetActive: Boolean;
begin
  Result := FController.UseSkins;
end;

procedure TRLDXSkinController.SetActive(const AValue: Boolean);
begin
  FController.UseSkins := AValue;
end;

function TRLDXSkinController.CreateController: TdxSkinController;
begin
  Result := TdxSkinController.Create(nil);
  Result.Kind := lfFlat;
  Result.UseSkins := True;
end;

function TRLDXSkinController.GetSkinNames: TStrings;
begin
  Result := TStringList.Create;
  ReadSkinNames(Result);
end;

procedure TRLDXSkinController.ReadSkinNames(const ASkinNames: TStrings);
var
  I: Integer;
begin
  if Assigned(ASkinNames) then
  begin
    ASkinNames.BeginUpdate;
    try
      ASkinNames.Clear;
      for I := 0 to Pred(cxLookAndFeelPaintersManager.Count) do
        if cxLookAndFeelPaintersManager.Items[I].InheritsFrom(
          TdxSkinLookAndFeelPainter) then
          ASkinNames.Add(cxLookAndFeelPaintersManager.Items[I].LookAndFeelName);
    finally
      ASkinNames.EndUpdate;
    end;
  end;
end;

class function TRLDXSkinController.Instance: TRLDXSkinController;
begin
  if not Assigned(FInstance) then
    FInstance := TRLDXSkinController.Create;
  Result := FInstance;
end;

class procedure TRLDXSkinController.DeleteInstance;
begin
  if Assigned(FInstance) then
    FreeAndNil(FInstance);
end;

initialization
  TRLDXSkinController.Instance.SkinName := 'Office2007Black';

finalization
  TRLDXSkinController.DeleteInstance;

end.
