//=============================================================================
// U_BindingAttributes
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_BindingAttributes;

{$I RL.INC}

interface

uses
  Classes,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  U_Exception,
  U_NamedCollection,
  U_RTTIAttributes;

type
  // Exceptions
  ERLBindingAttributeNotFound = class(ERLException);
  ERLBindingAttributeIsInvalid = class(ERLException);

  // Events
  TRLBindingAttributeEvent = procedure(Sender: TObject; Instance: TPersistent; var Value: Variant) of object;

  // TRLBindingAttribute
  TRLBindingAttribute = class(TRLNamedCollectionItem)
  private
    FInstanceType: TClass;
    FProperties: TRLRTTIAttribute;
    FOnReadValue: TRLBindingAttributeEvent;
    FOnWriteValue: TRLBindingAttributeEvent;
    procedure SetInstanceType(const AValue: TClass);
    function GetProperties: TRLRTTIAttribute;
  protected
    procedure KeyChanged; override;
    procedure InstanceTypeChanged; virtual;
    procedure ResetProperties; virtual;
    procedure CheckProperties; virtual;
    procedure DoReadValue(const AInstance: TPersistent; var AValue: Variant); virtual;
    procedure DoWriteValue(const AInstance: TPersistent; var AValue: Variant); virtual;
  public
    constructor Create(Collection: TCollection); override;
    function ReadValue(const AInstance: TPersistent): Variant; virtual;
    function ReadChar(const AInstance: TPersistent): string; virtual;
    function ReadString(const AInstance: TPersistent): string; virtual;
    function ReadInteger(const AInstance: TPersistent): Integer; virtual;
    function ReadInt64(const AInstance: TPersistent): Int64; virtual;
    function ReadFloat(const AInstance: TPersistent): Double; virtual;
    function ReadCurrency(const AInstance: TPersistent): Currency; virtual;
    function ReadDate(const AInstance: TPersistent): TDate; virtual;
    function ReadTime(const AInstance: TPersistent): TTime; virtual;
    function ReadDateTime(const AInstance: TPersistent): TDateTime; virtual;
    function ReadBoolean(const AInstance: TPersistent): Boolean; virtual;
    function ReadVariant(const AInstance: TPersistent): Variant; virtual;
    procedure WriteValue(const AInstance: TPersistent; const AValue: Variant); virtual;
    procedure WriteChar(const AInstance: TPersistent; const AValue: Char); virtual;
    procedure WriteString(const AInstance: TPersistent; const AValue: string); virtual;
    procedure WriteInteger(const AInstance: TPersistent; const AValue: Integer); virtual;
    procedure WriteInt64(const AInstance: TPersistent; const AValue: Int64); virtual;
    procedure WriteFloat(const AInstance: TPersistent; const AValue: Double); virtual;
    procedure WriteCurrency(const AInstance: TPersistent; const AValue: Currency); virtual;
    procedure WriteDate(const AInstance: TPersistent; const AValue: TDate); virtual;
    procedure WriteTime(const AInstance: TPersistent; const AValue: TTime); virtual;
    procedure WriteDateTime(const AInstance: TPersistent; const AValue: TDateTime); virtual;
    procedure WriteBoolean(const AInstance: TPersistent; const AValue: Boolean); virtual;
    procedure WriteVariant(const AInstance: TPersistent; const AValue: Variant); virtual;
    property InstanceType: TClass read FInstanceType write SetInstanceType;
    property Properties: TRLRTTIAttribute read GetProperties;
    property OnReadValue: TRLBindingAttributeEvent read FOnReadValue write FOnReadValue;
    property OnWriteValue: TRLBindingAttributeEvent read FOnWriteValue write FOnWriteValue;
  end;

  // TRLBindingAttributes
  TRLBindingAttributes = class(TRLNamedCollection)
  private
    FInstanceType: TClass;
    function GetItem(const AIndex: Integer): TRLBindingAttribute;
    procedure SetInstanceType(const AValue: TClass);
  protected
    function GetItemClass: TCollectionItemClass; override;
    procedure InstanceTypeChanged; virtual;
  public
    function Add: TRLBindingAttribute;
    function Insert(const AIndex: Integer): TRLBindingAttribute;
    function Find(const AName: string): TRLBindingAttribute;
    function FindByName(const AName: string): TRLBindingAttribute;
    property Items[const AIndex: Integer]: TRLBindingAttribute read GetItem; default;
    property InstanceType: TClass read FInstanceType write SetInstanceType;
  end;

implementation

uses
  U_RTTIContext;
  
{ TRLBindingAttribute }

constructor TRLBindingAttribute.Create(Collection: TCollection);
begin
  inherited;
  if Collection is TRLBindingAttributes then
    FInstanceType := TRLBindingAttributes(Collection).InstanceType;
end;

procedure TRLBindingAttribute.SetInstanceType(const AValue: TClass);
begin
  if AValue <> FInstanceType then
  begin
    FInstanceType := AValue;
    InstanceTypeChanged;
  end;
end;

function TRLBindingAttribute.GetProperties: TRLRTTIAttribute;
begin
  if not Assigned(FProperties) and Assigned(FInstanceType) then
    FProperties := TRLRTTIContext.Instance.Inspector.GetAttributes(
      FInstanceType).Find(Name);
  Result := FProperties;
end;

procedure TRLBindingAttribute.KeyChanged;
begin
  inherited;
  ResetProperties;
end;

procedure TRLBindingAttribute.InstanceTypeChanged;
begin
  ResetProperties;
end;

procedure TRLBindingAttribute.ResetProperties;
begin
  FProperties := nil;
end;

procedure TRLBindingAttribute.CheckProperties;
begin
  if not Assigned(Properties) then
    raise ERLBindingAttributeIsInvalid.CreateFmt(
      'Binding attribute "%s" is invalid.', [Name]);
end;

procedure TRLBindingAttribute.DoReadValue(const AInstance: TPersistent;
 var AValue: Variant);
begin
  if Assigned(FOnReadValue) then
    FOnReadValue(Self, AInstance, AValue);
end;

procedure TRLBindingAttribute.DoWriteValue(const AInstance: TPersistent;
 var AValue: Variant);
begin
  if Assigned(FOnWriteValue) then
    FOnWriteValue(Self, AInstance, AValue);
end;

function TRLBindingAttribute.ReadValue(const AInstance: TPersistent): Variant;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadValue(AInstance, Properties,
    False);
  DoReadValue(AInstance, Result);
end;

function TRLBindingAttribute.ReadChar(const AInstance: TPersistent): string;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadChar(AInstance, Properties);
end;

function TRLBindingAttribute.ReadString(const AInstance: TPersistent): string;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadString(AInstance, Properties);
end;

function TRLBindingAttribute.ReadInteger(const AInstance: TPersistent): Integer;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadInteger(AInstance,
    Properties);
end;

function TRLBindingAttribute.ReadInt64(const AInstance: TPersistent): Int64;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadInt64(AInstance, Properties);
end;

function TRLBindingAttribute.ReadFloat(const AInstance: TPersistent): Double;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadFloat(AInstance, Properties);
end;

function TRLBindingAttribute.ReadCurrency(
  const AInstance: TPersistent): Currency;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadCurrency(AInstance,
    Properties);
end;

function TRLBindingAttribute.ReadDate(const AInstance: TPersistent): TDate;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadDate(AInstance, Properties);
end;

function TRLBindingAttribute.ReadTime(const AInstance: TPersistent): TTime;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadTime(AInstance, Properties);
end;

function TRLBindingAttribute.ReadDateTime(
  const AInstance: TPersistent): TDateTime;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadDateTime(AInstance,
    Properties);
end;

function TRLBindingAttribute.ReadBoolean(const AInstance: TPersistent): Boolean;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadBoolean(AInstance,
    Properties);
end;

function TRLBindingAttribute.ReadVariant(const AInstance: TPersistent): Variant;
begin
  CheckProperties;
  Result := TRLRTTIContext.Instance.Inspector.ReadVariant(AInstance,
    Properties);
end;

procedure TRLBindingAttribute.WriteValue(const AInstance: TPersistent;
  const AValue: Variant);
var
  lValue: Variant;
begin
  CheckProperties;
  lValue := AValue;
  DoWriteValue(AInstance, lValue);
  TRLRTTIContext.Instance.Inspector.WriteValue(AInstance, Properties, lValue,
    False);
end;

procedure TRLBindingAttribute.WriteChar(const AInstance: TPersistent;
  const AValue: Char);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteChar(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteString(const AInstance: TPersistent;
  const AValue: string);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteString(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteInteger(const AInstance: TPersistent;
  const AValue: Integer);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteInteger(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteInt64(const AInstance: TPersistent;
  const AValue: Int64);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteInt64(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteFloat(const AInstance: TPersistent;
  const AValue: Double);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteFloat(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteCurrency(const AInstance: TPersistent;
  const AValue: Currency);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteCurrency(AInstance, Properties,
    AValue);
end;

procedure TRLBindingAttribute.WriteDate(const AInstance: TPersistent;
  const AValue: TDate);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteDate(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteTime(const AInstance: TPersistent;
  const AValue: TTime);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteTime(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteDateTime(const AInstance: TPersistent;
  const AValue: TDateTime);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteTime(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteBoolean(const AInstance: TPersistent;
  const AValue: Boolean);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteBoolean(AInstance, Properties, AValue);
end;

procedure TRLBindingAttribute.WriteVariant(const AInstance: TPersistent;
  const AValue: Variant);
begin
  CheckProperties;
  TRLRTTIContext.Instance.Inspector.WriteVariant(AInstance, Properties, AValue);
end;

{ TRLBindingAttributes }

function TRLBindingAttributes.GetItem(
  const AIndex: Integer): TRLBindingAttribute;
begin
  Result := TRLBindingAttribute(inherited Items[AIndex]);
end;

procedure TRLBindingAttributes.SetInstanceType(const AValue: TClass);
begin
  if AValue <> FInstanceType then
  begin
    FInstanceType := AValue;
    InstanceTypeChanged;
  end;
end;

function TRLBindingAttributes.GetItemClass: TCollectionItemClass;
begin
  Result := TRLBindingAttribute;
end;

procedure TRLBindingAttributes.InstanceTypeChanged;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].InstanceType := FInstanceType;
end;

function TRLBindingAttributes.Add: TRLBindingAttribute;
begin
  Result := TRLBindingAttribute(inherited Add);
end;

function TRLBindingAttributes.Insert(
  const AIndex: Integer): TRLBindingAttribute;
begin
  Result := TRLBindingAttribute(inherited Insert(AIndex));
end;

function TRLBindingAttributes.Find(const AName: string): TRLBindingAttribute;
begin
  Result := TRLBindingAttribute(inherited Find(AName));
end;

function TRLBindingAttributes.FindByName(
  const AName: string): TRLBindingAttribute;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLBindingAttributeNotFound.CreateFmt(
      'BindingAttribute "%s" not found.', [AName]);
end;

end.
