//=============================================================================
// U_DXFormManager
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DXFormManager;

interface

uses
  SysUtils,
  Windows,
  Classes,
  Graphics,
  ImgList,
  Controls,
  StdCtrls,
  cxButtons,
  cxCheckBox,
  cxContainer,
  cxGraphics,
  cxImage,
  cxListView,
  cxPC,
  cxRadioGroup,
  cxTextEdit,
  cxTrackBar,
  cxTreeView,
  dxGDIPlusClasses,
  U_FormManager,
  U_VCLHacks,
  U_VCLFormManager;

type
  // TRLDXFormManager
  TRLDXFormManager = class(TRLVCLFormManager)
  public
    function LoadPNGFromResource(const AIdentifier: string; const ASize: Integer): TGraphic; override;
    procedure AssignPNGFromResource(const AIdentifier: string; const ASize: Integer; const AImage: TcxImage); reintroduce; virtual;
    procedure AssignIconFromResource(const AIdentifier: string; const ASize: Integer; const AImage: TcxImage); reintroduce; virtual;
    procedure AssignImageFromResource(const AIdentifier: string; const AType: TRLResourceImageType; const ASize: Integer; const AImage: TcxImage); reintroduce; virtual;
    function AddPNGFromResource(const AIdentifier: string; const AImages: TCustomImageList): Integer; override;
    function AddIconFromResource(const AIdentifier: string; const AImages: TCustomImageList): Integer; override;
    function CanSkipControl(const AControl: TControl; const AShift: TShiftState): Boolean; override;
    class function Instance: TRLDXFormManager;
  end;

implementation

{ TRLDXFormManager }

function TRLDXFormManager.LoadPNGFromResource(const AIdentifier: string;
  const ASize: Integer): TGraphic;
var
  lStream: TResourceStream;
begin
  lStream := TResourceStream.Create(MainInstance,
    GetImageResourceName(AIdentifier, ritPNG, ASize), RT_RCDATA);
  try
    Result := TdxPNGImage.Create;
    Result.LoadFromStream(lStream);
  finally
    FreeAndNil(lStream);
  end;
end;

procedure TRLDXFormManager.AssignPNGFromResource(const AIdentifier: string;
  const ASize: Integer; const AImage: TcxImage);
var
  lPNG: TGraphic;
begin
  if Assigned(AImage) then
  begin
    lPNG := LoadPNGFromResource(AIdentifier, ASize);
    try
      AImage.Picture.Graphic := lPNG;
    finally
      FreeAndNil(lPNG);
    end;
  end;
end;

procedure TRLDXFormManager.AssignIconFromResource(const AIdentifier: string;
  const ASize: Integer; const AImage: TcxImage);
var
  lIcon: TIcon;
begin
  if Assigned(AImage) then
  begin
    lIcon := LoadIconFromResource(AIdentifier, ASize);
    try
      AImage.Picture.Icon := lIcon;
    finally
      FreeAndNil(lIcon);
    end;
  end;
end;

procedure TRLDXFormManager.AssignImageFromResource(const AIdentifier: string;
  const AType: TRLResourceImageType; const ASize: Integer;
  const AImage: TcxImage);
begin
  case AType of
    ritPNG: AssignPNGFromResource(AIdentifier, ASize, AImage);
    ritIcon: AssignIconFromResource(AIdentifier, ASize, AImage);
  end;
end;

function TRLDXFormManager.AddPNGFromResource(const AIdentifier: string;
  const AImages: TCustomImageList): Integer;
var
  lPNG: TdxPNGImage;
  lBitmap: TBitmap;
begin
  if AImages is TcxImageList then
  begin
    lPNG := TdxPNGImage(LoadPNGFromResource(AIdentifier, AImages.Width));
    try
      lBitmap := lPNG.GetAsBitmap;
      try
        Result := TcxImageList(AImages).Add(lBitmap, nil);
      finally
        FreeAndNil(lBitmap);
      end;
    finally
      FreeAndNil(lPNG);
    end;
  end
  else
    Result := -1;
end;

function TRLDXFormManager.AddIconFromResource(const AIdentifier: string;
  const AImages: TCustomImageList): Integer;
var
  lIcon: TIcon;
begin
  if AImages is TcxImageList then
  begin
    lIcon := LoadIconFromResource(AIdentifier, AImages.Width);
    try
      Result := TcxImageList(AImages).AddIcon(lIcon);
    finally
      FreeAndNil(lIcon);
    end;
  end
  else
    Result := inherited AddIconFromResource(AIdentifier, AImages);
end;

function TRLDXFormManager.CanSkipControl(const AControl: TControl;
  const AShift: TShiftState): Boolean;
begin
  if AControl is TCustomMemo then
    Result := not TRLCustomMemo.GetWantReturns(AControl) or (ssCtrl in AShift)
  else if (AControl is TcxCustomInnerListBox) or
    (AControl is TcxCustomInnerListView) or
    (AControl is TcxCustomInnerTreeView) or
    (AControl is TcxButton) then
    Result := ssCtrl in AShift
  else
    Result := (AControl is TcxCustomInnerTextEdit) or
      (AControl is TcxCustomCheckBox) or
      (AControl is TcxCustomImage) or
      (AControl is TcxCustomTrackBar) or
      (AControl is TcxCustomTabControl) or
      (AControl is TcxRadioButton);
end;

class function TRLDXFormManager.Instance: TRLDXFormManager;
begin
  Result := TRLDXFormManager(inherited Instance);
end;

initialization
  TRLFormManager.SetInstanceType(TRLDXFormManager);

finalization
  if TRLFormManager.GetInstanceType = TRLDXFormManager then
    TRLFormManager.DeleteInstance;

end.
