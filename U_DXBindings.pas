//=============================================================================
// U_DXBindings
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================
                                   
unit U_DXBindings;

interface

// Unsupported:
//  - TRLcxCheckBoxBinding
//      - Properties.AllowGrayed
//  - TRLcxComboBoxBinding
//      - Properties.Sorted
//  - TRLcxListBoxBinding
//      - Sorted
//      - MultiSelect
//      - Style lbVirtual
//      - Style lvVirtualOwnerDraw

uses
  Classes,
  ComCtrls,
  cxCalc,
  cxCalendar,
  cxCheckBox,
  cxColorComboBox,
  cxDropDownEdit,
  cxImage,
  cxLabel,
  cxListBox,
  cxListView,
  cxMaskEdit,
  cxMemo,
  cxProgressBar,
  cxRadioGroup,
  cxRichEdit,
  cxTextEdit,
  cxTimeEdit,
  cxCurrencyEdit,
  cxTrackBar,
  cxTreeView,
  cxTL,
  U_Collection,
  U_RTTIAttributes,
  U_Bindings,
  U_VCLBindings;

type
  // TRLcxTextEditBinding
  TRLcxTextEditBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxTextEdit;
    procedure SetControl(const AValue: TcxTextEdit);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxTextEdit read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxTextEditBinding;
  end;

  // TRLcxMaskEditBinding
  TRLcxMaskEditBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxMaskEdit;
    procedure SetControl(const AValue: TcxMaskEdit);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxMaskEdit read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxMaskEditBinding;
  end;

  // TRLcxMemoBinding
  TRLcxMemoBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxMemo;
    procedure SetControl(const AValue: TcxMemo);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxMemo read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxMemoBinding;
  end;

  // TRLcxDateEditBinding
  TRLcxDateEditBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxDateEdit;
    procedure SetControl(const AValue: TcxDateEdit);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxDateEdit read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxDateEditBinding;
  end;

  // TRLcxCheckBoxBinding
  TRLcxCheckBoxBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxCheckBox;
    procedure SetControl(const AValue: TcxCheckBox);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxCheckBox read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxCheckBoxBinding;
  end;

  // TRLcxComboBoxBinding
  TRLcxComboBoxBinding = class(TRLStringsBinding)
  private
    function GetControl: TcxComboBox;
    procedure SetControl(const AValue: TcxComboBox);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
    function GetStrings: TStrings; override;
    function GetItemIndex: Integer; override;
    procedure SetItemIndex(const AValue: Integer); override;
  public
    procedure Initialize; override;
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxComboBox read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxComboBoxBinding;
  end;

  // TRLcxCalcEditBinding
  TRLcxCalcEditBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxCalcEdit;
    procedure SetControl(const AValue: TcxCalcEdit);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxCalcEdit read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxCalcEditBinding;
  end;

  // TRLcxTimeEditBinding
  TRLcxTimeEditBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxTimeEdit;
    procedure SetControl(const AValue: TcxTimeEdit);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxTimeEdit read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxTimeEditBinding;
  end;

  // TRLcxCurrencyEditBinding
  TRLcxCurrencyEditBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxCurrencyEdit;
    procedure SetControl(const AValue: TcxCurrencyEdit);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxCurrencyEdit read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxCurrencyEditBinding;
  end;

  // TRLcxImageBinding
  TRLcxImageBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxImage;
    procedure SetControl(const AValue: TcxImage);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    procedure Read; override;
    procedure Write; override;
    property Control: TcxImage read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxImageBinding;
  end;

  // TRLcxRadioButtonBinding
  TRLcxRadioButtonBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxRadioButton;
    procedure SetControl(const AValue: TcxRadioButton);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxRadioButton read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxRadioButtonBinding;
  end;

  // TRLcxRadioGroupBinding
  { TODO -oRafael : Tratar TcxRadioGroupItems. }
  TRLcxRadioGroupBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxRadioGroup;
    procedure SetControl(const AValue: TcxRadioGroup);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxRadioGroup read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxRadioGroupBinding;
  end;

  // TRLcxListBoxBinding
  TRLcxListBoxBinding = class(TRLStringsBinding)
  private
    function GetControl: TcxListBox;
    procedure SetControl(const AValue: TcxListBox);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
    function GetStrings: TStrings; override;
    function GetItemIndex: Integer; override;
    procedure SetItemIndex(const AValue: Integer); override;
  public
    procedure Initialize; override;
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxListBox read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxListBoxBinding;
  end;

  // TRLcxLabelBinding
  TRLcxLabelBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxLabel;
    procedure SetControl(const AValue: TcxLabel);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    procedure Read; override;
    property Control: TcxLabel read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxLabelBinding;
  end;

  // TRLcxProgressBarBinding
  TRLcxProgressBarBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxProgressBar;
    procedure SetControl(const AValue: TcxProgressBar);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    property Control: TcxProgressBar read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxProgressBarBinding;
  end;

  // TRLcxTrackBarBinding
  TRLcxTrackBarBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxTrackBar;
    procedure SetControl(const AValue: TcxTrackBar);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxTrackBar read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxTrackBarBinding;
  end;

  // TRLcxColorComboBoxBinding
  TRLcxColorComboBoxBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxColorComboBox;
    procedure SetControl(const AValue: TcxColorComboBox);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxColorComboBox read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxColorComboBoxBinding;
  end;

  // TRLcxRichEditBinding
  TRLcxRichEditBinding = class(TRLAttributeBinding)
  private
    function GetControl: TcxRichEdit;
    procedure SetControl(const AValue: TcxRichEdit);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    procedure Read; override;
    procedure Write; override;
    property Control: TcxRichEdit read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxRichEditBinding;
  end;

  // TRLcxListViewBinding
  TRLcxListViewBinding = class(TRLListItemsBinding)
  private
    function GetControl: TcxListView;
    procedure SetControl(const AValue: TcxListView);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
    function GetItems: TListItems; override;
    function GetFocusedItem: TListItem; override;
    procedure SetFocusedItem(const AItem: TListItem); override;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    property Control: TcxListView read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxListViewBinding;
  end;

  { TODO -oRafael : TRLcxTreeViewBinding. }
  
  // TRLcxTreeListBinding
  TRLcxTreeListBinding = class(TRLCollectionBinding)
  private
    function GetControl: TcxTreeList;
    procedure SetControl(const AValue: TcxTreeList);
  protected
    function GetReadOnly: Boolean; override;
    procedure SetReadOnly(const AValue: Boolean); override;
    procedure BeginListUpdate; override;
    procedure EndListUpdate; override;
    procedure ClearList; override;
    function AddItem(const AInstance: TRLCollectionItem): TObject; override;
    procedure SelectItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure RefreshItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure CheckItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure UncheckItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure RemoveItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure FillItem(const AInstance: TRLCollectionItem; const AItem: TcxTreeListNode); virtual;
  public
    function CanFocus: Boolean; override;
    procedure SetFocus; override;
    function GetSelectedItem: TRLCollectionItem; override;
    function GetSelectionItems: TRLCollectionItemArray; override;
    function GetCheckedItems: TRLCollectionItemArray; override;
    function GetUncheckedItems: TRLCollectionItemArray; override;
    property Control: TcxTreeList read GetControl write SetControl;
    class function CreateBinding(const ABindings: TRLBindings): TRLcxTreeListBinding;
  end;

implementation

uses
  StrUtils,
  Variants;

{ TRLcxTextEditBinding }

function TRLcxTextEditBinding.GetControl: TcxTextEdit;
begin
  Result := TcxTextEdit(inherited Control);
end;

procedure TRLcxTextEditBinding.SetControl(const AValue: TcxTextEdit);
begin
  inherited Control := AValue;
end;

function TRLcxTextEditBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxTextEditBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxTextEditBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxTextEditBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxTextEditBinding.Read;
begin
  inherited;
  Control.Text := ReadString;
end;

procedure TRLcxTextEditBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteString(Control.Text);
end;

class function TRLcxTextEditBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxTextEditBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxTextEditBinding(ABindings.AddType(TRLcxTextEditBinding))
  else
    Result := nil;
end;

{ TRLcxMaskEditBinding }

function TRLcxMaskEditBinding.GetControl: TcxMaskEdit;
begin
  Result := TcxMaskEdit(inherited Control);
end;

procedure TRLcxMaskEditBinding.SetControl(const AValue: TcxMaskEdit);
begin
  inherited Control := AValue;
end;

function TRLcxMaskEditBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxMaskEditBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxMaskEditBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxMaskEditBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxMaskEditBinding.Read;
begin
  inherited;
  Control.Text := ReadString;
end;

procedure TRLcxMaskEditBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteString(Control.Text);
end;

class function TRLcxMaskEditBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxMaskEditBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxMaskEditBinding(ABindings.AddType(TRLcxMaskEditBinding))
  else
    Result := nil;
end;

{ TRLcxMemoBinding }

function TRLcxMemoBinding.GetControl: TcxMemo;
begin
  Result := TcxMemo(inherited Control);
end;

procedure TRLcxMemoBinding.SetControl(const AValue: TcxMemo);
begin
  inherited Control := AValue;
end;

function TRLcxMemoBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxMemoBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxMemoBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxMemoBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxMemoBinding.Read;
begin
  inherited;
  Control.Text := ReadString;
end;

procedure TRLcxMemoBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteString(Control.Text);
end;

class function TRLcxMemoBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxMemoBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxMemoBinding(ABindings.AddType(TRLcxMemoBinding))
  else
    Result := nil;
end;

{ TRLcxDateEditBinding }

function TRLcxDateEditBinding.GetControl: TcxDateEdit;
begin
  Result := TcxDateEdit(inherited Control);
end;

procedure TRLcxDateEditBinding.SetControl(const AValue: TcxDateEdit);
begin
  inherited Control := AValue;
end;

function TRLcxDateEditBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxDateEditBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxDateEditBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxDateEditBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxDateEditBinding.Read;
begin
  inherited;
  if Control.Properties.Kind = ckDate then
    Control.Date := Trunc(ReadDate)
  else
    Control.Date := ReadDateTime;
end;

procedure TRLcxDateEditBinding.Write;
begin
  inherited;
  if Control.Properties.Kind = ckDate then
    WriteDate(Trunc(Control.Date))
  else
    WriteDateTime(Control.Date);
end;

class function TRLcxDateEditBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxDateEditBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxDateEditBinding(ABindings.AddType(TRLcxDateEditBinding))
  else
    Result := nil;
end;

{ TRLcxCheckBoxBinding }

function TRLcxCheckBoxBinding.GetControl: TcxCheckBox;
begin
  Result := TcxCheckBox(inherited Control);
end;

procedure TRLcxCheckBoxBinding.SetControl(const AValue: TcxCheckBox);
begin
  inherited Control := AValue;
end;

function TRLcxCheckBoxBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxCheckBoxBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxCheckBoxBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxCheckBoxBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxCheckBoxBinding.Read;
begin
  inherited;
  Control.Checked := ReadBoolean;
end;

procedure TRLcxCheckBoxBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteBoolean(Control.Checked);
end;

class function TRLcxCheckBoxBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxCheckBoxBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxCheckBoxBinding(ABindings.AddType(TRLcxCheckBoxBinding))
  else
    Result := nil;
end;

{ TRLcxComboBoxBinding }

function TRLcxComboBoxBinding.GetControl: TcxComboBox;
begin
  Result := TcxComboBox(inherited Control);
end;

procedure TRLcxComboBoxBinding.SetControl(const AValue: TcxComboBox);
begin
  inherited Control := AValue;
end;

function TRLcxComboBoxBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxComboBoxBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxComboBoxBinding.GetStrings: TStrings;
begin
  Result := Control.Properties.Items;
end;

function TRLcxComboBoxBinding.GetItemIndex: Integer;
begin
  Result := Control.ItemIndex;
end;

procedure TRLcxComboBoxBinding.SetItemIndex(const AValue: Integer);
begin
  Control.ItemIndex := AValue;
end;

procedure TRLcxComboBoxBinding.Initialize;
begin
  inherited;
  Populate;
end;

function TRLcxComboBoxBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxComboBoxBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxComboBoxBinding.Read;
begin
  inherited;
  if Control.Properties.DropDownListStyle in [lsEditFixedList, lsEditList] then
    Control.Text := ReadString
  else
    SetValue(VarToStr(ReadValue));
end;

procedure TRLcxComboBoxBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
  begin
    if Control.Properties.DropDownListStyle in [lsEditFixedList, lsEditList] then
      WriteString(Control.Text)
    else
      WriteValue(GetValue);
  end;
end;

class function TRLcxComboBoxBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxComboBoxBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxComboBoxBinding(ABindings.AddType(TRLcxComboBoxBinding))
  else
    Result := nil;
end;

{ TRLcxCalcEditBinding }

function TRLcxCalcEditBinding.GetControl: TcxCalcEdit;
begin
  Result := TcxCalcEdit(inherited Control);
end;

procedure TRLcxCalcEditBinding.SetControl(const AValue: TcxCalcEdit);
begin
  inherited Control := AValue;
end;

function TRLcxCalcEditBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxCalcEditBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxCalcEditBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxCalcEditBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxCalcEditBinding.Read;
begin
  inherited;
  case Attribute.Properties.ValueType of
    avtInteger, avtInt64: Control.Value := ReadInteger;
    avtFloat: Control.Value := ReadFloat;
    avtCurrency: Control.Value := ReadCurrency;
  end;
end;

procedure TRLcxCalcEditBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    case Attribute.Properties.ValueType of
      avtInteger, avtInt64: WriteInteger(Trunc(Control.Value));
      avtFloat: WriteFloat(Control.Value);
      avtCurrency: WriteCurrency(Control.Value);
    end;
end;

class function TRLcxCalcEditBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxCalcEditBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxCalcEditBinding(ABindings.AddType(TRLcxCalcEditBinding))
  else
    Result := nil;
end;

{ TRLcxTimeEditBinding }

function TRLcxTimeEditBinding.GetControl: TcxTimeEdit;
begin
  Result := TcxTimeEdit(inherited Control);
end;

procedure TRLcxTimeEditBinding.SetControl(const AValue: TcxTimeEdit);
begin
  inherited Control := AValue;
end;

function TRLcxTimeEditBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxTimeEditBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxTimeEditBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxTimeEditBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxTimeEditBinding.Read;
begin
  inherited;
  Control.Time := ReadTime;
end;

procedure TRLcxTimeEditBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteTime(Control.Time);
end;

class function TRLcxTimeEditBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxTimeEditBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxTimeEditBinding(ABindings.AddType(TRLcxTimeEditBinding))
  else
    Result := nil;
end;

{ TRLcxCurrencyEditBinding }

function TRLcxCurrencyEditBinding.GetControl: TcxCurrencyEdit;
begin
  Result := TcxCurrencyEdit(inherited Control);
end;

procedure TRLcxCurrencyEditBinding.SetControl(const AValue: TcxCurrencyEdit);
begin
  inherited Control := AValue;
end;

function TRLcxCurrencyEditBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxCurrencyEditBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxCurrencyEditBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxCurrencyEditBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxCurrencyEditBinding.Read;
begin
  inherited;
  case Attribute.Properties.ValueType of
    avtInteger, avtInt64: Control.Value := ReadInteger;
    avtFloat: Control.Value := ReadFloat;
    avtCurrency: Control.Value := ReadCurrency;
  end;
end;

procedure TRLcxCurrencyEditBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    case Attribute.Properties.ValueType of
      avtInteger, avtInt64: WriteInteger(Trunc(Control.Value));
      avtFloat: WriteFloat(Control.Value);
      avtCurrency: WriteCurrency(Control.Value);
    end;
end;

class function TRLcxCurrencyEditBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxCurrencyEditBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxCurrencyEditBinding(ABindings.AddType(TRLcxCurrencyEditBinding))
  else
    Result := nil;
end;

{ TRLcxImageBinding }

function TRLcxImageBinding.GetControl: TcxImage;
begin
  Result := TcxImage(inherited Control);
end;

procedure TRLcxImageBinding.SetControl(const AValue: TcxImage);
begin
  inherited Control := AValue;
end;

function TRLcxImageBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxImageBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

procedure TRLcxImageBinding.Read;
begin
  inherited;
  { TODO -oRafael : Implementar. }
end;

procedure TRLcxImageBinding.Write;
begin
  inherited;
  { TODO -oRafael : Implementar. }
end;

class function TRLcxImageBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxImageBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxImageBinding(ABindings.AddType(TRLcxImageBinding))
  else
    Result := nil;
end;

{ TRLcxRadioButtonBinding }

function TRLcxRadioButtonBinding.GetControl: TcxRadioButton;
begin
  Result := TcxRadioButton(inherited Control);
end;

procedure TRLcxRadioButtonBinding.SetControl(const AValue: TcxRadioButton);
begin
  inherited Control := AValue;
end;

function TRLcxRadioButtonBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or not Control.Enabled;
end;

procedure TRLcxRadioButtonBinding.SetReadOnly(const AValue: Boolean);
begin
  SetEnabled(not AValue);
end;

function TRLcxRadioButtonBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxRadioButtonBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxRadioButtonBinding.Read;
begin
  inherited;
  Control.Checked := ReadBoolean;
end;

procedure TRLcxRadioButtonBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteBoolean(Control.Checked);
end;

class function TRLcxRadioButtonBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxRadioButtonBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxRadioButtonBinding(
      ABindings.AddType(TRLcxRadioButtonBinding))
  else
    Result := nil;
end;

{ TRLcxRadioGroupBinding }

function TRLcxRadioGroupBinding.GetControl: TcxRadioGroup;
begin
  Result := TcxRadioGroup(inherited Control);
end;

procedure TRLcxRadioGroupBinding.SetControl(const AValue: TcxRadioGroup);
begin
  inherited Control := AValue;
end;

function TRLcxRadioGroupBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxRadioGroupBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxRadioGroupBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxRadioGroupBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxRadioGroupBinding.Read;
begin
  inherited;
  { TODO -oRafael : Implementar. }
end;

procedure TRLcxRadioGroupBinding.Write;
begin
  inherited;
  { TODO -oRafael : Implementar. }
end;

class function TRLcxRadioGroupBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxRadioGroupBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxRadioGroupBinding(ABindings.AddType(TRLcxRadioGroupBinding))
  else
    Result := nil;
end;

{ TRLcxListBoxBinding }

function TRLcxListBoxBinding.GetControl: TcxListBox;
begin
  Result := TcxListBox(inherited Control);
end;

procedure TRLcxListBoxBinding.SetControl(const AValue: TcxListBox);
begin
  inherited Control := AValue;
end;

function TRLcxListBoxBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.ReadOnly;
end;

procedure TRLcxListBoxBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.ReadOnly := AValue;
end;

function TRLcxListBoxBinding.GetStrings: TStrings;
begin
  Result := Control.Items;
end;

function TRLcxListBoxBinding.GetItemIndex: Integer;
begin
  Result := Control.ItemIndex;
end;

procedure TRLcxListBoxBinding.SetItemIndex(const AValue: Integer);
begin
  Control.ItemIndex := AValue;
end;

procedure TRLcxListBoxBinding.Initialize;
begin
  inherited;
  Populate;
end;

function TRLcxListBoxBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxListBoxBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxListBoxBinding.Read;
begin
  inherited;
  SetValue(VarToStr(ReadValue));
end;

procedure TRLcxListBoxBinding.Write;
begin                 
  inherited;
  if Attribute.Properties.IsWritable then
    WriteValue(GetValue);
end;

class function TRLcxListBoxBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxListBoxBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxListBoxBinding(ABindings.AddType(TRLcxListBoxBinding))
  else
    Result := nil;
end;

{ TRLcxLabelBinding }

function TRLcxLabelBinding.GetControl: TcxLabel;
begin
  Result := TcxLabel(inherited Control);
end;

procedure TRLcxLabelBinding.SetControl(const AValue: TcxLabel);
begin
  inherited Control := AValue;
end;

function TRLcxLabelBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxLabelBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

procedure TRLcxLabelBinding.Read;
begin
  inherited;
  Control.Caption := ReadString;
end;

class function TRLcxLabelBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxLabelBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxLabelBinding(ABindings.AddType(TRLcxLabelBinding))
  else
    Result := nil;
end;

{ TRLcxProgressBarBinding }

function TRLcxProgressBarBinding.GetControl: TcxProgressBar;
begin
  Result := TcxProgressBar(inherited Control);
end;

procedure TRLcxProgressBarBinding.SetControl(const AValue: TcxProgressBar);
begin
  inherited Control := AValue;
end;

function TRLcxProgressBarBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxProgressBarBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxProgressBarBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxProgressBarBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxProgressBarBinding.Read;
begin
  inherited;
  Control.Position := ReadInteger;
end;

class function TRLcxProgressBarBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxProgressBarBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxProgressBarBinding(
      ABindings.AddType(TRLcxProgressBarBinding))
  else
    Result := nil;
end;

{ TRLcxTrackBarBinding }

function TRLcxTrackBarBinding.GetControl: TcxTrackBar;
begin
  Result := TcxTrackBar(inherited Control);
end;

procedure TRLcxTrackBarBinding.SetControl(const AValue: TcxTrackBar);
begin
  inherited Control := AValue;
end;

function TRLcxTrackBarBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxTrackBarBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxTrackBarBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxTrackBarBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxTrackBarBinding.Read;
begin
  inherited;
  Control.Position := ReadInteger;
end;

procedure TRLcxTrackBarBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteInteger(Control.Position);
end;

class function TRLcxTrackBarBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxTrackBarBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxTrackBarBinding(ABindings.AddType(TRLcxTrackBarBinding))
  else
    Result := nil;
end;

{ TRLcxColorComboBoxBinding }

function TRLcxColorComboBoxBinding.GetControl: TcxColorComboBox;
begin
  Result := TcxColorComboBox(inherited Control);
end;

procedure TRLcxColorComboBoxBinding.SetControl(const AValue: TcxColorComboBox);
begin
  inherited Control := AValue;
end;

function TRLcxColorComboBoxBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxColorComboBoxBinding.SetReadOnly(const AValue: Boolean);
begin
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxColorComboBoxBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxColorComboBoxBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxColorComboBoxBinding.Read;
begin
  inherited;
  Control.ColorValue := ReadInteger;
end;

procedure TRLcxColorComboBoxBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteInteger(Control.ColorValue);
end;

class function TRLcxColorComboBoxBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxColorComboBoxBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxColorComboBoxBinding(
      ABindings.AddType(TRLcxColorComboBoxBinding))
  else
    Result := nil;
end;

{ TRLcxRichEditBinding }

function TRLcxRichEditBinding.GetControl: TcxRichEdit;
begin
  Result := TcxRichEdit(inherited Control);
end;

procedure TRLcxRichEditBinding.SetControl(const AValue: TcxRichEdit);
begin
  inherited Control := AValue;
end;

function TRLcxRichEditBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.Properties.ReadOnly;
end;

procedure TRLcxRichEditBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.Properties.ReadOnly := AValue;
end;

function TRLcxRichEditBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxRichEditBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

procedure TRLcxRichEditBinding.Read;
begin
  inherited;
  Control.Text := ReadString;
end;

procedure TRLcxRichEditBinding.Write;
begin
  inherited;
  if Attribute.Properties.IsWritable then
    WriteString(Control.Text);
end;

class function TRLcxRichEditBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxRichEditBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxRichEditBinding(ABindings.AddType(TRLcxRichEditBinding))
  else
    Result := nil;
end;

{ TRLcxListViewBinding }

function TRLcxListViewBinding.GetControl: TcxListView;
begin
  Result := TcxListView(inherited Control);
end;

procedure TRLcxListViewBinding.SetControl(const AValue: TcxListView);
begin
  inherited Control := AValue;
end;

function TRLcxListViewBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or Control.ReadOnly;
end;

procedure TRLcxListViewBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.ReadOnly := AValue;
end;

function TRLcxListViewBinding.GetItems: TListItems;
begin
  Result := Control.Items;
end;

function TRLcxListViewBinding.GetFocusedItem: TListItem;
begin
  Result := Control.Selected;
end;

procedure TRLcxListViewBinding.SetFocusedItem(const AItem: TListItem);
begin
  Control.Selected := AItem;
end;

function TRLcxListViewBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxListViewBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

class function TRLcxListViewBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxListViewBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxListViewBinding(ABindings.AddType(TRLcxListViewBinding))
  else
    Result := nil;
end;

{ TRLcxTreeListBinding }

function TRLcxTreeListBinding.GetControl: TcxTreeList;
begin
  Result := TcxTreeList(inherited Control);
end;

procedure TRLcxTreeListBinding.SetControl(const AValue: TcxTreeList);
begin
  inherited Control := AValue;
end;

function TRLcxTreeListBinding.GetReadOnly: Boolean;
begin
  Result := inherited GetReadOnly or not Control.OptionsData.Editing;
end;

procedure TRLcxTreeListBinding.SetReadOnly(const AValue: Boolean);
begin
  inherited;
  Control.OptionsData.Editing := not AValue;
  Control.OptionsData.Inserting := not AValue;
  Control.OptionsData.Deleting := not AValue;
end;

procedure TRLcxTreeListBinding.BeginListUpdate;
begin
  Control.BeginUpdate;
end;

procedure TRLcxTreeListBinding.EndListUpdate;
begin
  Control.EndUpdate;
end;

procedure TRLcxTreeListBinding.ClearList;
begin
  Control.Clear;
end;

function TRLcxTreeListBinding.AddItem(
  const AInstance: TRLCollectionItem): TObject;
var
  lItem: TcxTreeListNode;
begin
  if AInstance.Index < Control.Root.Count then
    lItem := Control.Root[AInstance.Index]
  else
    lItem := nil;
  lItem := Control.Root.InsertChild(lItem);
  lItem.Data := AInstance;
  FillItem(AInstance, lItem);
  Result := lItem;
end;

procedure TRLcxTreeListBinding.SelectItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  Control.FocusedNode := TcxTreeListNode(AItem);
  Control.FocusedNode.MakeVisible;
end;

procedure TRLcxTreeListBinding.RefreshItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  FillItem(AInstance, TcxTreeListNode(AItem));
end;

procedure TRLcxTreeListBinding.CheckItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  TcxTreeListNode(AItem).Checked := True;
end;

procedure TRLcxTreeListBinding.UncheckItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  TcxTreeListNode(AItem).Checked := False;
end;

procedure TRLcxTreeListBinding.RemoveItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  TcxTreeListNode(AItem).Delete;
  inherited;
end;

procedure TRLcxTreeListBinding.FillItem(const AInstance: TRLCollectionItem;
  const AItem: TcxTreeListNode);
var
  I: Integer;
begin
  for I := 0 to Pred(Attributes.Count) do
    AItem.Texts[I] := VarToStr(Attributes[I].ReadValue(AInstance));
end;

function TRLcxTreeListBinding.CanFocus: Boolean;
begin
  inherited CanFocus;
  Result := Control.CanFocus;
end;

procedure TRLcxTreeListBinding.SetFocus;
begin
  inherited;
  Control.SetFocus;
end;

function TRLcxTreeListBinding.GetSelectedItem: TRLCollectionItem;
begin
  inherited GetSelectedItem;
  if Control.SelectionCount > 0 then
    Result := TRLCollectionItem(Control.Selections[0].Data)
  else
    Result := nil;
end;

function TRLcxTreeListBinding.GetSelectionItems: TRLCollectionItemArray;
var
  I: Integer;
begin
  inherited GetSelectionItems;
  Result := TRLCollectionItemArray.Create;
  for I := 0 to Pred(Control.Root.Count) do
    if Control.Root[I].Selected then
      Result.Add(TRLCollectionItem(Control.Root[I].Data));
end;

function TRLcxTreeListBinding.GetCheckedItems: TRLCollectionItemArray;
var
  I: Integer;
begin
  inherited GetCheckedItems;
  Result := TRLCollectionItemArray.Create;
  for I := 0 to Pred(Control.Root.Count) do
    if Control.Root[I].Checked then
      Result.Add(TRLCollectionItem(Control.Root[I].Data));
end;

function TRLcxTreeListBinding.GetUncheckedItems: TRLCollectionItemArray;
var
  I: Integer;
begin
  inherited GetUncheckedItems;
  Result := TRLCollectionItemArray.Create;
  for I := 0 to Pred(Control.Root.Count) do
    if not Control.Root[I].Checked then
      Result.Add(TRLCollectionItem(Control.Root[I].Data));
end;

class function TRLcxTreeListBinding.CreateBinding(
  const ABindings: TRLBindings): TRLcxTreeListBinding;
begin
  if Assigned(ABindings) then
    Result := TRLcxTreeListBinding(ABindings.AddType(TRLcxTreeListBinding))
  else
    Result := nil;
end;

end.
