//=============================================================================
// U_InterfacedObject
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_InterfacedObject;

interface

uses
  Classes;

type
  // References
  TRLInterfacedObjectClass = class of TRLInterfacedObject;

  // TRLInterfacedObject
  TRLInterfacedObject = class(TInterfacedObject)
  private
    FDestroyed: Boolean;
  protected
    property Destroyed: Boolean read FDestroyed;
  public
    constructor Create; virtual;
    destructor Destroy; override;
  end;

implementation

{ TRLInterfacedObject }

constructor TRLInterfacedObject.Create;
begin
  inherited Create;
end;

destructor TRLInterfacedObject.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

end.
