//=============================================================================
// U_VCLBindings
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLBindings;

interface

uses
  ComCtrls,
  U_Collection,
  U_Bindings;

type
  // TRLListItemsBinding
  TRLListItemsBinding = class(TRLCollectionBinding)
  protected
    procedure BeginListUpdate; override;
    procedure EndListUpdate; override;
    procedure ClearList; override;
    function AddItem(const AInstance: TRLCollectionItem): TObject; override;
    procedure SelectItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure RefreshItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure CheckItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure UncheckItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    procedure RemoveItem(const AInstance: TRLCollectionItem; const AItem: TObject); override;
    function GetItems: TListItems; virtual; abstract;
    function GetFocusedItem: TListItem; virtual; abstract;
    procedure SetFocusedItem(const AItem: TListItem); virtual; abstract;
    procedure FillItem(const AInstance: TRLCollectionItem; const AItem: TListItem); virtual;
    property Items: TListItems read GetItems;
    property FocusedItem: TListItem read GetFocusedItem write SetFocusedItem;
  public
    function GetSelectedItem: TRLCollectionItem; override;
    function GetSelectionItems: TRLCollectionItemArray; override;
    function GetCheckedItems: TRLCollectionItemArray; override;
    function GetUncheckedItems: TRLCollectionItemArray; override;
  end;

implementation

uses
  Variants;

{ TRLListItemsBinding }

procedure TRLListItemsBinding.BeginListUpdate;
begin
  Items.BeginUpdate;
end;

procedure TRLListItemsBinding.EndListUpdate;
begin
  Items.EndUpdate;
end;

procedure TRLListItemsBinding.ClearList;
begin
  Items.Clear;
end;

function TRLListItemsBinding.AddItem(
  const AInstance: TRLCollectionItem): TObject;
var
  lItem: TListItem;
begin
  lItem := Items.Insert(AInstance.Index);
  lItem.Data := AInstance;
  FillItem(AInstance, lItem);
  Result := lItem;
end;

procedure TRLListItemsBinding.SelectItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  FocusedItem := TListItem(AItem);
  FocusedItem.MakeVisible(False);
end;

procedure TRLListItemsBinding.RefreshItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  FillItem(AInstance, TListItem(AItem));
end;

procedure TRLListItemsBinding.CheckItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  TListItem(AItem).Checked := True;
end;

procedure TRLListItemsBinding.UncheckItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  TListItem(AItem).Checked := False;
end;

procedure TRLListItemsBinding.RemoveItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  TListItem(AItem).Delete;
  inherited;
end;

procedure TRLListItemsBinding.FillItem(const AInstance: TRLCollectionItem;
  const AItem: TListItem);
var
  I: Integer;
begin
  AItem.Caption := VarToStr(Attributes[0].ReadValue(AInstance));
  AItem.SubItems.BeginUpdate;
  try
    AItem.SubItems.Clear;
    for I := 1 to Pred(Attributes.Count) do
      AItem.SubItems.Add(VarToStr(Attributes[I].ReadValue(AInstance)));
  finally
    AItem.SubItems.EndUpdate;
  end;
end;

function TRLListItemsBinding.GetSelectedItem: TRLCollectionItem;
begin
  inherited GetSelectedItem;
  if Assigned(FocusedItem) then
    Result := TRLCollectionItem(FocusedItem.Data)
  else
    Result := nil;
end;

function TRLListItemsBinding.GetSelectionItems: TRLCollectionItemArray;
var
  I: Integer;
begin
  inherited GetSelectionItems;
  Result := TRLCollectionItemArray.Create;
  for I := 0 to Pred(Items.Count) do
    if Items[I].Selected then
      Result.Add(TRLCollectionItem(Items[I].Data));
end;

function TRLListItemsBinding.GetCheckedItems: TRLCollectionItemArray;
var
  I: Integer;
begin
  inherited GetCheckedItems;
  Result := TRLCollectionItemArray.Create;
  for I := 0 to Pred(Items.Count) do
    if Items[I].Checked then
      Result.Add(TRLCollectionItem(Items[I].Data));
end;

function TRLListItemsBinding.GetUncheckedItems: TRLCollectionItemArray;
var
  I: Integer;
begin
  inherited GetUncheckedItems;
  Result := TRLCollectionItemArray.Create;
  for I := 0 to Pred(Items.Count) do
    if not Items[I].Checked then
      Result.Add(TRLCollectionItem(Items[I].Data));
end;

end.
