//=============================================================================
// U_Dialog
// AppKit - Application Development Kit
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Dialog;

interface

uses
  Windows,
  SysUtils,
  Controls,
  U_BaseType;

type
  // Enumerated types
  TRLDialogKind = (dkInformation, dkWarning, dkError, dkQuestion);
  TRLDialogButton = (dbOk, dbYes, dbNo, dbCancel, dbRetry, dbIgnore, dbAbort,
    dbAll, dbYesToAll, dbNoToAll, dbDetails);
  TRLDialogButtons = set of TRLDialogButton;

  // References
  TRLDialogClass = class of TRLDialog;

  // TRLDialog
  TRLDialog = class(TRLBaseType)
  public
    function Show(const AMessage: string): TRLDialogButton; overload; virtual;
    function Show(const AMessage: string; const AKind: TRLDialogKind): TRLDialogButton; overload; virtual;
    function Show(const AMessage: string; const AKind: TRLDialogKind; const AButtons: TRLDialogButtons; const ADefault: TRLDialogButton): TRLDialogButton; overload;
    function Show(const AMessage: string; const AKind: TRLDialogKind; const AButtons: TRLDialogButtons; const ADefault: TRLDialogButton; const ADetails: string): TRLDialogButton; overload; virtual; abstract;
    procedure ShowWarning(const AMessage: string); virtual;
    procedure ShowError(const AMessage: string); overload; virtual;
    procedure ShowError(const AMessage, ADetails: string); overload; virtual;
    function ShowQuestion(const AMessage: string; const AButtons: TRLDialogButtons; const ADefault: TRLDialogButton): TRLDialogButton; virtual;
    class function GetInstanceType: TRLDialogClass;
    class procedure SetInstanceType(const AValue: TRLDialogClass);
    class function Instance: TRLDialog;
    class procedure DeleteInstance;
  end;

resourcestring
  SRLInformation  = 'Informa��o';
  SRLWarning = 'Aviso';
  SRLError = 'Erro';
  SRLQuestion = 'Pergunta';
  SRLOk = '&Ok';
  SRLYes = '&Sim';
  SRLNo = '&N�o';
  SRLCancel = '&Cancelar';
  SRLRetry = '&Repetir';
  SRLIgnore = '&Ignorar';
  SRLAbort = '&Abortar';
  SRLAll = '&Todos';
  SRLYesToAll = 'S&im para todos';
  SRLNoToAll = 'N�&o para todos';
  SRLOpenDetails = '&Detalhes +';
  SRLCloseDetails = '&Detalhes -';

var
  RLDialogKindIcon: array[TRLDialogKind] of string = ('RES_ICO_MK_INFORMATION',
    'RES_ICO_MK_WARNING', 'RES_ICO_MK_ERROR', 'RES_ICO_MK_QUESTION');
  RLDialogKindCaption: array[TRLDialogKind] of string = (SRLInformation,
    SRLWarning, SRLError, SRLQuestion);
  RLDialogButtonCaption: array[TRLDialogButton] of string = (SRLOk, SRLYes,
    SRLNo, SRLCancel, SRLRetry, SRLIgnore, SRLAbort, SRLAll, SRLYesToAll,
    SRLNoToAll, SRLOpenDetails);

// Utils
function DialogButtonToModalResult(const AValue: TRLDialogButton): Integer;
function ModalResultToDialogButton(const AValue: Integer): TRLDialogButton;

implementation

var
  RLInstanceType: TRLDialogClass;
  RLInstance: TRLDialog = nil;
  
// Utils
function DialogButtonToModalResult(const AValue: TRLDialogButton): Integer;
begin
  case AValue of
    dbOk: Result := mrOk;
    dbYes: Result := mrYes;
    dbNo: Result := mrNo;
    dbCancel: Result := mrCancel;
    dbRetry: Result := mrRetry;
    dbIgnore: Result := mrIgnore;
    dbAbort: Result := mrAbort;
    dbAll: Result := mrAll;
    dbYesToAll: Result := mrYesToAll;
    dbNoToAll: Result := mrNoToAll;
    dbDetails: Result := mrNone;
  else
    Result := mrOk;
  end;
end;

function ModalResultToDialogButton(const AValue: Integer): TRLDialogButton;
begin
  case AValue of
    mrOk: Result := dbOk;
    mrYes: Result := dbYes;
    mrNo: Result := dbNo;
    mrCancel: Result := dbCancel;
    mrRetry: Result := dbRetry;
    mrIgnore: Result := dbIgnore;
    mrAbort: Result := dbAbort;
    mrAll: Result := dbAll;
    mrYesToAll: Result := dbYesToAll;
    mrNoToAll: Result := dbNoToAll;
  else
    Result := dbOk;
  end;
end;

{ TRLDialog }

class function TRLDialog.GetInstanceType: TRLDialogClass;
begin
  Result := RLInstanceType;
end;

class procedure TRLDialog.SetInstanceType(const AValue: TRLDialogClass);
begin
  if RLInstanceType <> AValue then
  begin
    if Assigned(RLInstance) then
      FreeAndNil(RLInstance);
    RLInstanceType := AValue;
  end;
end;

function TRLDialog.Show(const AMessage: string): TRLDialogButton;
begin
  Result := Show(AMessage, dkInformation);
end;

function TRLDialog.Show(const AMessage: string;
  const AKind: TRLDialogKind): TRLDialogButton;
begin
  Result := Show(AMessage, AKind, [dbOk], dbOk);
end;

function TRLDialog.Show(const AMessage: string; const AKind: TRLDialogKind;
  const AButtons: TRLDialogButtons;
  const ADefault: TRLDialogButton): TRLDialogButton;
begin
  Result := Show(AMessage, AKind, AButtons, ADefault, '');
end;

procedure TRLDialog.ShowWarning(const AMessage: string);
begin
  Show(AMessage, dkWarning, [dbOk], dbOk);
end;

procedure TRLDialog.ShowError(const AMessage: string);
begin
  ShowError(AMessage, '');
end;

procedure TRLDialog.ShowError(const AMessage, ADetails: string);
begin
  Show(AMessage, dkError, [dbOk], dbOk, ADetails);
end;

function TRLDialog.ShowQuestion(const AMessage: string;
  const AButtons: TRLDialogButtons;
  const ADefault: TRLDialogButton): TRLDialogButton;
begin
  Result := Show(AMessage, dkQuestion, AButtons, ADefault, '');
end;

class function TRLDialog.Instance: TRLDialog;
begin
  if not Assigned(RLInstance) and Assigned(RLInstanceType) then
    RLInstance := TRLDialog(RLInstanceType.Create);
  Result := RLInstance;
end;

class procedure TRLDialog.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;
  
finalization
  TRLDialog.DeleteInstance;

end.
