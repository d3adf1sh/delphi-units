//=============================================================================
// U_ArrayType
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ArrayType;

interface

uses
  SysUtils,
  Classes,
  XMLIntf,
  U_BaseType,
  U_InterfacedObject,
  U_Enumerable,
  U_Enumerator,
  U_Serializer;

const
  CType = 'type';

type
  // TRLArrayType
  TRLArrayType = class(TRLBaseType, IRLEnumerable, IRLSerializer)
  private
    FItems: array of TPersistent;
    FFreeValues: Boolean;
    function GetItem(const AIndex: Integer): TPersistent;
    procedure SetItem(const AIndex: Integer; const AValue: TPersistent);
    function GetCount: Integer;
  protected
    function GetItemIdentifier: string; virtual;
    function GetItemsIdentifier: string; virtual;
  public
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Add(const AValue: TPersistent);
    procedure Insert(const AIndex: Integer; const AValue: TPersistent);
    procedure Delete(const AIndex: Integer);
    procedure Clear; virtual;
    function GetEnumerator: IRLEnumerator; virtual;
    procedure ToXML(const ANode: IXMLNode); virtual;                  
    procedure FromXML(const ANode: IXMLNode); virtual;
    property Items[const AIndex: Integer]: TPersistent read GetItem write SetItem; default;
    property Count: Integer read GetCount;
    property FreeValues: Boolean read FFreeValues write FFreeValues;
  end;

  // TRLArrayTypeEnumerator
  TRLArrayTypeEnumerator = class(TRLInterfacedObject, IRLEnumerator)
  private
    FInstance: TRLArrayType;
    FItem: TPersistent;
    FIndex: Integer;
  public
    function GetCurrent: TObject; virtual;
    function Next: Boolean; virtual;
    procedure Reset; virtual;
    property Instance: TRLArrayType read FInstance write FInstance;
  end;

implementation

uses
  Variants,
  U_RTTIContext;

{ TRLArrayType }

destructor TRLArrayType.Destroy;
begin
  Clear;
  inherited;
end;

function TRLArrayType.GetItem(const AIndex: Integer): TPersistent;
begin
  Result := FItems[AIndex];
end;

procedure TRLArrayType.SetItem(const AIndex: Integer;
  const AValue: TPersistent);
begin
  FItems[AIndex] := AValue;
end;

function TRLArrayType.GetCount: Integer;
begin
  Result := Length(FItems);
end;

function TRLArrayType.GetItemIdentifier: string;
begin
  Result := 'Item';
end;

function TRLArrayType.GetItemsIdentifier: string;
begin
  Result := 'Items';
end;

procedure TRLArrayType.Assign(Source: TPersistent);
var
  lArray: TRLArrayType;
  lItem: TRLBaseType;
  I: Integer;
begin
  if Source is TRLArrayType then
  begin
    Clear;
    lArray := TRLArrayType(Source);
    for I := 0 to Pred(lArray.Count) do
      if lArray[I] is TRLBaseType then
      begin
        lItem := TRLBaseTypeClass(lArray[I].ClassType).Create;
        lItem.Assign(lArray[I]);
        Add(lItem);
      end;
      
    FreeValues := lArray.FreeValues;
  end;
end;

procedure TRLArrayType.Add(const AValue: TPersistent);
begin
  SetLength(FItems, Succ(Count));
  FItems[Pred(Count)] := AValue;
end;

procedure TRLArrayType.Insert(const AIndex: Integer;
  const AValue: TPersistent);
begin
  if (AIndex > -1) and (AIndex <= Count) then
  begin
    SetLength(FItems, Succ(Count));
    if AIndex < Pred(Count) then
      Move(FItems[AIndex], FItems[Succ(AIndex)],
        SizeOf(TPersistent) * (Count - AIndex));
    FItems[AIndex] := AValue;
  end;  
end;

procedure TRLArrayType.Delete(const AIndex: Integer);
begin
  if Count > 0 then
  begin
    if FFreeValues and Assigned(FItems[AIndex]) then
      FreeAndNil(FItems[AIndex]);
    if (AIndex > -1) and (AIndex < Count) then
      Move(FItems[Succ(AIndex)], FItems[AIndex],
        SizeOf(TPersistent) * (Count - AIndex));
    SetLength(FItems, Pred(Count));
  end;
end;

procedure TRLArrayType.Clear;
var
  I: Integer;
begin
  if FFreeValues then
    for I := 0 to Pred(Count) do
      if Assigned(FItems[I]) then
        FreeAndNil(FItems[I]);
  SetLength(FItems, 0);
end;

function TRLArrayType.GetEnumerator: IRLEnumerator;
var
  lEnumerator: TRLArrayTypeEnumerator;
begin
  lEnumerator := TRLArrayTypeEnumerator.Create;
  lEnumerator.Instance := Self;
  Result := lEnumerator;
end;

procedure TRLArrayType.ToXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  lNode: IXMLNode;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesToXML(Self, ANode);
    lNodes := ANode.AddChild(GetItemsIdentifier);
    for I := 0 to Pred(Count) do
      if Items[I] is TRLBaseType then
      begin
        lNode := lNodes.AddChild(GetItemIdentifier);
        lNode.Attributes[CType] := Items[I].ClassName;
        TRLRTTIContext.Instance.ObjectToXML(Items[I], lNode);
      end;
  end;
end;

procedure TRLArrayType.FromXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  lItem: TRLBaseType;
  lClass: TClass;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    Clear;
    TRLRTTIContext.Instance.AttributesFromXML(Self, ANode);
    lNodes := ANode.ChildNodes.FindNode(GetItemsIdentifier);
    if Assigned(lNodes) then
      for I := 0 to Pred(lNodes.ChildNodes.Count) do
      begin
        lClass := GetClass(VarToStr(lNodes.ChildNodes[I].Attributes[CType]));
        if Assigned(lClass) and lClass.InheritsFrom(TRLBaseType) then
        begin
          lItem := TRLBaseTypeClass(lClass).Create;
          TRLRTTIContext.Instance.ObjectFromXML(lItem, lNodes.ChildNodes[I]);
          Add(lItem);
        end;
      end;
  end;
end;

{ TRLArrayTypeEnumerator }

function TRLArrayTypeEnumerator.GetCurrent: TObject;
begin
  Result := FItem;
end;

function TRLArrayTypeEnumerator.Next: Boolean;
begin
  if Assigned(FInstance) and (FIndex < FInstance.Count) then
  begin
    FItem := FInstance.Items[FIndex];
    Inc(FIndex);
  end
  else
    FItem := nil;
  Result := Assigned(FItem);
end;

procedure TRLArrayTypeEnumerator.Reset;
begin
  FIndex := 0;
end;

end.
