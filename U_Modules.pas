//=============================================================================
// U_Modules
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Modules;

interface

uses
  Windows,
  SysUtils,
  Classes,
  U_Exception,
  U_KeyedCollection;

type
  // Enumerations
  TRLModuleFormat = (mfPackage, mfDLL);

  // Exceptions
  ERLModuleAlreadyExists = class(ERLException);
  ERLModuleNotFound = class(ERLException);
  ERLCouldNotLoadModule = class(ERLException);
  ERLCouldNotUnloadModule = class(ERLException);
  ERLModuleNotLoaded = class(ERLException);

  // TRLModule
  TRLModule = class(TRLKeyedCollectionItem)
  private
    FFormat: TRLModuleFormat;
    FEnabled: Boolean;
    FHandle: THandle;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
    procedure KeyAlreadyExists(const AValue: string); override;
    procedure KeyChanged; override;
    procedure CheckHandle; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Load(const AForce: Boolean); virtual;
    function Loaded: Boolean; virtual;
    procedure Unload; virtual;
    function Test(out AError: string): Boolean; virtual;
    function GetMethod(const AName: string): Pointer; virtual;
    function TryGetMethod(const AName: string; out ARef: Pointer): Boolean; virtual;
    function ContainsMethod(const AName: string): Boolean; virtual;
    property Handle: THandle read FHandle;
  published
    property FileName: string read GetKey write SetKey;
    property Format: TRLModuleFormat read FFormat write FFormat;
    property Enabled: Boolean read FEnabled write FEnabled;
  end;

  // TRLModules
  TRLModules = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLModule;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLModule;
    function Insert(const AIndex: Integer): TRLModule;
    function Find(const AFileName: string): TRLModule;
    function FindByFileName(const AFileName: string): TRLModule;
    function Contains(const AFileName: string): Boolean; reintroduce; virtual;
    function Remove(const AFileName: string): Boolean; reintroduce; virtual;
    procedure Load(const AFileName: string; const AForce: Boolean); virtual;
    function Loaded(const AFileName: string): Boolean; virtual;
    procedure Unload(const AFileName: string); virtual;
    function Test(const AFileName: string; out AError: string): Boolean; virtual;
    function GetMethod(const AFileName, AName: string): Pointer; virtual;
    function TryGetMethod(const AFileName, AName: string; out ARef: Pointer): Boolean; virtual;
    function ContainsMethod(const AFileName, AName: string): Boolean; virtual;
    procedure LoadAll(const AForce: Boolean); virtual;
    procedure UnloadAll; virtual;
    function AllLoaded: Boolean; virtual;
    property Items[const AIndex: Integer]: TRLModule read GetItem; default;
  end;

implementation

uses
  U_NTUtils;

{ TRLModule }

constructor TRLModule.Create(Collection: TCollection);
begin
  inherited;
  FEnabled := True;
end;

destructor TRLModule.Destroy;
begin
  Unload;
  inherited;
end;

function TRLModule.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TRLModule.SetKey(const AValue: string);
begin
  inherited;
end;

procedure TRLModule.KeyAlreadyExists(const AValue: string);
begin
  raise ERLModuleAlreadyExists.CreateFmt('Module "%s" already exists.',
    [AValue]);
end;

procedure TRLModule.KeyChanged;
var
  sExtension: string;
begin
  inherited;
  sExtension := TRLNTUtils.GetExtensionOfFile(FileName);
  if SameText(sExtension, '.BPL') then
    FFormat := mfPackage
  else
    if SameText(sExtension, '.DLL') then
      FFormat := mfDLL;
end;

procedure TRLModule.CheckHandle;
begin
  if not Loaded then
    raise ERLModuleNotLoaded.Create('Module not loaded.');
end;

procedure TRLModule.Assign(Source: TPersistent);
var
  lModule: TRLModule;
begin
  inherited;
  if Source is TRLModule then
  begin
    lModule := TRLModule(Source);
    Format := lModule.Format;
    Enabled := lModule.Enabled;
  end;
end;

procedure TRLModule.Load(const AForce: Boolean);
begin
  if (FEnabled or AForce) and not Loaded then
    try
      case FFormat of
        mfPackage: FHandle := LoadPackage(FileName);
        mfDLL:
          begin
            FHandle := LoadLibrary(PChar(FileName));
            if FHandle = 0 then
              raise Exception.Create(TRLNTUtils.GetLastError);
          end;
      end;
    except
      on E: Exception do
        raise ERLCouldNotLoadModule.Create('Could not load module.' +
          sLineBreak + E.Message);
    end;
end;

function TRLModule.Loaded: Boolean;
begin
  Result := FHandle > 0;
end;

procedure TRLModule.Unload;
begin
  if Loaded then
    try
      try
        case FFormat of
          mfPackage: UnloadPackage(FHandle);
          mfDLL:
            if not FreeLibrary(FHandle) then
              raise Exception.Create(TRLNTUtils.GetLastError);
        end;
      except
        on E: Exception do
          raise ERLCouldNotUnloadModule.Create('Could not unload module.' +
            sLineBreak + E.Message);
      end;                           
    finally
      FHandle := 0;
    end;
end;

function TRLModule.Test(out AError: string): Boolean;
var
  lModule: TRLModule;
begin
  try
    lModule := TRLModule.Create(nil);
    try
      lModule.Assign(Self);
      lModule.Load(True);
      lModule.Unload;
    finally
      FreeAndNil(lModule);
    end;

    AError := '';
    Result := True;
  except
    on E: Exception do
    begin
      AError := E.Message;
      Result := False;
    end;
  end;
end;

function TRLModule.GetMethod(const AName: string): Pointer;
begin
  CheckHandle;
  Result := GetProcAddress(FHandle, PChar(AName));
end;

function TRLModule.TryGetMethod(const AName: string;
  out ARef: Pointer): Boolean;
begin
  ARef := GetMethod(AName);
  Result := Assigned(ARef);
end;

function TRLModule.ContainsMethod(const AName: string): Boolean;
var
  ptrRef: Pointer;
begin
  Result := TryGetMethod(AName, ptrRef);
end;

{ TRLModules }

constructor TRLModules.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 128;
  CaseSensitive := False;
end;

function TRLModules.GetItem(const AIndex: Integer): TRLModule;
begin
  Result := TRLModule(inherited Items[AIndex]);
end;

function TRLModules.GetItemClass: TCollectionItemClass;
begin
  Result := TRLModule;
end;

function TRLModules.Add: TRLModule;
begin
  Result := TRLModule(inherited Add);
end;

function TRLModules.Insert(const AIndex: Integer): TRLModule;
begin
  Result := TRLModule(inherited Insert(AIndex));
end;

function TRLModules.Find(const AFileName: string): TRLModule;
begin
  Result := TRLModule(inherited Find(AFileName));
end;

function TRLModules.FindByFileName(const AFileName: string): TRLModule;
begin
  Result := Find(AFileName);
  if not Assigned(Result) then
    raise ERLModuleNotFound.CreateFmt('Module "%s" not found.', [AFileName]);
end;

function TRLModules.Contains(const AFileName: string): Boolean;
begin
  Result := inherited Contains(AFileName);
end;

function TRLModules.Remove(const AFileName: string): Boolean;
begin
  Result := inherited Remove(AFileName);
end;

procedure TRLModules.Load(const AFileName: string; const AForce: Boolean);
var
  lModule: TRLModule;
begin
  lModule := Find(AFileName);
  if Assigned(lModule) then
    lModule.Load(AForce);
end;

function TRLModules.Loaded(const AFileName: string): Boolean;
var
  lModule: TRLModule;
begin
  lModule := Find(AFileName);
  Result := Assigned(lModule) and lModule.Loaded;
end;

procedure TRLModules.Unload(const AFileName: string);
var
  lModule: TRLModule;
begin
  lModule := Find(AFileName);
  if Assigned(lModule) then
    lModule.Unload;
end;

function TRLModules.Test(const AFileName: string; out AError: string): Boolean;
var
  lModule: TRLModule;
begin
  lModule := Find(AFileName);
  if Assigned(lModule) then
    Result := lModule.Test(AError)
  else
  begin
    AError := '';
    Result := False;
  end;
end;

function TRLModules.GetMethod(const AFileName, AName: string): Pointer;
var
  lModule: TRLModule;
begin
  lModule := Find(AFileName);
  if Assigned(lModule) then
    Result := lModule.GetMethod(AName)
  else
    Result := nil;
end;

function TRLModules.TryGetMethod(const AFileName, AName: string;
  out ARef: Pointer): Boolean;
var
  lModule: TRLModule;
begin
  lModule := Find(AFileName);
  Result := Assigned(lModule) and lModule.TryGetMethod(AName, ARef);
  if not Result then
    ARef := nil;
end;

function TRLModules.ContainsMethod(const AFileName, AName: string): Boolean;
var
  lModule: TRLModule;
begin
  lModule := Find(AFileName);
  Result := Assigned(lModule) and lModule.ContainsMethod(AName);
end;

procedure TRLModules.LoadAll(const AForce: Boolean);
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Load(AForce);
end;

procedure TRLModules.UnloadAll;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Unload;
end;

function TRLModules.AllLoaded: Boolean;
var
  I: Integer;
begin
  Result := True;
  I := 0;
  while (I <= Pred(Count)) and Result do
    if not Items[I].Loaded then
      Result := False
    else
      Inc(I);
end;

end.
