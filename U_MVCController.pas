//=============================================================================
// U_MVCController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MVCController;

{$I RL.INC}

interface

{ TODO -oRafael : Flag FInitialized. }

uses
  SysUtils,
  Classes,
  Forms,
  U_BaseType,
  U_Exception,
  U_HashTable,
  U_Actions,
  U_Bindings,
  U_MVCAbstract;

type
  // Forward declarations
  TRLMVCController = class; 

  // References
  TRLMVCControllerClass = class of TRLMVCController;

  // Exceptions
  ERLMVCControllerNotFound = class(ERLException);
  ERLMVCControllerIsNull = class(ERLException); //External

  // TRLMVCControllerThread
  TRLMVCControllerThread = class(TThread)
  private
    FController: TRLMVCController;
  protected
    procedure Execute; override;
  public
    property Controller: TRLMVCController read FController write FController;
  end;

  // TRLMVCController
  TRLMVCController = class(TRLBaseType, IRLMVCController)
  private
    FTitle: string;
    FActions: TRLActions;
    FBindings: TRLBindings;
    FModel: TComponent;
    FView: TComponent;
    FModelIntf: IRLMVCModel;
    FViewIntf: IRLMVCView;
    FThread: TRLMVCControllerThread;
    FThreadError: Exception;
    FFreeModel: Boolean;
    FFreeView: Boolean;
    function GetTitle: string;
    procedure SetTitle(const AValue: string);
    function GetActions: TRLActions;
    function GetBindings: TRLBindings;
    function GetModel: TComponent;
    procedure SetModel(const AValue: TComponent);
    function GetView: TComponent;
    procedure SetView(const AValue: TComponent);
    function GetModelIntf: IRLMVCModel;
    function GetViewIntf: IRLMVCView;
  protected
    function CreateActions: TRLActions; virtual;
    function CreateBindings: TRLBindings; virtual;
    procedure ModelChanged; virtual;
    procedure ViewChanged; virtual;
    function PostData: Boolean; virtual;
    procedure DispatchWriteBindings; virtual;
    procedure DispatchValidate; virtual;
    function DispatchThreadStarted: Boolean; virtual;
    function DispatchThreadCompleted(const AError: Exception): Boolean; virtual;
    procedure WriteBindings; virtual;
    procedure Validate; virtual;
    procedure ThreadStarted; virtual;
    procedure ThreadCompleted; virtual;
    procedure CheckModelIntf; virtual;
    procedure CheckViewIntf; virtual;
    property ModelIntf: IRLMVCModel read GetModelIntf;
    property ViewIntf: IRLMVCView read GetViewIntf;
    property Thread: TRLMVCControllerThread read FThread;
    property ThreadError: Exception read FThreadError;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize; virtual;
    procedure Display; virtual;
    function CanDisplayModal: Boolean; virtual;
    function DisplayModal: Boolean; virtual;
    function Post: Boolean; virtual;
    function PostThread: Boolean; virtual;
    function Posting: Boolean; virtual;
    property Title: string read GetTitle write SetTitle;
    property Actions: TRLActions read GetActions;
    property Bindings: TRLBindings read GetBindings;
    property Model: TComponent read GetModel write SetModel;
    property View: TComponent read GetView write SetView;
    class function GetController(const AName: string): TRLMVCController;
    class function FindController(const AName: string): TRLMVCController;
  end;

  // TRLMVCControllerCache
  TRLMVCControllerCache = class(TRLBaseType)
  private
    FControllers: TRLHashTable;
  protected
    function CreateControllers: TRLHashTable; virtual;
    property Controllers: TRLHashTable read FControllers;
  public
    constructor Create; override;
    destructor Destroy; override;
    function Add(const AInstance: TRLMVCController): Boolean; virtual;
    function Find(const AReference: TRLMVCControllerClass; out AInstance: TRLMVCController): Boolean; virtual;
    function Contains(const AReference: TRLMVCControllerClass): Boolean; virtual;
    function Remove(const AInstance: TRLMVCController): Boolean; virtual;
    class function Instance: TRLMVCControllerCache;
    class procedure DeleteInstance;
  end;

implementation

uses
  ActiveX,
  StrUtils,
  U_MVCDynamicView;

var
  RLInstance: TRLMVCControllerCache;

{ TRLMVCControllerThread }

procedure TRLMVCControllerThread.Execute;
begin
  if Assigned(FController) then
  begin
    CoInitialize(nil);
    try
      FController.Post;
    finally
      FController.FThread := nil;
      CoUninitialize;
    end;
  end;
end;

{ TRLMVCController }

constructor TRLMVCController.Create;
begin
  inherited;
  TRLMVCControllerCache.Instance.Add(Self);
end;

destructor TRLMVCController.Destroy;
begin
  if Assigned(FActions) then
    FreeAndNil(FActions);
  if Assigned(FBindings) then
    FreeAndNil(FBindings);
  if Assigned(FModel) and FFreeModel then
    FreeAndNil(FModel);
  if Assigned(FView) and FFreeView and
    (not Assigned(ViewIntf) or not ViewIntf.GetFreeController) then
    FreeAndNil(FView);
  TRLMVCControllerCache.Instance.Remove(Self);
  inherited;
end;

function TRLMVCController.GetTitle: string;
begin
  Result := FTitle;
end;

procedure TRLMVCController.SetTitle(const AValue: string);
begin
  FTitle := AValue;
end;

function TRLMVCController.GetActions: TRLActions;
begin
  if not Assigned(FActions) then
    FActions := CreateActions;
  Result := FActions;
end;

function TRLMVCController.GetBindings: TRLBindings;
begin
  if not Assigned(FBindings) then
    FBindings := CreateBindings;
  Result := FBindings;
end;

function TRLMVCController.GetModel: TComponent;
var
  lClass: TComponentClass;
begin
  if not Assigned(FModel) then
  begin
    lClass := TComponentClass(
      GetClass(AnsiReplaceText(ClassName, 'Controller', 'Model')));
    if Assigned(lClass) then
    begin
      FModel := lClass.Create(nil);
      FFreeModel := True;
    end;
  end;

  Result := FModel;
end;

procedure TRLMVCController.SetModel(const AValue: TComponent);
begin
  if AValue <> FModel then
  begin
    if Assigned(FModel) and FFreeModel then
    begin
      FreeAndNil(FModel);
      FFreeModel := False;
    end;

    FModel := AValue;
    FModelIntf := nil;
    ModelChanged;
  end;
end;

function TRLMVCController.GetView: TComponent;
var
  lClass: TComponentClass;
begin
  if not Assigned(FView) then
  begin
    lClass := TComponentClass(
      GetClass(AnsiReplaceText(ClassName, 'Controller', 'View')));
    if Assigned(lClass) then
    begin
      if lClass.InheritsFrom(TRLMVCDynamicView) then
        FView := TRLMVCDynamicViewClass(lClass).CreateNew(Application)
      else
        FView := lClass.Create(Application);
      FFreeView := True;
    end;
  end;

  Result := FView;
end;

procedure TRLMVCController.SetView(const AValue: TComponent);
begin
  if AValue <> FView then
  begin
    if Assigned(FView) and FFreeView then
    begin
      FreeAndNil(FView);
      FFreeView := False;
    end;

    FView := AValue;
    FViewIntf := nil;
    ViewChanged;
  end;
end;

function TRLMVCController.GetModelIntf: IRLMVCModel;
begin
  if not Assigned(FModelIntf) and Assigned(Model) then
  begin
    Model.GetInterface(IRLMVCModel, FModelIntf);
    if Assigned(FModelIntf) then
      FModelIntf.SetController(Self);
  end;

  Result := FModelIntf;
end;

function TRLMVCController.GetViewIntf: IRLMVCView;
begin
  if not Assigned(FViewIntf) and Assigned(View) then
  begin
    View.GetInterface(IRLMVCView, FViewIntf);
    if Assigned(FViewIntf) then
      FViewIntf.SetController(Self);
  end;

  Result := FViewIntf;
end;

function TRLMVCController.CreateActions: TRLActions;
begin
  Result := TRLActions.Create;
end;

function TRLMVCController.CreateBindings: TRLBindings;
begin
  Result := TRLBindings.Create;
end;

procedure TRLMVCController.ModelChanged;
begin
end;

procedure TRLMVCController.ViewChanged;
begin
end;

function TRLMVCController.PostData: Boolean;
begin
  Result := True;
end;

procedure TRLMVCController.DispatchWriteBindings;
begin
  if Assigned(FThread) then
    FThread.Synchronize(WriteBindings)
  else
    WriteBindings;
end;

procedure TRLMVCController.DispatchValidate;
begin
  if Assigned(FThread) then
    FThread.Synchronize(Validate)
  else
    Validate;
end;

function TRLMVCController.DispatchThreadStarted: Boolean;
begin
  Result := Assigned(FThread);
  if Result then
    FThread.Synchronize(ThreadStarted);
end;

function TRLMVCController.DispatchThreadCompleted(
  const AError: Exception): Boolean;
begin
  Result := Assigned(FThread);
  if Result then
  begin
    FThreadError := AError;
    try
      FThread.Synchronize(ThreadCompleted);
    finally
      FThreadError := nil;
    end;
  end;
end;

procedure TRLMVCController.WriteBindings;
begin
  if Assigned(FBindings) then
    FBindings.WriteAll;
end;

procedure TRLMVCController.Validate;
begin
end;

procedure TRLMVCController.ThreadStarted;
begin
  { TODO -oRafael : Implementar. }
end;

procedure TRLMVCController.ThreadCompleted;
begin
  { TODO -oRafael : Implementar. }
end;

procedure TRLMVCController.CheckModelIntf;
begin
  if Assigned(Model) and not Assigned(ModelIntf) then
    raise ERLMVCCouldNotGetModelInterface.Create(
      'Could not get model interface.');
end;

procedure TRLMVCController.CheckViewIntf;
begin
  if not Assigned(ViewIntf) then
    raise ERLMVCCouldNotGetViewInterface.Create(
      'Could not get view interface.');
end;

procedure TRLMVCController.Initialize;
begin
  CheckModelIntf;
  CheckViewIntf;
  if Assigned(ModelIntf) then
    ModelIntf.Initialize;
  ViewIntf.Initialize;
  ViewIntf.SetTitle(Title);
end;

procedure TRLMVCController.Display;
begin
  CheckViewIntf;
  ViewIntf.Display;
end;

function TRLMVCController.CanDisplayModal: Boolean;
begin
  CheckViewIntf;
  Result := ViewIntf.CanDisplayModal;
end;

function TRLMVCController.DisplayModal: Boolean;
begin
  CheckViewIntf;
  Result := ViewIntf.DisplayModal;
end;

function TRLMVCController.Post: Boolean;
begin
  try
    DispatchThreadStarted;
    DispatchWriteBindings;
    DispatchValidate;
    Result := PostData;
    DispatchThreadCompleted(nil);
  except
    on E: Exception do
    begin
      Result := False;
      if not DispatchThreadCompleted(E) then
        raise;
    end;
  end;
end;

function TRLMVCController.PostThread: Boolean;
begin
  Result := not Assigned(FThread);
  if Result then
  begin
    FThread := TRLMVCControllerThread.Create(True);
    TRLMVCControllerThread(FThread).Controller := Self;
    FThread.FreeOnTerminate := True;
{$IFDEF D15UP}
    FThread.Start;
{$ELSE}
    FThread.Resume;
{$ENDIF}
  end;
end;

function TRLMVCController.Posting: Boolean;
begin
  Result := Assigned(FThread);
end;

class function TRLMVCController.GetController(
  const AName: string): TRLMVCController;
var
  lClass: TRLMVCControllerClass;
begin
  lClass := TRLMVCControllerClass(GetClass(Format('TRL%sController', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLMVCController.FindController(
  const AName: string): TRLMVCController;
begin
  Result := GetController(AName);
  if not Assigned(Result) then
    raise ERLMVCControllerNotFound.CreateFmt('Controller "%s" not found.',
      [AName]);
end;

{ TRLMVCControllerCache }

constructor TRLMVCControllerCache.Create;
begin
  inherited;
  FControllers := CreateControllers;  
end;

destructor TRLMVCControllerCache.Destroy;
begin
  FreeAndNil(FControllers);
  inherited;
end;

function TRLMVCControllerCache.CreateControllers: TRLHashTable;
begin
  Result := TRLHashTable.Create;
end;

function TRLMVCControllerCache.Add(const AInstance: TRLMVCController): Boolean;
begin
  if Assigned(AInstance) then
    Result := FControllers.Add(AInstance.ClassName, AInstance)
  else
    Result := False;
end;

function TRLMVCControllerCache.Find(const AReference: TRLMVCControllerClass;
  out AInstance: TRLMVCController): Boolean;
var
  lInstance: TObject;
begin
  if Assigned(AReference) then
  begin
    Result := FControllers.Find(AReference.ClassName, lInstance);
    AInstance := TRLMVCController(lInstance);
  end
  else
  begin
    Result := False;
    AInstance := nil;
  end;
end;

function TRLMVCControllerCache.Contains(
  const AReference: TRLMVCControllerClass): Boolean;
var
  lInstance: TRLMVCController;
begin
  Result := Find(AReference, lInstance);
end;

function TRLMVCControllerCache.Remove(
  const AInstance: TRLMVCController): Boolean;
begin
  if Assigned(AInstance) then
    Result := FControllers.Remove(AInstance.ClassName)
  else
    Result := False;
end;

class function TRLMVCControllerCache.Instance: TRLMVCControllerCache;
begin
  if not Assigned(RLInstance) then
    RLInstance := TRLMVCControllerCache.Create;
  Result := RLInstance;
end;

class procedure TRLMVCControllerCache.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLMVCControllerCache.DeleteInstance;

end.
