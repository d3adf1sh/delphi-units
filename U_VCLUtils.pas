//=============================================================================
// U_VCLUtils
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLUtils;

{$I RL.INC}

interface

uses
{$IFDEF D20UP}
  UITypes,
{$ELSE}
  StdCtrls,
{$ENDIF}
  U_BaseType;

type
  // TRLVCLUtils
  TRLVCLUtils = class(TRLBaseType)
  public
    class function EditCharCaseToString(const AValue: TEditCharCase): string;
    class function StringToEditCharCase(const AValue: string): TEditCharCase;
    class function ScrollStyleToString(const AValue: TScrollStyle): string;
    class function StringToScrollStyle(const AValue: string): TScrollStyle;
  end;

implementation

uses
  TypInfo;

{ TRLVCLUtils }

class function TRLVCLUtils.EditCharCaseToString(
  const AValue: TEditCharCase): string;
begin
  Result := GetEnumName(TypeInfo(TEditCharCase), Ord(AValue));
end;

class function TRLVCLUtils.StringToEditCharCase(
  const AValue: string): TEditCharCase;
begin
  Result := TEditCharCase(GetEnumValue(TypeInfo(TEditCharCase), AValue));
end;

class function TRLVCLUtils.ScrollStyleToString(
  const AValue: TScrollStyle): string;
begin
  Result := GetEnumName(TypeInfo(TScrollStyle), Ord(AValue));
end;

class function TRLVCLUtils.StringToScrollStyle(
  const AValue: string): TScrollStyle;
begin
  Result := TScrollStyle(GetEnumValue(TypeInfo(TScrollStyle), AValue));
end;

end.
