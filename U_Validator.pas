//=============================================================================
// U_Validator
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Validator;

interface

type
  // IRLValidator
  IRLValidator = interface
    ['{DD63A2E3-6C6D-4D4A-AD99-53D0964763C3}']
    procedure Validate;
  end;

implementation

end.
