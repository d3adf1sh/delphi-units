//=============================================================================
// U_MVCTask
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MVCTask;

{$I RL.INC}

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_Validator,
  U_FormException,
  U_MVCAbstract,
  U_MVCController;

type
  // Forward declarations
  TRLMVCTask = class;

  // References
  TRLMVCTaskClass = class of TRLMVCTask;

  // Exceptions
  TRLMVCCouldNotGetTaskViewInterface = class(ERLException);

  // Events
  TRLMVCTaskProgress = procedure(Sender: TObject; Description: string; Position, Count: Integer) of object;
  
  // IRLMVCTaskView
  IRLMVCTaskView = interface
    ['{DCB187BE-698B-4D8A-A9A2-42D9E13EF17B}']
    procedure StartTask;
    procedure CompleteTask(const AError: Exception);
    procedure UpdateTaskProgress(const ADescription: string; const APosition, ACount: Integer);
  end;

  // TRLMVCTaskThread
  TRLMVCTaskThread = class(TThread)
  private
    FTask: TRLMVCTask;
  protected
    procedure Execute; override;
  public
    property Task: TRLMVCTask read FTask write FTask;
  end;

  // TRLMVCTask
  TRLMVCTask = class(TRLBaseType, IRLValidator)
  private
    FParent: TRLMVCTask;
    FOnStarted: TNotifyEvent;
    FOnCompleted: TNotifyEvent;
    FOnProgress: TRLMVCTaskProgress;
    FOnError: TRLExceptionEvent;
    FThread: TRLMVCTaskThread;
    FProgressDescription: string;
    FProgressPosition: Integer;
    FProgressCount: Integer;
    FError: Exception;
  protected
    function DispatchStarted: Boolean; virtual;
    function DispatchCompleted: Boolean; virtual;
    procedure DispatchProgress(const ADescription: string); overload; virtual;
    procedure DispatchProgress(const ADescription: string; const APosition, ACount: Integer); overload; virtual;
    function DispatchError(const AError: Exception): Boolean; virtual;
    procedure DoStarted; virtual;
    procedure DoCompleted; virtual;
    procedure DoProgress; virtual;
    procedure DoError; virtual;
    property Thread: TRLMVCTaskThread read FThread;
    property ProgressDescription: string read FProgressDescription;
    property ProgressPosition: Integer read FProgressPosition;
    property ProgressCount: Integer read FProgressCount;
    property Error: Exception read FError;
  public
    constructor CreateTask(const AParent: TRLMVCTask); virtual;
    procedure Validate; virtual;
    function Run: Boolean; virtual; abstract;
    function RunThread: Boolean; virtual;
    function Running: Boolean; virtual;
    property Parent: TRLMVCTask read FParent write FParent;
    property OnStarted: TNotifyEvent read FOnStarted write FOnStarted;
    property OnCompleted: TNotifyEvent read FOnCompleted write FOnCompleted;
    property OnProgress: TRLMVCTaskProgress read FOnProgress write FOnProgress;
    property OnError: TRLExceptionEvent read FOnError write FOnError;
  end;

  // TRLMVCTaskController
  TRLMVCTaskController = class(TRLMVCController)
  private
    FTask: TRLMVCTask;
    FTaskViewIntf: IRLMVCTaskView;
    function GetTaskViewIntf: IRLMVCTaskView;
  protected
    procedure ViewChanged; override;
    procedure Validate; override;
    function CreateTask: TRLMVCTask; virtual; abstract;
    procedure CheckTaskViewIntf; virtual;
    procedure TaskStarted(Sender: TObject); virtual;
    procedure TaskCompleted(Sender: TObject); virtual;
    procedure TaskProgress(Sender: TObject; Description: string; Position, Count: Integer); virtual;
    procedure TaskError(Sender: TObject; Error: Exception); virtual;
    property TaskViewIntf: IRLMVCTaskView read GetTaskViewIntf;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize; override;
    function Post: Boolean; override;
    function Posting: Boolean; override;
    function PostThread: Boolean; override;
    property Task: TRLMVCTask read FTask;
  end;

implementation

uses
  ActiveX;

{ TRLMVCTaskThread }

procedure TRLMVCTaskThread.Execute;
begin
  if Assigned(FTask) then
  begin
    CoInitialize(nil);
    try
      FTask.Run;
    finally
      FTask.FThread := nil;
      CoUninitialize;
    end;
  end;
end;

{ TRLMVCTask }

constructor TRLMVCTask.CreateTask(const AParent: TRLMVCTask);
begin
  inherited Create;
  FParent := AParent;
end;

function TRLMVCTask.DispatchStarted: Boolean;
begin
  Result := Assigned(FThread);
  if Result then
    FThread.Synchronize(DoStarted);
end;

function TRLMVCTask.DispatchCompleted: Boolean;
begin
  Result := Assigned(FThread);
  if Result then
    FThread.Synchronize(DoCompleted);
end;

procedure TRLMVCTask.DispatchProgress(const ADescription: string);
begin
  DispatchProgress(ADescription, 0, 0);
end;

procedure TRLMVCTask.DispatchProgress(const ADescription: string;
  const APosition, ACount: Integer);
begin
  if Assigned(FParent) then
    FParent.DispatchProgress(ADescription, APosition, ACount)
  else
  begin
    FProgressDescription := ADescription;
    FProgressPosition := APosition;
    FProgressCount := ACount;
    try
      if Assigned(FThread) then
        FThread.Synchronize(DoProgress)
      else
        DoProgress;
    finally
      FProgressCount := 0;
      FProgressPosition := 0;
      FProgressDescription := '';
    end;
  end;
end;

function TRLMVCTask.DispatchError(const AError: Exception): Boolean;
begin
  Result := Assigned(FThread);
  if Result then
  begin
    FError := AError;
    try
      FThread.Synchronize(DoError)
    finally
      FError := nil;
    end;
  end;
end;

procedure TRLMVCTask.DoStarted;
begin
  if Assigned(FOnStarted) then
    FOnStarted(Self);
end;

procedure TRLMVCTask.DoCompleted;
begin
  if Assigned(FOnCompleted) then
    FOnCompleted(Self);
end;

procedure TRLMVCTask.DoProgress;
begin
  if Assigned(FOnProgress) then
    FOnProgress(Self, FProgressDescription, FProgressPosition, FProgressCount);
end;

procedure TRLMVCTask.DoError;
begin
  if Assigned(FOnError) then
    FOnError(Self, FError);
end;

procedure TRLMVCTask.Validate;
begin
end;

function TRLMVCTask.RunThread: Boolean;
begin
  Result := not Assigned(FThread);
  if Result then
  begin
    FThread := TRLMVCTaskThread.Create(True);
    TRLMVCTaskThread(FThread).Task := Self;
    FThread.FreeOnTerminate := True;
{$IFDEF D15UP}
    FThread.Start;
{$ELSE}
    FThread.Resume;
{$ENDIF}
  end;
end;

function TRLMVCTask.Running: Boolean;
begin
  Result := Assigned(FThread);
end;

{ TRLMVCTaskController }

constructor TRLMVCTaskController.Create;
begin
  inherited;
  FTask := CreateTask;
  FTask.OnStarted := TaskStarted;
  FTask.OnCompleted := TaskCompleted;
  FTask.OnProgress := TaskProgress;
  FTask.OnError := TaskError;
end;

destructor TRLMVCTaskController.Destroy;
begin
  FreeAndNil(FTask);
  inherited;
end;

function TRLMVCTaskController.GetTaskViewIntf: IRLMVCTaskView;
begin
  if not Assigned(FTaskViewIntf) and Assigned(View) then
    View.GetInterface(IRLMVCTaskView, FTaskViewIntf);
  Result := FTaskViewIntf;
end;

procedure TRLMVCTaskController.ViewChanged;
begin
  inherited;
  FTaskViewIntf := nil;
end;

procedure TRLMVCTaskController.Validate;
begin
  inherited;
  FTask.Validate;
end;

procedure TRLMVCTaskController.CheckTaskViewIntf;
begin
  if not Assigned(TaskViewIntf) then
    raise TRLMVCCouldNotGetTaskViewInterface.Create(
      'Could not get task view interface.');
end;

procedure TRLMVCTaskController.TaskStarted(Sender: TObject);
begin
  TaskViewIntf.StartTask;
end;

procedure TRLMVCTaskController.TaskCompleted(Sender: TObject);
begin
  TaskViewIntf.CompleteTask(nil);
end;

procedure TRLMVCTaskController.TaskProgress(Sender: TObject;
  Description: string; Position, Count: Integer);
begin
  TaskViewIntf.UpdateTaskProgress(Description, Position, Count);
end;

procedure TRLMVCTaskController.TaskError(Sender: TObject;
  Error: Exception);
var
  lError: ERLFormException;
begin
  lError := ERLFormException.CreateFormException(Error.Message, '', fekError);
  try
    TaskViewIntf.CompleteTask(lError);
  finally
    FreeAndNil(lError);
  end;
end;

procedure TRLMVCTaskController.Initialize;
begin
  inherited;
  CheckTaskViewIntf;
end;

function TRLMVCTaskController.Post: Boolean;
begin
  Result := inherited Post and FTask.Run;
end;

function TRLMVCTaskController.PostThread: Boolean;
begin
  Result := inherited Post and FTask.RunThread;
end;

function TRLMVCTaskController.Posting: Boolean;
begin
  Result := FTask.Running;
end;

end.
