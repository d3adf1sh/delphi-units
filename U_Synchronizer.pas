//=============================================================================
// U_Synchronizer
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Synchronizer;

interface

type
  // IRLSynchronizer
  IRLSynchronizer = interface
    ['{BF9EE576-D297-461F-8F70-04F3A97B78DB}']
    procedure Synchronize(const ASource: TObject);
  end;

implementation

end.