//=============================================================================
// U_FormManager
// AppKit - Application Development Kit
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_FormManager;

interface

uses
  Windows,
  SysUtils,
  Classes,
  Graphics,
  ImgList,
  ExtCtrls,
  Controls,
  U_BaseType;

type
  // Enumerations
  TRLEnvironmentStyle = (esMDI, esSDI);
  TRLResourceImageType = (ritPNG, ritIcon);

  // References
  TRLFormManagerClass = class of TRLFormManager;

  // TRLFormManager
  TRLFormManager = class(TRLBaseType)
  private
    FEnvironmentStyle: TRLEnvironmentStyle;
    FZoomWindows: Boolean;
    FUseSystemFont: Boolean;
  protected
    function GetImageResourceName(const AIdentifier: string; const AType: TRLResourceImageType; const ASize: Integer): string; virtual;
  public
    constructor Create; override;
    function LoadPNGFromResource(const AIdentifier: string; const ASize: Integer): TGraphic; virtual; abstract;
    function LoadIconFromResource(const AIdentifier: string; const ASize: Integer): TIcon; virtual;
    procedure AssignPNGFromResource(const AIdentifier: string; const ASize: Integer; const AImage: TImage); virtual;
    procedure AssignIconFromResource(const AIdentifier: string; const ASize: Integer; const AImage: TImage); virtual;
    procedure AssignImageFromResource(const AIdentifier: string; const AType: TRLResourceImageType; const ASize: Integer; const AImage: TImage); virtual;
    function AddPNGFromResource(const AIdentifier: string; const AImages: TCustomImageList): Integer; virtual; abstract;
    function AddIconFromResource(const AIdentifier: string; const AImages: TCustomImageList): Integer; virtual;
    function AddImageFromResource(const AIdentifier: string; const AType: TRLResourceImageType; const AImages: TCustomImageList): Integer; virtual;
    function CanSkipControl(const AControl: TControl; const AShift: TShiftState): Boolean; virtual;
    property EnvironmentStyle: TRLEnvironmentStyle read FEnvironmentStyle write FEnvironmentStyle;
    property ZoomWindows: Boolean read FZoomWindows write FZoomWindows;
    property UseSystemFont: Boolean read FUseSystemFont write FUseSystemFont;
    class function GetInstanceType: TRLFormManagerClass;
    class procedure SetInstanceType(const AValue: TRLFormManagerClass);
    class function Instance: TRLFormManager;
    class procedure DeleteInstance;
    { TODO -oRafael : Complementar. }
  end;

const
  RLResourceImageType: array[TRLResourceImageType] of string = ('PNG', 'ICO');

implementation

uses
  RTLConsts;

var  
  RLInstanceType: TRLFormManagerClass;
  RLInstance: TRLFormManager = nil;

{ TRLFormManager }

constructor TRLFormManager.Create;
begin
  inherited;
  FUseSystemFont := True;
end;

class function TRLFormManager.GetInstanceType: TRLFormManagerClass;
begin
  Result := RLInstanceType;
end;

class procedure TRLFormManager.SetInstanceType(
  const AValue: TRLFormManagerClass);
begin
  if RLInstanceType <> AValue then
  begin
    if Assigned(RLInstance) then
      FreeAndNil(RLInstance);
    RLInstanceType := AValue;
  end;
end;

function TRLFormManager.GetImageResourceName(const AIdentifier: string;
  const AType: TRLResourceImageType; const ASize: Integer): string;
begin
  Result := Format('RES_%s_%s_%d',
    [RLResourceImageType[AType], AIdentifier, ASize]);
end;

function TRLFormManager.LoadIconFromResource(const AIdentifier: string;
  const ASize: Integer): TIcon;
var
  lHandle: THandle;
begin
  lHandle := LoadImage(MainInstance,
    PChar(GetImageResourceName(AIdentifier, ritIcon, ASize)), IMAGE_ICON, ASize,
    ASize, 0);
  if lHandle = 0 then
    raise EResNotFound.CreateFmt(SResNotFound, [AIdentifier]);
  Result := TIcon.Create;
  Result.Handle := lHandle;
end;

procedure TRLFormManager.AssignPNGFromResource(const AIdentifier: string;
  const ASize: Integer; const AImage: TImage);
var
  lPNG: TGraphic;
begin
  if Assigned(AImage) then
  begin
    lPNG := LoadPNGFromResource(AIdentifier, ASize);
    try
      AImage.Picture.Graphic := lPNG;
    finally
      FreeAndNil(lPNG);
    end;
  end;
end;

procedure TRLFormManager.AssignIconFromResource(const AIdentifier: string;
  const ASize: Integer; const AImage: TImage);
var
  lIcon: TIcon;
begin
  if Assigned(AImage) then
  begin
    lIcon := LoadIconFromResource(AIdentifier, ASize);
    try
      AImage.Picture.Icon := lIcon;
    finally
      FreeAndNil(lIcon);
    end;
  end;
end;

procedure TRLFormManager.AssignImageFromResource(const AIdentifier: string;
  const AType: TRLResourceImageType; const ASize: Integer;
  const AImage: TImage);
begin
  case AType of
    ritPNG: AssignPNGFromResource(AIdentifier, ASize, AImage);
    ritIcon: AssignIconFromResource(AIdentifier, ASize, AImage);
  end;
end;

function TRLFormManager.AddIconFromResource(const AIdentifier: string;
  const AImages: TCustomImageList): Integer;
var
  lIcon: TIcon;
begin
  if Assigned(AImages) then
  begin
    lIcon := LoadIconFromResource(AIdentifier, AImages.Width);
    try
      Result := AImages.AddIcon(lIcon);
    finally
      FreeAndNil(lIcon);
    end;
  end
  else
    Result := -1;
end;

function TRLFormManager.AddImageFromResource(const AIdentifier: string;
  const AType: TRLResourceImageType; const AImages: TCustomImageList): Integer;
begin
  case AType of
    ritPNG: Result := AddPNGFromResource(AIdentifier, AImages);
    ritIcon: Result := AddIconFromResource(AIdentifier, AImages);
  else
    Result := -1;
  end;
end;

function TRLFormManager.CanSkipControl(const AControl: TControl;
  const AShift: TShiftState): Boolean;
begin
  Result := False;
end;

class function TRLFormManager.Instance: TRLFormManager;
begin
  if not Assigned(RLInstance) and Assigned(RLInstanceType) then
    RLInstance := TRLFormManager(RLInstanceType.Create);
  Result := RLInstance;
end;

class procedure TRLFormManager.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLFormManager.DeleteInstance;

end.
