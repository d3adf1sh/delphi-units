//=============================================================================
// U_RTTIContext
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_RTTIContext;

interface

uses
  SysUtils,
  Classes,
  XMLIntf,
  XMLDOM,
  XMLDoc,
  OXMLDOMVendor,
  U_BaseType,
  U_Exception,
  U_Serializer,
  U_AttributeSerializer,
  U_Synchronizer,
  U_RTTIAttributes,
  U_RTTIInspector,
  U_RTTIDefaultInspector;

const
  CType = 'type';

type
  // Enumerations
  TRLRTTISerializationFormat = (sfXML, sfReadableXML, sfJSon);

  // Exceptions
  ERLRTTICouldNotWriteAttribute = class(ERLException);

  // TRLRTTIContext
  TRLRTTIContext = class(TRLBaseType)
  private
    FInspector: TRLRTTIInspector;
    FFreeInspector: Boolean;
    function GetInspector: TRLRTTIInspector;
    procedure SetInspector(const AValue: TRLRTTIInspector);
  protected
    function CreateInspector: TRLRTTIInspector; virtual;
  public
    destructor Destroy; override;
    procedure ToFile(const AInstance: TObject; const AFileName: string; const AFormat: TRLRTTISerializationFormat); virtual;
    procedure ToStream(const AInstance: TObject; const AStream: TStream; const AFormat: TRLRTTISerializationFormat); virtual;
    function ToText(const AInstance: TObject; const AFormat: TRLRTTISerializationFormat): string; virtual;
    function ToXML(const AInstance: TObject; const AReadable: Boolean): string; virtual;
    function ToJSon(const AInstance: TObject): string; virtual;
    function FromFile(const AInstance: TObject; const AFileName: string; const AFormat: TRLRTTISerializationFormat): TObject; virtual;
    function FromStream(const AInstance: TObject; const AStream: TStream; const AFormat: TRLRTTISerializationFormat): TObject; virtual;
    function FromText(const AInstance: TObject; const AText: string; const AFormat: TRLRTTISerializationFormat): TObject; virtual;
    function FromXML(const AInstance: TObject; const AText: string): TObject; virtual;
    function FromJSon(const AInstance: TObject; const AText: string): TObject; virtual;
    procedure ObjectToXML(const AInstance: TObject; const ANode: IXMLNode); virtual;
    procedure ObjectFromXML(const AInstance: TObject; const ANode: IXMLNode); virtual;
    procedure AttributesToXML(const AInstance: TObject; const ANode: IXMLNode); virtual;
    procedure AttributesFromXML(const AInstance: TObject; const ANode: IXMLNode); virtual;
    procedure Synchronize(const ALeft, ARight: TObject); virtual;
    procedure SynchronizeAttributes(const ALeft, ARight: TObject); virtual;
    property Inspector: TRLRTTIInspector read GetInspector write SetInspector;
    class function Instance: TRLRTTIContext;
    class procedure DeleteInstance;
  end;

implementation

uses
  Variants,
  U_System;

var
  RLInstance: TRLRTTIContext = nil;

{ TRLRTTIContext }

destructor TRLRTTIContext.Destroy;
begin
  if Assigned(FInspector) then
    FreeAndNil(FInspector);
  inherited;
end;

function TRLRTTIContext.GetInspector: TRLRTTIInspector;
begin
  if not Assigned(FInspector) then
  begin
    FInspector := CreateInspector;
    FFreeInspector := True;
  end;

  Result := FInspector;
end;

procedure TRLRTTIContext.SetInspector(const AValue: TRLRTTIInspector);
begin
  if AValue <> FInspector then
  begin
    if Assigned(FInspector) and FFreeInspector then
    begin
      FreeAndNil(FInspector);
      FFreeInspector := False;
    end;
    
    FInspector := AValue;
  end;
end;

function TRLRTTIContext.CreateInspector: TRLRTTIInspector;
begin
  Result := TRLRTTIDefaultInspector.Create;
end;

procedure TRLRTTIContext.ToFile(const AInstance: TObject;
  const AFileName: string; const AFormat: TRLRTTISerializationFormat);
var
  lStream: TFileStream;
begin
  lStream := TFileStream.Create(AFileName, fmCreate);
  try
    ToStream(AInstance, lStream, AFormat);
  finally
    FreeAndNil(lStream);
  end;
end;

procedure TRLRTTIContext.ToStream(const AInstance: TObject;
  const AStream: TStream; const AFormat: TRLRTTISerializationFormat);
begin
  U_System.SetStreamData(AStream, ToText(AInstance, AFormat));
end;

function TRLRTTIContext.ToText(const AInstance: TObject;
  const AFormat: TRLRTTISerializationFormat): string;
begin
  case AFormat of
    sfXML: Result := ToXML(AInstance, False);
    sfReadableXML: Result := ToXML(AInstance, True);
    sfJSon: Result := ToJSon(AInstance);
  end;
end;

function TRLRTTIContext.ToXML(const AInstance: TObject;
  const AReadable: Boolean): string;
var
  lXML: TXMLDocument;
begin
  if Assigned(AInstance) then
  begin
    lXML := TXMLDocument.Create(nil);
    try
      lXML.DOMVendor := GetDOMVendor(SOXMLDOMVendor);
      lXML.Active := True;
      lXML.Version := '1.0';
      lXML.Encoding := 'iso-8859-1';
      lXML.DocumentElement := lXML.CreateElement('object', '');
      lXML.DocumentElement.Attributes[CType] := AInstance.ClassName;
      ObjectToXML(AInstance, lXML.DocumentElement);
      if AReadable then
        Result := XMLDoc.FormatXMLData(lXML.XML.Text)
      else
        Result := lXML.XML.Text;
    finally
      FreeAndNil(lXML);
    end;
  end
  else
    Result := '';
end;

function TRLRTTIContext.ToJSon(const AInstance: TObject): string;
begin
end;

function TRLRTTIContext.FromFile(const AInstance: TObject;
  const AFileName: string; const AFormat: TRLRTTISerializationFormat): TObject;
var
  lStream: TFileStream;
begin
  if FileExists(AFileName) then
  begin
    lStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyWrite);
    try
      Result := FromStream(AInstance, lStream, AFormat);
    finally
      FreeAndNil(lStream);
    end;
  end
  else
    Result := nil;
end;

function TRLRTTIContext.FromStream(const AInstance: TObject;
  const AStream: TStream; const AFormat: TRLRTTISerializationFormat): TObject;
begin
  Result := FromText(AInstance, U_System.GetStreamData(AStream), AFormat);
end;

function TRLRTTIContext.FromText(const AInstance: TObject;
  const AText: string; const AFormat: TRLRTTISerializationFormat): TObject;
begin
  case AFormat of
    sfXML, sfReadableXML: Result := FromXML(AInstance, AText);
    sfJSon: Result := FromJSon(AInstance, AText);
  else
    Result := nil;
  end;
end;

function TRLRTTIContext.FromXML(const AInstance: TObject;
  const AText: string): TObject;
var
  lXML: TXMLDocument;
  lXMLIntf: IXMLDocument;
  lClass: TClass;
begin
  Result := AInstance;
  if AText <> '' then
  begin
    lXML := TXMLDocument.Create(nil);
    lXMLIntf := lXML;
    try
      lXML.DOMVendor := GetDOMVendor(SOXMLDOMVendor);
      lXML.XML.Text := AText;
      lXML.Active := True;
      if Assigned(lXML.DocumentElement) then
      begin
        if not Assigned(Result) then
        begin
          lClass := Classes.GetClass(
            VarToStr(lXML.DocumentElement.Attributes[CType]));
          if Assigned(lClass) and lClass.InheritsFrom(TRLBaseType) then
            Result := TRLBaseTypeClass(lClass).Create;
        end;

        ObjectFromXML(Result, lXML.DocumentElement);
      end;
    finally
      lXMLIntf := nil;
    end;
  end;
end;

function TRLRTTIContext.FromJSon(const AInstance: TObject;
  const AText: string): TObject;
begin
  Result := AInstance;
end;

procedure TRLRTTIContext.ObjectToXML(const AInstance: TObject;
   const ANode: IXMLNode);
var
  lSerializer: IRLSerializer;
begin
  if Assigned(AInstance) and Assigned(ANode) then
  begin
    if AInstance.GetInterface(IRLSerializer, lSerializer) then
      lSerializer.ToXML(ANode)
    else
      AttributesToXML(AInstance, ANode);
  end;
end;

procedure TRLRTTIContext.ObjectFromXML(const AInstance: TObject;
   const ANode: IXMLNode);
var
  lSerializer: IRLSerializer;
begin
  if Assigned(AInstance) and Assigned(ANode) then
  begin
    if AInstance.GetInterface(IRLSerializer, lSerializer) then
      lSerializer.FromXML(ANode)
    else
      AttributesFromXML(AInstance, ANode);
  end;
end;

procedure TRLRTTIContext.AttributesToXML(const AInstance: TObject;
  const ANode: IXMLNode);
var
  lAttributes: TRLRTTIAttributes;
  lSerializer: IRLAttributeSerializer;
  lNode: IXMLNode;
  sValue: string;
  I: Integer;
begin
  if Assigned(AInstance) and Assigned(ANode) then
  begin
    AInstance.GetInterface(IRLAttributeSerializer, lSerializer);
    lAttributes := Inspector.GetObjectAttributes(AInstance);
    for I := 0 to Pred(lAttributes.Count) do
    begin
      if not Assigned(lSerializer) or
        lSerializer.IsSerializableAttribute(lAttributes[I].Name) then
      begin
        if lAttributes[I].ValueType <> avtObject then
        begin
          lNode := ANode.AddChild(lAttributes[I].Name);
          if not Assigned(lSerializer) or
            not lSerializer.ReadAttributeValue(lAttributes[I].Name, sValue) then
            lNode.Text := VarToStr(
              Inspector.ReadValue(AInstance, lAttributes[I], True))
          else
            lNode.Text := sValue;
        end
        else
          ObjectToXML(Inspector.ReadObject(AInstance, lAttributes[I]),
            ANode.AddChild(lAttributes[I].Name));
      end;
    end;
  end;
end;

procedure TRLRTTIContext.AttributesFromXML(const AInstance: TObject;
  const ANode: IXMLNode);
var
  lSerializer: IRLAttributeSerializer;
  lAttributes: TRLRTTIAttributes;
  lAttribute: TRLRTTIAttribute;
  sValue: string;
  I: Integer;
begin
  if Assigned(AInstance) and Assigned(ANode) then
  begin
    AInstance.GetInterface(IRLAttributeSerializer, lSerializer);
    lAttributes := Inspector.GetObjectAttributes(AInstance);
    for I := 0 to Pred(ANode.ChildNodes.Count) do
    begin
      lAttribute := lAttributes.Find(ANode.ChildNodes[I].NodeName);
      if Assigned(lAttribute) and
        (not Assigned(lSerializer) or
          lSerializer.IsSerializableAttribute(lAttribute.Name)) then
      begin
        if lAttribute.ValueType <> avtObject then
        begin
          sValue := ANode.ChildNodes[I].Text;
          if not Assigned(lSerializer) or
            not lSerializer.WriteAttributeValue(lAttribute.Name, sValue) then
            try
              Inspector.WriteValue(AInstance, lAttribute, sValue, True);
            except
              on E: Exception do
                raise ERLRTTICouldNotWriteAttribute.Create(
                  Format('Could not write attribute "%s".', [lAttribute.Name]) +
                    sLineBreak + E.Message);
            end;
        end
        else
          ObjectFromXML(Inspector.ReadObject(AInstance, lAttribute),
            ANode.ChildNodes[I]);
      end;
    end;
  end;
end;

procedure TRLRTTIContext.Synchronize(const ALeft, ARight: TObject);
var
  lSynchronizer: IRLSynchronizer;
begin
  if Assigned(ALeft) and Assigned(ARight) then
  begin
    if ALeft.GetInterface(IRLSynchronizer, lSynchronizer) then
      lSynchronizer.Synchronize(ARight)
    else
      SynchronizeAttributes(ALeft, ARight);
  end;
end;

procedure TRLRTTIContext.SynchronizeAttributes(const ALeft, ARight: TObject);
var
  lAttributes: TRLRTTIAttributes;
  I: Integer;
begin
  if Assigned(ALeft) and Assigned(ARight) then
  begin
    lAttributes := Inspector.GetObjectAttributes(ARight);
    for I := 0 to Pred(lAttributes.Count) do
      if lAttributes[I].ValueType <> avtObject then
        Inspector.WriteValue(ALeft, lAttributes[I],
          Inspector.ReadValue(ARight, lAttributes[I], False), False)
      else
        Synchronize(Inspector.ReadObject(ALeft, lAttributes[I]),
          Inspector.ReadObject(ARight, lAttributes[I]));
  end;
end;

class function TRLRTTIContext.Instance: TRLRTTIContext;
begin
  if not Assigned(RLInstance) then
    RLInstance := Self.Create;
  Result := RLInstance;
end;

class procedure TRLRTTIContext.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLRTTIContext.DeleteInstance;

end.
