//=============================================================================
// U_MVCAbstract
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MVCAbstract;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_Actions,
  U_Bindings;

type
  // Exceptions
  ERLMVCCouldNotGetModelInterface = class(ERLException);
  ERLMVCCouldNotGetViewInterface = class(ERLException);
  ERLMVCCouldNotGetControllerInterface = class(ERLException);

  // IRLMVCModel
  IRLMVCModel = interface
    ['{32B3E9D2-0D8B-489B-BEA2-334161120B04}']
    function GetController: TObject;
    procedure SetController(const AValue: TObject);
    procedure Initialize;
  end;

  // IRLMVCView
  IRLMVCView = interface
    ['{BD8813E3-528E-4F17-9662-22FA54E5508E}']
    function GetController: TObject;
    procedure SetController(const AValue: TObject);
    function GetFreeController: Boolean;
    function GetTitle: string;
    procedure SetTitle(const AValue: string);
    function GetHandle: THandle;
    procedure ThreadStarted;
    procedure ThreadCompleted(const AException: Exception);
    procedure Initialize;
    procedure Display;
    function CanDisplayModal: Boolean;
    function DisplayModal: Boolean;
  end;

  // IRLMVCController
  IRLMVCController = interface
    ['{6EDDC913-036D-4EE9-9D03-2D975BA7520E}']
    function GetTitle: string;
    procedure SetTitle(const AValue: string);
    function GetActions: TRLActions;
    function GetBindings: TRLBindings;
    function GetModel: TComponent;
    procedure SetModel(const AValue: TComponent);
    function GetView: TComponent;
    procedure SetView(const AValue: TComponent);
    procedure Initialize;
    procedure Display;
    function CanDisplayModal: Boolean;
    function DisplayModal: Boolean;
    function Post: Boolean;
  end;

implementation

end.
