//=============================================================================
// U_NamedCollection
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_NamedCollection;

interface

uses
  Classes,
  U_Exception,
  U_StringList,
  U_Collection,
  U_KeyedCollection;

type
  // Exceptions
  ERLNameIsEmpty = class(ERLException);
  ERLNameIsInvalid = class(ERLException);
  ERLNameAlreadyExists = class(ERLException);

  // TRLNamedCollectionItem
  TRLNamedCollectionItem = class(TRLKeyedCollectionItem)
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
    procedure KeyAlreadyExists(const AValue: string); override;
    procedure CheckName; virtual;
  public
    procedure Validate; override;
  published
    property Name: string read GetKey write SetKey;
  end;

  // TRLNamedCollectionItemArray
  TRLNamedCollectionItemArray = class(TRLKeyedCollectionItemArray)
  private
    function GetItem(const AIndex: Integer): TRLNamedCollectionItem;
    procedure SetItem(const AIndex: Integer; const AValue: TRLNamedCollectionItem);
  public
    property Items[const AIndex: Integer]: TRLNamedCollectionItem read GetItem write SetItem; default;
  end;

  // TRLNamedCollection
  TRLNamedCollection = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLNamedCollectionItem;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Find(const AName: string): TRLNamedCollectionItem;
    function Contains(const AName: string): Boolean; reintroduce; virtual;
    function Remove(const AName: string): Boolean; reintroduce; virtual;
    function ToArray: TRLNamedCollectionItemArray; overload;
    function ToArray(const ANames: TRLStringList): TRLNamedCollectionItemArray; overload;
    property Items[const AIndex: Integer]: TRLNamedCollectionItem read GetItem; default;
  end;

implementation

{ TRLNamedCollectionItem }

function TRLNamedCollectionItem.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TRLNamedCollectionItem.SetKey(const AValue: string);
begin
  inherited;
end;

procedure TRLNamedCollectionItem.KeyAlreadyExists(const AValue: string);
begin
  raise ERLNameAlreadyExists.CreateFmt('Name "%s" already exists.', [AValue]);
end;

procedure TRLNamedCollectionItem.CheckName;
begin
  if Name = '' then
    raise ERLNameIsEmpty.Create('Name is empty.');
end;

procedure TRLNamedCollectionItem.Validate;
begin
  inherited;
  CheckName;
end;

{ TRLNamedCollectionItemArray }

function TRLNamedCollectionItemArray.GetItem(
  const AIndex: Integer): TRLNamedCollectionItem;
begin
  Result := TRLNamedCollectionItem(inherited Items[AIndex]);
end;

procedure TRLNamedCollectionItemArray.SetItem(const AIndex: Integer;
  const AValue: TRLNamedCollectionItem);
begin
  inherited Items[AIndex] := AValue;
end;

{ TRLNamedCollection }

constructor TRLNamedCollection.CreateParented(const AParent: TObject);
begin
  inherited;
  CaseSensitive := False;
end;

function TRLNamedCollection.GetItem(
  const AIndex: Integer): TRLNamedCollectionItem;
begin
  Result := TRLNamedCollectionItem(inherited Items[AIndex]);
end;

function TRLNamedCollection.GetItemClass: TCollectionItemClass;
begin
  Result := TRLNamedCollectionItem;
end;

function TRLNamedCollection.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLNamedCollectionItemArray;
end;

function TRLNamedCollection.Find(const AName: string): TRLNamedCollectionItem;
begin
  Result := TRLNamedCollectionItem(inherited Find(AName));
end;

function TRLNamedCollection.Contains(const AName: string): Boolean;
begin
  Result := inherited Contains(AName);
end;

function TRLNamedCollection.Remove(const AName: string): Boolean;
begin
  Result := inherited Remove(AName);
end;

function TRLNamedCollection.ToArray: TRLNamedCollectionItemArray;
begin
  Result := TRLNamedCollectionItemArray(inherited ToArray);
end;

function TRLNamedCollection.ToArray(
  const ANames: TRLStringList): TRLNamedCollectionItemArray;
begin
  Result := TRLNamedCollectionItemArray(inherited ToArray(ANames));
end;

end.
