//=============================================================================
// U_VCLDialog
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLDialog;

interface

uses
  Windows,
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  ExtCtrls,
  U_StringList,
  U_Dialog,
  U_FormType,
  U_VCLHacks;

const
  CHorizontalMargin = 10;
  CVerticalMargin = 10;
  CHorizontalSpacing = 10;
  CVerticalSpacing = 10;
  CIconHeight = 32;
  CIconWidth = 32;
  CButtonHeight = 23;
  CButtonWidth = 60;
  CButtonSpacing = 5;
  CButtonCaptionSpacing = 10;
  CDetailHeight = 100;
  CDetailMargin = 25;

type
  // TRLVCLDialogForm
  TRLVCLDialogForm = class(TRLFormType)
  private
    FIcon: TImage;
    FMessage: TControl;
    FDetailMessage: TControl;
    FDetailButton: TButton;
    procedure DetailButtonClick(Sender: TObject);
    function GetText: string;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoShow; override;
  public
    property Icon: TImage read FIcon write FIcon;
    property Message: TControl read FMessage write FMessage;
    property DetailMessage: TControl read FDetailMessage write FDetailMessage;
    property DetailButton: TButton read FDetailButton write FDetailButton;
  end;

  // TRLVCLDialog
  TRLVCLDialog = class(TRLDialog)
  protected
    function CreateIcon: TImage; virtual;
    function CreateMessage: TControl; virtual;
    function CreateDetailMessage: TControl; virtual;
    function CreateButton: TButton; virtual;
  public
    function Show(const AMessage: string; const AKind: TRLDialogKind; const AButtons: TRLDialogButtons; const ADefault: TRLDialogButton; const ADetails: string): TRLDialogButton; override;
    class function Instance: TRLVCLDialog;
  end;

implementation

uses
  Math,
  Clipbrd,
  U_System;

{ TRLVCLDialogForm }

procedure TRLVCLDialogForm.DetailButtonClick(Sender: TObject);
var
  nHeight: Integer;
  nFullHeight: Integer;
begin
  nHeight := FDetailMessage.Top;
  nFullHeight := FDetailMessage.Top + FDetailMessage.Height + FMessage.Top;
  if ClientHeight = nHeight then
  begin
    FDetailButton.Caption := SRLCloseDetails;
    ClientHeight := nFullHeight;
  end
  else
  begin
    ClientHeight := nHeight;
    FDetailButton.Caption := SRLOpenDetails;
  end;
end;

function TRLVCLDialogForm.GetText: string;
var
  lMessage: TRLStringList;
  sDivider: string;
  sCaptions: string;
  I: integer;
begin
  sDivider := StringOfChar('-', 30);
  for I := 0 to Pred(ControlCount) do
    if Controls[I] is TButton then
      sCaptions := sCaptions + TButton(Controls[I]).Caption +
        StringOfChar(' ', 3);
  lMessage := TRLStringList.Create;
  try
    lMessage.Add(sDivider);
    lMessage.Add(Caption);
    lMessage.Add(sDivider);
    lMessage.Add(TRLControl.GetCaption(FMessage));
    lMessage.Add(sDivider);
    lMessage.Add(StringReplace(sCaptions, '&', '', [rfReplaceAll]));
    lMessage.Add(sDivider);
    if Assigned(FDetailMessage) then
    begin
      lMessage.Add(TRLControl.GetText(FDetailMessage));
      lMessage.Add(sDivider);
    end;

    Result := lMessage.Text;
  finally
    FreeAndNil(lMessage);
  end;
end;

procedure TRLVCLDialogForm.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Shift = [ssCtrl]) and U_System.CharInSet(Chr(Key), ['c', 'C']) then
    Clipboard.AsText := GetText;
end;

procedure TRLVCLDialogForm.DoShow;
begin
  inherited;
  if Assigned(FDetailButton) then
    FDetailButton.OnClick := DetailButtonClick;
end;

{ TRLVCLDialog }

function TRLVCLDialog.CreateIcon: TImage;
begin
  Result := TImage.Create(nil);
end;

function TRLVCLDialog.CreateMessage: TControl;
var
  lMessage: TLabel;
begin
  lMessage := TLabel.Create(nil);
  lMessage.WordWrap := True;
  lMessage.Transparent := True;
  Result := lMessage;
end;

function TRLVCLDialog.CreateDetailMessage: TControl;
var
  lDetailMessage: TMemo;
begin
  lDetailMessage := TMemo.Create(nil);
  lDetailMessage.ScrollBars := ssVertical;
  lDetailMessage.WordWrap := True;
  lDetailMessage.ReadOnly := True;
  Result := lDetailMessage;
end;

function TRLVCLDialog.CreateButton: TButton;
begin
  Result := TButton.Create(nil);
end;

function TRLVCLDialog.Show(const AMessage: string; const AKind: TRLDialogKind;
  const AButtons: TRLDialogButtons; const ADefault: TRLDialogButton;
  const ADetails: string): TRLDialogButton;
var
  lForm: TRLVCLDialogForm;
  lTextRect: TRect;
  lButtons: TRLDialogButtons;
  nButtonCount: Integer;
  nCurrentButtonWidth: Integer;
  nButtonTop: Integer;
  nButtonLeft: Integer;
  nButtonWidth: Integer;
  nButtonGroupWidth: Integer;
  nIconTextHeight: Integer;
  lCancelButton: TRLDialogButton;
  lButton: TButton;
  B: TRLDialogButton;
begin
  lForm := TRLVCLDialogForm.CreateNew(nil);
  try
    nButtonWidth := CButtonWidth;
    nButtonCount := 0;
    lButtons := AButtons;
    if (ADetails <> '') and not (dbDetails in lButtons) then
      lButtons := lButtons + [dbDetails];
    for B := Low(TRLDialogButton) to High(TRLDialogButton) do
      if B in lButtons then
      begin
        Inc(nButtonCount);
        SetRect(lTextRect, 0, 0, 0, 0);
        DrawText(lForm.Canvas.Handle, PChar(RLDialogButtonCaption[B]), -1,
          lTextRect,
            DT_CALCRECT or DT_LEFT or DT_SINGLELINE or
              lForm.DrawTextBiDiModeFlagsReadingOnly);
        nCurrentButtonWidth := lTextRect.Right - lTextRect.Left +
          CButtonCaptionSpacing;
        if nCurrentButtonWidth > nButtonWidth then
          nButtonWidth := nCurrentButtonWidth;
      end;

    nButtonGroupWidth := (nButtonWidth * nButtonCount) +
      (CButtonSpacing * Pred(nButtonCount));
    SetRect(lTextRect, 0, 0, Screen.Width div 2, 0);
    DrawText(lForm.Canvas.Handle, PChar(AMessage), Succ(Length(AMessage)),
      lTextRect,
        DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK or
          lForm.DrawTextBiDiModeFlagsReadingOnly);
    nIconTextHeight := IfThen(lTextRect.Bottom > CIconHeight, lTextRect.Bottom,
      CIconHeight);
    lForm.BorderStyle := bsDialog;
    lForm.KeyPreview := True;
    lForm.Caption := RLDialogKindCaption[AKind];
    lForm.Position := poScreenCenter;
    lForm.ClientHeight := nIconTextHeight + CVerticalSpacing + CButtonHeight +
      (CVerticalMargin * 2);
    lForm.ClientWidth :=
      Max(CIconWidth + CHorizontalSpacing + lTextRect.Right,
        nButtonGroupWidth) +
      (CHorizontalMargin * 2);
    lForm.Icon := CreateIcon;
    lForm.Icon.Parent := lForm;
    lForm.Icon.Picture.Icon.Handle := LoadIcon(MainInstance,
      PChar(RLDialogKindIcon[AKind]));
    lForm.Icon.Top := CVerticalMargin;
    lForm.Icon.Left := CHorizontalMargin;
    lForm.Icon.Height := CIconHeight;
    lForm.Icon.Width := CIconWidth;
    lForm.Message := CreateMessage;
    lForm.Message.Parent := lForm;
    lForm.Message.Top := CVerticalMargin;
    lForm.Message.Left := CHorizontalMargin + CIconWidth + CHorizontalSpacing;
    lForm.Message.Height := lTextRect.Bottom;
    lForm.Message.Width := lTextRect.Right;
    TRLControl.SetCaption(lForm.Message, AMessage);
    if dbCancel in lButtons then
      lCancelButton := dbCancel
    else
      if dbNo in lButtons then
        lCancelButton := dbNo
      else
        lCancelButton := dbOk;
    nButtonTop := lForm.Message.Top + nIconTextHeight + CVerticalSpacing;
    nButtonLeft := lForm.ClientWidth - nButtonGroupWidth - CHorizontalMargin;
    for B := Low(TRLDialogButton) to High(TRLDialogButton) do
      if B in lButtons then
      begin
        lButton := CreateButton;
        lButton.Parent := lForm;
        lButton.Caption := RLDialogButtonCaption[B];
        lButton.ModalResult := DialogButtonToModalResult(B);
        lButton.Top := nButtonTop;
        lButton.Left := nButtonLeft;
        lButton.Height := CButtonHeight;
        lButton.Width := nButtonWidth;
        if B = ADefault then
        begin
          lButton.Default := True;
          lForm.ActiveControl := lButton;
        end;

        if B = lCancelButton then
        begin
          lButton.Cancel := True;
          lForm.AllowEscape := True;
        end;

        if B = dbDetails then
          lForm.DetailButton := lButton;
        nButtonLeft := nButtonLeft + CButtonSpacing + nButtonWidth;
      end;

    if dbDetails in lButtons then
    begin
      lForm.DetailMessage := CreateDetailMessage;
      lForm.DetailMessage.Parent := lForm;
      lForm.DetailMessage.Top := nButtonTop + CButtonHeight + CVerticalMargin;
      lForm.DetailMessage.Left := CHorizontalMargin;
      lForm.DetailMessage.Height := CDetailHeight;
      lForm.DetailMessage.Width := lForm.ClientWidth - (CHorizontalMargin * 2);
      TRLControl.SetText(lForm.DetailMessage, ADetails);
    end;

    Result := ModalResultToDialogButton(lForm.ShowModal);
  finally
    FreeAndNil(lForm);
  end;
end;

class function TRLVCLDialog.Instance: TRLVCLDialog;
begin
  Result := TRLVCLDialog(inherited Instance);
end;

initialization
  TRLDialog.SetInstanceType(TRLVCLDialog);

finalization
  if TRLDialog.GetInstanceType = TRLVCLDialog then
    TRLDialog.DeleteInstance;

end.
