//=============================================================================
// U_DXDialog
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DXDialog;

interface

uses
  Controls,
  StdCtrls,
  cxButtons,
  cxEdit,
  cxLabel,
  cxMemo,
  U_Dialog,
  U_VCLDialog;

type
  // TRLDXDialog
  TRLDXDialog = class(TRLVCLDialog)
  protected
    function CreateMessage: TControl; override;
    function CreateDetailMessage: TControl; override;
    function CreateButton: TButton; override;
    class function Instance: TRLDXDialog;
  end;

implementation

{ TRLDXDialog }

function TRLDXDialog.CreateMessage: TControl;
var
  lMessage: TcxLabel;
begin
  lMessage := TcxLabel.Create(nil);
  lMessage.Properties.WordWrap := True;
  lMessage.Style.TransparentBorder := False;
  lMessage.Transparent := True;
  Result := lMessage;
end;

function TRLDXDialog.CreateDetailMessage: TControl;
var
  lDetailMessage: TcxMemo;
begin
  lDetailMessage := TcxMemo.Create(nil);
  lDetailMessage.Style.BorderStyle := ebs3D;
  lDetailMessage.Properties.ScrollBars := ssVertical;
  lDetailMessage.Properties.WordWrap := True;
  lDetailMessage.Properties.ReadOnly := True;
  Result := lDetailMessage;
end;

function TRLDXDialog.CreateButton: TButton;
begin
  Result := inherited CreateButton; //TcxButton.Create(nil);
end;

class function TRLDXDialog.Instance: TRLDXDialog;
begin
  Result := TRLDXDialog(inherited Instance);
end;

initialization
  TRLDialog.SetInstanceType(TRLDXDialog);

finalization
  if TRLDialog.GetInstanceType = TRLDXDialog then
    TRLDialog.DeleteInstance;

end.
