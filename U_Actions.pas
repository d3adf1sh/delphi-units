//=============================================================================
// U_Actions
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Actions;

interface

uses
  SysUtils,
  Classes,
  ImgList,
  ActnList,
  U_Exception,
  U_NamedCollection;

type
  // Exceptions
  ERLActionNotFound = class(ERLException);

  // TRLAction
  TRLAction = class(TRLNamedCollectionItem)
  private
    FAction: TAction;
    function GetCaption: string;
    procedure SetCaption(const AValue: string);
    function GetHint: string;
    procedure SetHint(const AValue: string);
    function GetShortCut: TShortCut;
    procedure SetShortCut(const AValue: TShortCut);
    function GetEnabled: Boolean;
    procedure SetEnabled(const AValue: Boolean);
    function GetImageIndex: Integer;
    procedure SetImageIndex(const AValue: Integer);
    function GetGroupIndex: Integer;
    procedure SetGroupIndex(const AValue: Integer);
    function GetAutoCheck: Boolean;
    procedure SetAutoCheck(const AValue: Boolean);
    function GetChecked: Boolean;
    procedure SetChecked(const AValue: Boolean);
    function GetVisible: Boolean;
    procedure SetVisible(const AValue: Boolean);
    function GetOnExecute: TNotifyEvent;
    procedure SetOnExecute(const AValue: TNotifyEvent);
  protected
    procedure KeyChanged; override;
    function CreateAction: TAction; virtual;
    property Action: TAction read FAction;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Execute; virtual;
    function GetAction: TAction; virtual;
    property Caption: string read GetCaption write SetCaption;
    property Hint: string read GetHint write SetHint;
    property ShortCut: TShortCut read GetShortCut write SetShortCut;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property ImageIndex: Integer read GetImageIndex write SetImageIndex;
    property GroupIndex: Integer read GetGroupIndex write SetGroupIndex;
    property AutoCheck: Boolean read GetAutoCheck write SetAutoCheck;
    property Checked: Boolean read GetChecked write SetChecked;
    property Visible: Boolean read GetVisible write SetVisible;
    property OnExecute: TNotifyEvent read GetOnExecute write SetOnExecute;
  end;

  // TRLActions
  TRLActions = class(TRLNamedCollection)
  private
    FActions: TActionList;
    function GetItem(const AIndex: Integer): TRLAction;
    function GetImages: TCustomImageList;
    procedure SetImages(const AValue: TCustomImageList);
    function GetChecked(const AName: string): Boolean;
    procedure SetChecked(const AName: string; const AValue: Boolean);
    function GetEnabled(const AName: string): Boolean;
    procedure SetEnabled(const AName: string; const AValue: Boolean);
    function GetVisible(const AName: string): Boolean;
    procedure SetVisible(const AName: string; const AValue: Boolean);
  protected
    function GetItemClass: TCollectionItemClass; override;
    function CreateActions: TActionList; virtual;
    property Actions: TActionList read FActions;
  public
    constructor CreateParented(const AParent: TObject); override;
    destructor Destroy; override;
    function Add: TRLAction;
    function Insert(const AIndex: Integer): TRLAction;
    function Find(const AName: string): TRLAction;
    function FindByName(const AName: string): TRLAction;
    procedure Execute(const AName: string); virtual;
    function GetAction(const AName: string): TAction; virtual;
    property Items[const AIndex: Integer]: TRLAction read GetItem; default;
    property Images: TCustomImageList read GetImages write SetImages;
    property Enabled[const AName: string]: Boolean read GetEnabled write SetEnabled;
    property Checked[const AName: string]: Boolean read GetChecked write SetChecked;
    property Visible[const AName: string]: Boolean read GetVisible write SetVisible;
  end;

implementation

{ TRLAction }

constructor TRLAction.Create(Collection: TCollection);
begin
  inherited;
  FAction := CreateAction;
  if Collection is TRLActions then
    FAction.ActionList := TRLActions(Collection).FActions;
end;

destructor TRLAction.Destroy;
begin
  FreeAndNil(FAction);
  inherited;
end;

function TRLAction.GetCaption: string;
begin
  Result := FAction.Caption;
end;

procedure TRLAction.SetCaption(const AValue: string);
begin
  FAction.Caption := AValue;
end;

function TRLAction.GetHint: string;
begin
  Result := FAction.Hint;
end;

procedure TRLAction.SetHint(const AValue: string);
begin
  FAction.Hint := AValue;
end;

function TRLAction.GetShortCut: TShortCut;
begin
  Result := FAction.ShortCut;
end;

procedure TRLAction.SetShortCut(const AValue: TShortCut);
begin
  FAction.ShortCut := AValue;
end;

function TRLAction.GetEnabled: Boolean;
begin
  Result := FAction.Enabled;
end;

procedure TRLAction.SetEnabled(const AValue: Boolean);
begin
  FAction.Enabled := AValue;
end;

function TRLAction.GetImageIndex: Integer;
begin
  Result := FAction.ImageIndex;
end;

procedure TRLAction.SetImageIndex(const AValue: Integer);
begin
  FAction.ImageIndex := AValue;
end;

function TRLAction.GetGroupIndex: Integer;
begin
  Result := FAction.GroupIndex;
end;

procedure TRLAction.SetGroupIndex(const AValue: Integer);
begin
  FAction.GroupIndex := AValue;
end;

function TRLAction.GetAutoCheck: Boolean;
begin
  Result := FAction.AutoCheck;
end;

procedure TRLAction.SetAutoCheck(const AValue: Boolean);
begin
  FAction.AutoCheck := AValue;
end;

function TRLAction.GetChecked: Boolean;
begin
  Result := FAction.Checked;
end;

procedure TRLAction.SetChecked(const AValue: Boolean);
begin
  FAction.Checked := AValue;
end;

function TRLAction.GetVisible: Boolean;
begin
  Result := FAction.Visible;
end;

procedure TRLAction.SetVisible(const AValue: Boolean);
begin
  FAction.Visible := AValue;
end;

function TRLAction.GetOnExecute: TNotifyEvent;
begin
  Result := FAction.OnExecute;
end;

procedure TRLAction.SetOnExecute(const AValue: TNotifyEvent);
begin
  FAction.OnExecute := AValue;
end;

procedure TRLAction.KeyChanged;
begin
  inherited;
  FAction.Name := 'act' + Name;
end;

function TRLAction.CreateAction: TAction;
begin
  Result := TAction.Create(nil);
end;

procedure TRLAction.Execute;
begin
  FAction.Execute;
end;

function TRLAction.GetAction: TAction;
begin
  Result := FAction;
end;

{ TRLActions }

constructor TRLActions.CreateParented(const AParent: TObject);
begin
  inherited;
  FActions := CreateActions;
end;

destructor TRLActions.Destroy;
begin
  Clear; //remover a��es primeiro
  FreeAndNil(FActions);
  inherited;
end;

function TRLActions.GetItem(const AIndex: Integer): TRLAction;
begin
  Result := TRLAction(inherited Items[AIndex]);
end;

function TRLActions.GetImages: TCustomImageList;
begin
  Result := FActions.Images;
end;

procedure TRLActions.SetImages(const AValue: TCustomImageList);
begin
  FActions.Images := AValue;
end;

function TRLActions.GetChecked(const AName: string): Boolean;
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  Result := Assigned(lAction) and lAction.Checked;
end;

procedure TRLActions.SetChecked(const AName: string; const AValue: Boolean);
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  if Assigned(lAction) then
    lAction.Checked := AValue;
end;

function TRLActions.GetEnabled(const AName: string): Boolean;
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  Result := Assigned(lAction) and lAction.Enabled;
end;

procedure TRLActions.SetEnabled(const AName: string; const AValue: Boolean);
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  if Assigned(lAction) then
    lAction.Enabled := AValue;
end;

function TRLActions.GetVisible(const AName: string): Boolean;
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  Result := Assigned(lAction) and lAction.Visible;
end;

procedure TRLActions.SetVisible(const AName: string; const AValue: Boolean);
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  if Assigned(lAction) then
    lAction.Visible := AValue;
end;

function TRLActions.GetItemClass: TCollectionItemClass;
begin
  Result := TRLAction;
end;

function TRLActions.CreateActions: TActionList;
begin
  Result := TActionList.Create(nil);
end;

function TRLActions.Add: TRLAction;
begin
  Result := TRLAction(inherited Add);
end;

function TRLActions.Insert(const AIndex: Integer): TRLAction;
begin
  Result := TRLAction(inherited Insert(AIndex));
end;

function TRLActions.Find(const AName: string): TRLAction;
begin
  Result := TRLAction(inherited Find(AName));
end;

function TRLActions.FindByName(const AName: string): TRLAction;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLActionNotFound.CreateFmt('Action "%s" not found.', [AName]);
end;

procedure TRLActions.Execute(const AName: string);
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  if Assigned(lAction) then
    lAction.Execute;
end;

function TRLActions.GetAction(const AName: string): TAction;
var
  lAction: TRLAction;
begin
  lAction := Find(AName);
  if Assigned(lAction) then
    Result := lAction.GetAction
  else
    Result := nil;
end;

end.
