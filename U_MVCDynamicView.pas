//=============================================================================
// U_MVCDynamicView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MVCDynamicView;

interface

uses
  U_MVCView;

type
  // References
  TRLMVCDynamicViewClass = class of TRLMVCDynamicView;

  // TRLMVCDynamicView
  TRLMVCDynamicView = class(TRLMVCView)
  protected
    procedure CreateControls; virtual; abstract;
  public
    procedure AfterConstruction; override;
  end;

implementation

{ TRLMVCDynamicView }

procedure TRLMVCDynamicView.AfterConstruction;
begin
  inherited;
  CreateControls;
end;

end.
