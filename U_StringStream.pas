//=============================================================================
// U_StringStream
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_StringStream;

interface

uses
  Classes,
  XMLIntf,
  U_Serializer;

type
  // References
  TRLStringStreamClass = class of TRLStringStream;

{$M+}
  // TRLStringStream
  TRLStringStream = class(TStringStream, IInterface, IRLSerializer)
  private
    FDestroyed: Boolean;
  protected
    property Destroyed: Boolean read FDestroyed;
  public
    destructor Destroy; override;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    procedure ToXML(const ANode: IXMLNode); virtual;
    procedure FromXML(const ANode: IXMLNode); virtual;
  end;
{$M-}

implementation

{ TRLStringStream }

destructor TRLStringStream.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

function TRLStringStream.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TRLStringStream._AddRef: Integer;
begin
  Result := -1;
end;

function TRLStringStream._Release: Integer;
begin
  Result := -1;
end;

procedure TRLStringStream.ToXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
    ANode.Text := DataString;
end;

procedure TRLStringStream.FromXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
  begin
    Size := 0;
    WriteString(ANode.Text);
  end;
end;

end.
