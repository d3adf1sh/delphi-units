//=============================================================================
// U_Collection
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Collection;

interface

uses
  Classes,
  XMLIntf,
  U_ArrayType,
  U_Exception,
  U_InterfacedObject,
  U_Validator,
  U_Enumerable,
  U_Enumerator,
  U_Serializer;

const
  FType = 'type';

type
  // References
  TRLCollectionItemClass = class of TRLCollectionItem;
  TRLCollectionItemArrayClass = class of TRLCollectionItemArray;
  TRLCollectionClass = class of TRLCollection;

  // Exceptions
  ERLCollectionIsNull = class(ERLException);

  // TRLCollectionItem
  TRLCollectionItem = class(TCollectionItem, IRLValidator)
  private
    FUpdateCount: Integer;
    FDestroyed: Boolean;
  protected
    procedure CheckCollection; virtual;
    property UpdateCount: Integer read FUpdateCount;
    property Destroyed: Boolean read FDestroyed;
  public
    constructor CreateItem; virtual;
    destructor Destroy; override;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    procedure BeginUpdate; virtual;
    procedure EndUpdate; virtual;
    function InUpdate: Boolean; virtual;
    procedure Validate; virtual;
  end;

  // TRLCollectionItemArray
  TRLCollectionItemArray = class(TRLArrayType)
  private
    function GetItem(const AIndex: Integer): TRLCollectionItem;
    procedure SetItem(const AIndex: Integer; const AValue: TRLCollectionItem);
  public
    property Items[const AIndex: Integer]: TRLCollectionItem read GetItem write SetItem; default;
  end;

  // TRLCollection
  TRLCollection = class(TCollection, IRLValidator, IRLEnumerable, IRLSerializer)
  private
    FParent: TObject;
    FDestroyed: Boolean;
    function GetItem(const AIndex: Integer): TRLCollectionItem;
  protected
    function GetItemClass: TCollectionItemClass; virtual;
    function GetItemArrayClass: TRLCollectionItemArrayClass; virtual;
    function GetItemIdentifier: string; virtual;
    function GetItemsIdentifier: string; virtual;
    procedure CheckItems; virtual;
    property Destroyed: Boolean read FDestroyed;
  public
    constructor Create; reintroduce; virtual;
    constructor CreateParented(const AParent: TObject); virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function AddType(const AType: TCollectionItemClass): TCollectionItem;
    function InsertType(const AType: TCollectionItemClass; const AIndex: Integer): TCollectionItem;
    procedure Validate; virtual;
    function GetEnumerator: IRLEnumerator; virtual;
    procedure ToXML(const ANode: IXMLNode); virtual;
    procedure FromXML(const ANode: IXMLNode); virtual;
    function ToArray: TRLCollectionItemArray;
    property Items[const AIndex: Integer]: TRLCollectionItem read GetItem; default;
    property Parent: TObject read FParent write FParent;
  end;

  // TRLCollectionEnumerator
  TRLCollectionEnumerator = class(TRLInterfacedObject, IRLEnumerator)
  private
    FInstance: TRLCollection;
    FItem: TRLCollectionItem;
    FIndex: Integer;
  public
    function GetCurrent: TObject; virtual;
    function GetCurrentItem: TRLCollectionItem; virtual;
    function Next: Boolean; virtual;
    procedure Reset; virtual;
    property Instance: TRLCollection read FInstance write FInstance;
  end;

implementation

uses
  Variants,
  U_RTTIContext;

{ TRLCollectionItem }

constructor TRLCollectionItem.CreateItem;
begin
  inherited Create(nil);
end;

destructor TRLCollectionItem.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

procedure TRLCollectionItem.CheckCollection;
begin
  if not Assigned(Collection) then
    raise ERLCollectionIsNull.Create('Collection is null.');
end;

function TRLCollectionItem.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TRLCollectionItem._AddRef: Integer;
begin
  Result := -1;
end;

function TRLCollectionItem._Release: Integer;
begin
  Result := -1;
end;

procedure TRLCollectionItem.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

procedure TRLCollectionItem.EndUpdate;
begin
  if FUpdateCount > 0 then
    Dec(FUpdateCount);
end;

function TRLCollectionItem.InUpdate: Boolean;
begin
  Result := FUpdateCount > 0;
end;

procedure TRLCollectionItem.Validate;
begin
end;

{ TRLCollectionItemArray }

function TRLCollectionItemArray.GetItem(
  const AIndex: Integer): TRLCollectionItem;
begin
  Result := TRLCollectionItem(inherited Items[AIndex]);
end;

procedure TRLCollectionItemArray.SetItem(const AIndex: Integer;
  const AValue: TRLCollectionItem);
begin
  inherited Items[AIndex] := AValue;
end;

{ TRLCollection }

constructor TRLCollection.Create;
begin
  CreateParented(nil);
end;

constructor TRLCollection.CreateParented(const AParent: TObject);
begin
  inherited Create(GetItemClass);
  FParent := AParent;
end;

destructor TRLCollection.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

function TRLCollection.GetItem(const AIndex: Integer): TRLCollectionItem;
begin
  Result := TRLCollectionItem(inherited Items[AIndex]);
end;

function TRLCollection.GetItemClass: TCollectionItemClass;
begin
  Result := TRLCollectionItem;
end;

function TRLCollection.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLCollectionItemArray;
end;

function TRLCollection.GetItemIdentifier: string;
begin
  Result := 'Item';
end;

function TRLCollection.GetItemsIdentifier: string;
begin
  Result := 'Items';
end;

procedure TRLCollection.CheckItems;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Validate;
end;

procedure TRLCollection.Assign(Source: TPersistent);
var
  lCollection: TRLCollection;
  lItem: TRLCollectionItem;
  I: Integer;
begin
  if Source is TRLCollection then
  begin
    lCollection := TRLCollection(Source);
    BeginUpdate;
    try
      Clear;
      for I := 0 to Pred(lCollection.Count) do
      begin
        lItem := TRLCollectionItem(Add);
        lItem.BeginUpdate;
        try
          lItem.Assign(lCollection.Items[I]);
        finally
          lItem.EndUpdate;
        end;
      end;
    finally
      EndUpdate;
    end;
  end;
end;

function TRLCollection.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TRLCollection._AddRef: Integer;
begin
  Result := -1;
end;

function TRLCollection._Release: Integer;
begin
  Result := -1;
end;

function TRLCollection.AddType(
  const AType: TCollectionItemClass): TCollectionItem;
begin
  Result := InsertType(AType, Count);
end;

function TRLCollection.InsertType(const AType: TCollectionItemClass;
  const AIndex: Integer): TCollectionItem;
var
  ptrItemClass: ^TCollectionItemClass;
  lItemClass: TCollectionItemClass;
begin
  if Assigned(AType) then
  begin
    ptrItemClass := @ItemClass;
    lItemClass := ptrItemClass^;
    try
      ptrItemClass^ := AType;
      Result := Insert(AIndex);
    finally
      ptrItemClass^ := lItemClass;
    end;
  end
  else
    Result := nil;
end;

procedure TRLCollection.Validate;
begin
  CheckItems;
end;

function TRLCollection.GetEnumerator: IRLEnumerator;
var
  lEnumerator: TRLCollectionEnumerator;
begin
  lEnumerator := TRLCollectionEnumerator.Create;
  lEnumerator.Instance := Self;
  Result := lEnumerator;
end;

procedure TRLCollection.ToXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  lNode: IXMLNode;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesToXML(Self, ANode);
    lNodes := ANode.AddChild(GetItemsIdentifier);
    for I := 0 to Pred(Count) do
    begin
      lNode := lNodes.AddChild(GetItemIdentifier);
      if Items[I].ClassType <> ItemClass then
        lNode.Attributes[FType] := Items[I].ClassName;
      TRLRTTIContext.Instance.ObjectToXML(Items[I], lNode);
    end;
  end;
end;

procedure TRLCollection.FromXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  lItem: TCollectionItem;
  lClass: TCollectionItemClass;
  sClassName: string;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesFromXML(Self, ANode);
    lNodes := ANode.ChildNodes.FindNode(GetItemsIdentifier);
    if Assigned(lNodes) then
    begin
      BeginUpdate;
      try
        Clear;
        for I := 0 to Pred(lNodes.ChildNodes.Count) do
        begin
          sClassName := VarToStr(lNodes.ChildNodes[I].Attributes[FType]);
          if sClassName <> '' then
          begin
            lClass := TCollectionItemClass(GetClass(sClassName));
            if Assigned(lClass) then
              lItem := AddType(lClass)
            else
              lItem := Add;
          end
          else
            lItem := Add;
          TRLRTTIContext.Instance.ObjectFromXML(lItem, lNodes.ChildNodes[I]);
        end;
      finally
        EndUpdate;
      end;
    end;
  end;
end;

function TRLCollection.ToArray: TRLCollectionItemArray;
var
  I: Integer;
begin
  Result := GetItemArrayClass.Create;
  for I := 0 to Pred(Count) do
    Result.Add(Items[I]);
end;

{ TRLCollectionEnumerator }

function TRLCollectionEnumerator.GetCurrent: TObject;
begin
  Result := FItem;
end;

function TRLCollectionEnumerator.GetCurrentItem: TRLCollectionItem;
begin
  Result := TRLCollectionItem(GetCurrent);
end;

function TRLCollectionEnumerator.Next: Boolean;
begin
  if Assigned(FInstance) and (FIndex < FInstance.Count) then
  begin
    FItem := FInstance.Items[FIndex];
    Inc(FIndex);
  end
  else
    FItem := nil;
  Result := Assigned(FItem);
end;

procedure TRLCollectionEnumerator.Reset;
begin
  FIndex := 0;
end;

end.
