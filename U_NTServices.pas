 //=============================================================================
// U_NTServices
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_NTServices;

{$I RL.INC}

interface

uses
  Windows,
  WinSvc,
  SysUtils,
  Classes,
  U_Exception,
  U_NamedCollection;

type
  // Exceptions
  ERLServiceNotFound = class(ERLException);

  // TRLNTService
  TRLNTService = class(TRLNamedCollectionItem)
  private
    FHandle: THandle;
    FManagerHandle: THandle;
    function GetFileName: string;
    function GetFileLocation: string;
    function GetDisplayName: string; reintroduce;
  protected
    procedure KeyChanged; override;
{$IFDEF D20UP}
    function GetConfiguration: LPQUERY_SERVICE_CONFIGW; virtual;
{$ELSE}
    function GetConfiguration: PQueryServiceConfig; virtual;
{$ENDIF}
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    function Installed: Boolean; virtual;
    function Start(const AWait: Boolean): Boolean; virtual;
    function Stop(const AWait: Boolean): Boolean; virtual;
    function Started: Boolean; virtual;
    function Stopped: Boolean; virtual;
    property Handle: THandle read FHandle;
    property FileName: string read GetFileName;
    property FileLocation: string read GetFileLocation;
    property DisplayName: string read GetDisplayName;
  end;

  // TRLNTServices
  TRLNTServices = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLNTService;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLNTService;
    function Insert(const AIndex: Integer): TRLNTService;
    function Find(const AName: string): TRLNTService;
    function FindByName(const AName: string): TRLNTService;
    function Installed(const AName: string): Boolean; virtual;
    function Start(const AName: string; const AWait: Boolean): Boolean; virtual;
    function Stop(const AName: string; const AWait: Boolean): Boolean; virtual;
    function Started(const AName: string): Boolean; virtual;
    function Stopped(const AName: string): Boolean; virtual;
    procedure StartAll(const AWait: Boolean); virtual;
    procedure StopAll(const AWait: Boolean); virtual;
    function AllStarted: Boolean; virtual;
    function AllStopped: Boolean; virtual;
    property Items[const AIndex: Integer]: TRLNTService read GetItem; default;
  end;

implementation

uses
  U_NTUtils;

{ TRLNTService }

constructor TRLNTService.Create(Collection: TCollection);
begin
  inherited;
  FManagerHandle := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
end;

destructor TRLNTService.Destroy;
begin
  if FHandle > 0 then
    CloseServiceHandle(FHandle);
  if FManagerHandle > 0 then
    CloseServiceHandle(FManagerHandle);
  inherited;
end;

function TRLNTService.GetFileName: string;
var
{$IFDEF D20UP}
  ptrConfig: LPQUERY_SERVICE_CONFIGW;
{$ELSE}
  ptrConfig: PQueryServiceConfig;
{$ENDIF}
begin
  ptrConfig := GetConfiguration;
  if Assigned(ptrConfig) then
    try
      Result := ptrConfig.lpBinaryPathName;
    finally
      FreeMem(ptrConfig);
    end
  else
    Result := '';
end;

function TRLNTService.GetFileLocation: string;
begin
  Result := TRLNTUtils.GetPathOfFile(FileName);
end;

function TRLNTService.GetDisplayName: string;
var
{$IFDEF D20UP}
  ptrConfig: LPQUERY_SERVICE_CONFIGW;
{$ELSE}
  ptrConfig: PQueryServiceConfig;
{$ENDIF}
begin
  ptrConfig := GetConfiguration;
  if Assigned(ptrConfig) then
    try
      Result := ptrConfig.lpDisplayName;
    finally
      FreeMem(ptrConfig);
    end
  else
    Result := '';
end;

procedure TRLNTService.KeyChanged;
begin
  inherited;
  if FHandle > 0 then
    CloseServiceHandle(FHandle);
  if FManagerHandle > 0 then
    FHandle := OpenService(FManagerHandle, PChar(Name), SERVICE_ALL_ACCESS)
  else
    FHandle := 0;
end;

{$IFDEF D20UP}
function TRLNTService.GetConfiguration: LPQUERY_SERVICE_CONFIGW;
{$ELSE}
function TRLNTService.GetConfiguration: PQueryServiceConfig;
{$ENDIF}
var
  nSize: DWord;
begin
  Result := nil;
  if Installed then
  begin
    nSize := 0;
    QueryServiceConfig(FHandle, nil, 0, nSize);
    if nSize > 0 then
    begin
      GetMem(Result, nSize);
      QueryServiceConfig(FHandle, Result, nSize, nSize);
    end;
  end;
end;

function TRLNTService.Installed: Boolean;
begin
  Result := FHandle > 0;
end;

function TRLNTService.Start(const AWait: Boolean): Boolean;
var
  ptrArgs: PChar;
begin                           
  Result := Installed and not Started and StartService(FHandle, 0, ptrArgs);
  if Result and AWait then
    while not Started do
      ;//wait
end;

function TRLNTService.Stop(const AWait: Boolean): Boolean;
var
  lStatus: TServiceStatus;
begin
  Result := Installed and not Stopped and
    ControlService(FHandle, SERVICE_CONTROL_STOP, lStatus);
  if Result and AWait then        
    while not Stopped do
      ;//wait        
end;

function TRLNTService.Started: Boolean;
var
  lStatus: TServiceStatus;
begin
  Result := Installed and QueryServiceStatus(FHandle, lStatus) and
    (lStatus.dwCurrentState = SERVICE_RUNNING);
end;

function TRLNTService.Stopped: Boolean;
var
  lStatus: TServiceStatus;
begin
  Result := Installed and QueryServiceStatus(FHandle, lStatus) and
    (lStatus.dwCurrentState = SERVICE_STOPPED);
end;

{ TRLNTServices }

constructor TRLNTServices.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 32;
end;

function TRLNTServices.GetItem(const AIndex: Integer): TRLNTService;
begin
  Result := TRLNTService(inherited Items[AIndex]);
end;

function TRLNTServices.GetItemClass: TCollectionItemClass;
begin
  Result := TRLNTService;
end;

function TRLNTServices.Add: TRLNTService;
begin
  Result := TRLNTService(inherited Add);
end;

function TRLNTServices.Insert(const AIndex: Integer): TRLNTService;
begin
  Result := TRLNTService(inherited Insert(AIndex));
end;

function TRLNTServices.Find(const AName: string): TRLNTService;
begin
  Result := TRLNTService(inherited Find(AName));
end;

function TRLNTServices.FindByName(const AName: string): TRLNTService;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLServiceNotFound.CreateFmt('Service "%s" not found.', [AName]);
end;

function TRLNTServices.Installed(const AName: string): Boolean;
var
  lService: TRLNTService;
begin
  lService := Find(AName);
  Result := Assigned(lService) and lService.Installed;
end;

function TRLNTServices.Start(const AName: string;
  const AWait: Boolean): Boolean;
var
  lService: TRLNTService;
begin
  lService := Find(AName);
  Result := Assigned(lService) and lService.Start(AWait);
end;

function TRLNTServices.Stop(const AName: string;
  const AWait: Boolean): Boolean;
var
  lService: TRLNTService;
begin
  lService := Find(AName);
  Result := Assigned(lService) and lService.Stop(AWait);
end;

function TRLNTServices.Started(const AName: string): Boolean;
var
  lService: TRLNTService;
begin
  lService := Find(AName);
  Result := Assigned(lService) and lService.Started;
end;

function TRLNTServices.Stopped(const AName: string): Boolean;
var
  lService: TRLNTService;
begin
  lService := Find(AName);
  Result := Assigned(lService) and lService.Stopped;
end;

procedure TRLNTServices.StartAll(const AWait: Boolean);
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Start(AWait);
end;

procedure TRLNTServices.StopAll(const AWait: Boolean);
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Stop(AWait);
end;

function TRLNTServices.AllStarted: Boolean;
var
  I: Integer;
begin
  Result := True;
  I := 0;
  while (I <= Pred(Count)) and Result do
    if not Items[I].Started then
      Result := False
    else
      Inc(I);
end;

function TRLNTServices.AllStopped: Boolean;
var
  I: Integer;
begin
  Result := True;
  I := 0;
  while (I <= Pred(Count)) and Result do
    if not Items[I].Stopped then
      Result := False
    else
      Inc(I);
end;

end.
