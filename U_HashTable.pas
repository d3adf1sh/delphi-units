//=============================================================================
// U_HashTable
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_HashTable;

{$I RL.INC}

interface

uses
  SysUtils,
  U_BaseType,
  U_InterfacedObject;

type
  // Forward declarations
  TRLHash = class;  

  // TRLHash
  PRLHash = ^TRLHash;
  TRLHash = class
  private
    FKey: string;
    FValue: TObject;
    FNext: TRLHash;
  public
    property Key: string read FKey write FKey;
    property Value: TObject read FValue write FValue;
    property Next: TRLHash read FNext write FNext;
  end;
  
  // IRLHashTableEnumerator
  IRLHashTableEnumerator = interface
    ['{7D97764B-6DF9-42B5-A43D-ED37109D5DF4}']
    function GetKey: string;
    function GetKeyAsInteger: Integer;
    function GetKeyAsObject: TObject;
    function GetValue: TObject;
    function Next: Boolean;
    procedure Reset;
  end;

  // IRLHashTableEnumerable
  IRLHashTableEnumerable = interface
    ['{8F78BF2B-476B-4228-BAE3-968A0C1D0F3E}']
    function GetEnumerator: IRLHashTableEnumerator;
  end;

  // TRLHashTable
  TRLHashTable = class(TRLBaseType, IRLHashTableEnumerable)
  private
    FItems: array of TRLHash;
    FCaseSensitive: Boolean;
    FFreeValues: Boolean;
    function GetSize: Integer;
    procedure SetSize(const AValue: Integer);
    procedure SetCaseSensitive(const AValue: Boolean);
  protected
    function GetHashCode(const AKey: string): Cardinal; reintroduce; virtual;
    function FindHash(const AKey: string): PRLHash; virtual;
  public
    constructor Create; override;
    constructor CreateHashTable(const ASize: Integer); virtual;
    destructor Destroy; override;
    function Add(const AKey: string; const AValue: TObject): Boolean; overload; virtual;
    function Add(const AKey: Integer; const AValue: TObject): Boolean; overload; virtual;
    function Add(const AKey, AValue: TObject): Boolean; overload; virtual;
    function Modify(const AKey: string; const AValue: TObject): Boolean; overload; virtual;
    function Modify(const AKey: Integer; const AValue: TObject): Boolean; overload; virtual;
    function Modify(const AKey, AValue: TObject): Boolean; overload; virtual;
    function Find(const AKey: string; out AValue: TObject): Boolean; overload; virtual;
    function Find(const AKey: Integer; out AValue: TObject): Boolean; overload; virtual;
    function Find(const AKey: TObject; out AValue: TObject): Boolean; overload; virtual;
    function Contains(const AKey: string): Boolean; overload; virtual;
    function Contains(const AKey: Integer): Boolean; overload; virtual;
    function Contains(const AKey: TObject): Boolean; overload; virtual;
    function Remove(const AKey: string): Boolean; overload; virtual;
    function Remove(const AKey: Integer): Boolean; overload; virtual;
    function Remove(const AKey: TObject): Boolean; overload; virtual;
    procedure Clear; virtual;
    function GetEnumerator: IRLHashTableEnumerator; virtual;
    property Size: Integer read GetSize write SetSize;
    property CaseSensitive: Boolean read FCaseSensitive write SetCaseSensitive;
    property FreeValues: Boolean read FFreeValues write FFreeValues;
  end;

  // TRLHashTableEnumerator
  TRLHashTableEnumerator = class(TRLInterfacedObject, IRLHashTableEnumerator)
  private
    FHashTable: TRLHashTable;
    FHash: TRLHash;
    FIndex: Integer;
  public
    function GetKey: string; virtual;
    function GetKeyAsInteger: Integer; virtual;
    function GetKeyAsObject: TObject; virtual;
    function GetValue: TObject; virtual;
    function Next: Boolean; virtual;
    procedure Reset; virtual;
    property HashTable: TRLHashTable read FHashTable write FHashTable;
  end;

implementation

uses
  Math,
  U_System;

{ TRLHashTable }

constructor TRLHashTable.Create;
begin
  CreateHashTable(256);
end;

constructor TRLHashTable.CreateHashTable(const ASize: Integer);
begin
  inherited Create;
  SetSize(ASize);
  SetCaseSensitive(True);
end;

destructor TRLHashTable.Destroy;
begin
  Clear;
  inherited;
end;

function TRLHashTable.GetSize: Integer;
begin
  Result := Length(FItems);
end;

procedure TRLHashTable.SetSize(const AValue: Integer);
begin
  if AValue <> GetSize then
  begin
    Clear;
    SetLength(FItems, AValue);
  end;
end;

procedure TRLHashTable.SetCaseSensitive(const AValue: Boolean);
begin
  if AValue <> FCaseSensitive then
  begin
    FCaseSensitive := AValue;
    Clear;
  end;
end;

function TRLHashTable.GetHashCode(const AKey: string): Cardinal;
begin
  if not FCaseSensitive then
    Result := U_System.GetHashCode(AnsiUpperCase(AKey))
  else
    Result := U_System.GetHashCode(AKey);
end;

function TRLHashTable.FindHash(const AKey: string): PRLHash;
var
  ptrHash: PRLHash;
begin
  Result := nil;
  ptrHash := @FItems[GetHashCode(AKey) mod Cardinal(Size)];
  while Assigned(ptrHash^) and not Assigned(Result) do
    if (not FCaseSensitive and SameText(ptrHash^.Key, AKey)) or
      (ptrHash^.Key = AKey) then
      Result := ptrHash
    else
      ptrHash := @ptrHash^.Next;
end;

function TRLHashTable.Add(const AKey: string; const AValue: TObject): Boolean;
var
  nIndex: Integer;
  lHash: TRLHash;
begin
  Result := not Assigned(FindHash(AKey));
  if Result then
  begin
    nIndex := GetHashCode(AKey) mod Cardinal(Size);
    lHash := TRLHash.Create;
    lHash.Key := AKey;
    lHash.Value := AValue;
    lHash.Next := FItems[nIndex];
    FItems[nIndex] := lHash;
  end;
end;

function TRLHashTable.Add(const AKey: Integer; const AValue: TObject): Boolean;
begin
  Result := Add(IntToStr(AKey), AValue);
end;

function TRLHashTable.Add(const AKey, AValue: TObject): Boolean;
begin
  Result := Add(Integer(AKey), AValue);
end;

function TRLHashTable.Modify(const AKey: string;
  const AValue: TObject): Boolean;
var
  ptrHash: PRLHash;
begin
  ptrHash := FindHash(AKey);
  Result := Assigned(ptrHash);
  if Result then
    ptrHash^.Value := AValue;
end;

function TRLHashTable.Modify(const AKey: Integer;
  const AValue: TObject): Boolean;
begin
  Result := Modify(IntToStr(AKey), AValue);
end;

function TRLHashTable.Modify(const AKey, AValue: TObject): Boolean;
begin
  Result := Modify(Integer(AKey), AValue);
end;

function TRLHashTable.Find(const AKey: string; out AValue: TObject): Boolean;
var
  ptrHash: PRLHash;
begin
  ptrHash := FindHash(AKey);
  Result := Assigned(ptrHash);
  if Result then
    AValue := ptrHash^.Value
  else
    AValue := nil;
end;

function TRLHashTable.Find(const AKey: Integer; out AValue: TObject): Boolean;
begin
  Result := Find(IntToStr(AKey), AValue);
end;

function TRLHashTable.Find(const AKey: TObject; out AValue: TObject): Boolean;
begin
  Result := Find(Integer(AKey), AValue);
end;

function TRLHashTable.Contains(const AKey: string): Boolean;
var
  lValue: TObject;
begin
  Result := Find(AKey, lValue);
end;

function TRLHashTable.Contains(const AKey: Integer): Boolean;
begin
  Result := Contains(IntToStr(AKey));
end;

function TRLHashTable.Contains(const AKey: TObject): Boolean;
begin
  Result := Contains(Integer(AKey));
end;

function TRLHashTable.Remove(const AKey: string): Boolean;
var
  ptrHash: PRLHash;
  lHash: TRLHash;
begin
  ptrHash := FindHash(AKey);
  Result := Assigned(ptrHash);
  if Result then
  begin
    lHash := ptrHash^;
    ptrHash^ := lHash.Next;
    if FFreeValues and Assigned(lHash.Value) then
{$IFDEF NEXTGEN}
      lHash.Value := nil;
{$ELSE}
      lHash.Value.Free;
{$ENDIF}
    FreeAndNil(lHash);
  end;
end;

function TRLHashTable.Remove(const AKey: Integer): Boolean;
begin
  Result := Remove(IntToStr(AKey));
end;

function TRLHashTable.Remove(const AKey: TObject): Boolean;
begin
  Result := Remove(Integer(AKey));
end;

procedure TRLHashTable.Clear;
var
  I: Integer;
  lHash: TRLHash;
  lNext: TRLHash;
begin
  for I := 0 to High(FItems) do
  begin
    lHash := FItems[I];
    while Assigned(lHash) do
    begin
      lNext := lHash.Next;
      if FFreeValues and Assigned(lHash.Value) then
{$IFDEF NEXTGEN}
      lHash.Value := nil;
{$ELSE}
      lHash.Value.Free;
{$ENDIF}
      FreeAndNil(lHash);
      lHash := lNext;
    end;

    FItems[I] := nil;
  end;
end;

function TRLHashTable.GetEnumerator: IRLHashTableEnumerator;
var
  lEnumerator: TRLHashTableEnumerator;
begin
  lEnumerator := TRLHashTableEnumerator.Create;
  lEnumerator.HashTable := Self;
  Result := lEnumerator;
end;

{ TRLHashTableEnumerator }

function TRLHashTableEnumerator.GetKey: string;
begin
  if Assigned(FHash) then
    Result := FHash.Key
  else
    Result := '';
end;

function TRLHashTableEnumerator.GetKeyAsInteger: Integer;
begin
  Result := StrToIntDef(GetKey, 0);
end;

function TRLHashTableEnumerator.GetKeyAsObject: TObject;
begin
  Result := TObject(GetKeyAsInteger);
end;

function TRLHashTableEnumerator.GetValue: TObject;
begin
  if Assigned(FHash) then
    Result := FHash.Value
  else
    Result := nil;
end;

function TRLHashTableEnumerator.Next: Boolean;
begin
  if Assigned(FHash) and Assigned(FHash.Next) then
    FHash := FHash.Next
  else
  begin
    FHash := nil;
    while Assigned(FHashTable) and (FIndex < Length(FHashTable.FItems)) and
      not Assigned(FHash) do
    begin
      if Assigned(FHashTable.FItems[FIndex]) then
        FHash := FHashTable.FItems[FIndex];
      Inc(FIndex);
    end;
  end;

  Result := Assigned(FHash);
end;

procedure TRLHashTableEnumerator.Reset;
begin
  FHash := nil;
  FIndex := 0;
end;

end.
